package com.masterpik.rush;


import com.masterpik.rush.confy.PartyConfy;
import org.bukkit.Location;
import org.bukkit.entity.Item;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

public class Timer extends BukkitRunnable {

    private int count = 0;
    private int price1bcl = 0;
    private int price2bcl = 0;
    private int price3bcl = 0;
    private int price4bcl = 0;


    @Override
    public void run() {

        Vector itemVector = new Vector(0, 0, 0);

        if (count == 0) {
            price3bcl = 0;
            while (price3bcl < PartyConfy.spawners.size()) {
                if (PartyConfy.getStart(PartyConfy.locationToParty(PartyConfy.spawners.get(price3bcl)))) {
                    Item price3 = PartyConfy.spawners.get(price3bcl).getWorld().dropItem(
                            new Location(PartyConfy.spawners.get(price3bcl).getWorld(),
                                    PartyConfy.spawners.get(price3bcl).getX()+ 0.5,
                                    PartyConfy.spawners.get(price3bcl).getY() + 0,
                                    PartyConfy.spawners.get(price3bcl).getZ()+0.5), Items.price_3);
                    price3.setVelocity(price3.getVelocity().zero());
                }
                price3bcl ++;
            }
            count ++;
        } else  if (count == 1) {
            price1bcl = 0;
            while (price1bcl < PartyConfy.spawners.size()) {
                if (PartyConfy.getStart(PartyConfy.locationToParty(PartyConfy.spawners.get(price1bcl)))) {
                    Item price1 = PartyConfy.spawners.get(price1bcl).getWorld().dropItem(
                            new Location(PartyConfy.spawners.get(price1bcl).getWorld(),
                                    PartyConfy.spawners.get(price1bcl).getX()+ 0.5,
                                    PartyConfy.spawners.get(price1bcl).getY() + 0,
                                    PartyConfy.spawners.get(price1bcl).getZ()+0.5), Items.price_1);
                    price1.setVelocity(price1.getVelocity().zero());
                }
                price1bcl ++;
            }
            count ++;
        } else  if (count == 2) {
            price2bcl = 0;
            while (price2bcl < PartyConfy.spawners.size()) {
                if (PartyConfy.getStart(PartyConfy.locationToParty(PartyConfy.spawners.get(price2bcl)))) {
                    Item price2 = PartyConfy.spawners.get(price2bcl).getWorld().dropItem(
                            new Location(PartyConfy.spawners.get(price2bcl).getWorld(),
                                    PartyConfy.spawners.get(price2bcl).getX()+ 0.5,
                                    PartyConfy.spawners.get(price2bcl).getY() + 0,
                                    PartyConfy.spawners.get(price2bcl).getZ()+0.5), Items.price_2);
                    price2.setVelocity(price2.getVelocity().zero());
                }
                price2bcl ++;
            }
            count ++;
        } else  if (count == 3) {
            price4bcl = 0;
            while (price4bcl < PartyConfy.spawners.size()) {
                if (PartyConfy.getStart(PartyConfy.locationToParty(PartyConfy.spawners.get(price4bcl)))) {
                    Item price4 = PartyConfy.spawners.get(price4bcl).getWorld().dropItem(
                            new Location(PartyConfy.spawners.get(price4bcl).getWorld(),
                                    PartyConfy.spawners.get(price4bcl).getX()+ 0.5,
                                    PartyConfy.spawners.get(price4bcl).getY() + 0,
                                    PartyConfy.spawners.get(price4bcl).getZ()+0.5), Items.price_4);
                    price4.setVelocity(price4.getVelocity().zero());
                }

                price4bcl ++;
            }
            count = 0;
        }

    }
}
