package com.masterpik.rush;

import com.masterpik.api.util.UtilWorld;
import com.masterpik.games.settings.LobbySettings;
import com.masterpik.rush.confy.PartyConfy;
import com.masterpik.rush.confy.PlayersConfy;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.ScoreboardManager;

import java.util.ArrayList;


public class Main extends JavaPlugin implements Runnable{

	public static ScoreboardManager scoreboardManager;
	public static BungeeUtils bungee;

	//public static FakePlayer fakePlayer;

	public static ArrayList<Player> spectators;

	@Override
	public void onEnable() {


		Main.bungee = new BungeeUtils(this, "gamesCo");
		Bukkit.getMessenger().registerIncomingPluginChannel(this, "BungeeCord", new BungeeWork(this));

		//Bukkit.getMessenger().registerIncomingPluginChannel(this, "BungeeCord", new BungeeWork());

		UtilWorld.loadWorld("lobby");
		PartyManagement.initGamerule("lobby");

		Listener listen = new Events();
		PluginManager pm = Bukkit.getServer().getPluginManager();
	    pm.registerEvents(listen, this);

		getCommand("start").setExecutor(new Commands());
		getCommand("spectate").setExecutor(new Commands());

		scoreboardManager = Bukkit.getScoreboardManager();
		
	    Settings.BreakInit();
	    Settings.BreakTNTInit();
	    Settings.TNTBreakerInit();
	    
	    PlayersConfy.playerInit();
	    PartyConfy.partyInit();


		Misc.initLobbyItem();
	    Items.itemsInit();
		Traders.tradersInit();

		TimeManagement.startTimer();

		//Bukkit.getScheduler().scheduleSyncDelayedTask(this, this, 10L);

		//fakePlayer = new FakePlayer("rush");
		//fakePlayer.spawn();

		spectators = new ArrayList<>();

		LobbySettings.setLobbyWorld(Settings.lobby1.getWorld());
		LobbySettings.setCanSwapItem(false);

	}

	@Override
	public void onDisable() {
	}

	@Override
	public void run() {
		//Worlds.worldInit();
	}
}
