package com.masterpik.rush;

import org.bukkit.Bukkit;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

public class Traders {

	public static String trader__1_main_name = "§b§l§nARMES";
	public static String trader__2_main_name = "§c§l§nARMURES";
	public static String trader__3_main_name = "§2§l§nDEPLACEMENT";
	public static String trader__4_main_name = "§d§l§nEPICERIE";

	public static ArrayList<ArrayList<ArrayList<ArrayList<Inventory>>>> traders = new ArrayList<ArrayList<ArrayList<ArrayList<Inventory>>>>(3);
	public static ArrayList<Inventory> tradersAll = new ArrayList<Inventory>();
	public static ArrayList<Inventory> tradersMain = new ArrayList<Inventory>();
	public static ArrayList<Inventory> tradersChange = new ArrayList<Inventory>();


	public static void tradersInit() {

		Traders.trader__1_main_name = "§b§l§nARMES";
		Traders.trader__2_main_name = "§c§l§nARMURES";
		Traders.trader__3_main_name = "§2§l§nDEPLACEMENT";
		Traders.trader__4_main_name = "§d§l§nEPICERIE";

		int bucle1 = 0;
		int bucle2 = 0;
		int bucle3 = 0;
		int bucle4 = 0;
		int nbb = 0;
		double nbinv = 0;

		while (bucle1 < 3) {

			Traders.traders.add(bucle1, new ArrayList<ArrayList<ArrayList<Inventory>>>(4));

			bucle2 = 0;

			while (bucle2 < 4) {

				nbinv = 0;

				if (bucle2 == 0) {
					if (bucle1 == 0) {
						nbb = 5;
					}
					else if (bucle1 == 1){
						nbb = 8;
					}
					else if (bucle1 == 2) {
						nbb = 8;
					}
				}
				else if (bucle2 == 1) {
					if (bucle1 == 0) {
						nbb = 5;
					}
					else if (bucle1 == 1){
						nbb = 5;
					}
					else if (bucle1 == 2) {
						nbb = 5;
					}
				}
				else if (bucle2 == 2) {
					if (bucle1 == 0) {
						nbb = 5;
					}
					else if (bucle1 == 1){
						nbb = 9;
					}
					else if (bucle1 == 2) {
						nbb = 6;
					}
				}
				else if (bucle2 == 3) {
					if (bucle1 == 0) {
						nbb = 10;
					}
					else if (bucle1 == 1){
						nbb = 10;
					}
					else if (bucle1 == 2) {
						nbb = 10;
					}
				}


				nbinv = Math.ceil(((double)nbb)/((double)4));

				Traders.traders.get(bucle1).add(bucle2, new ArrayList<ArrayList<Inventory>>(2));
				Traders.traders.get(bucle1).get(bucle2).add(0, new ArrayList<Inventory>(nbb));
				Traders.traders.get(bucle1).get(bucle2).add(1, new ArrayList<Inventory>(nbb));

				bucle3 = 0;

				while (bucle3 < nbinv) {

					Traders.traders.get(bucle1).get(bucle2).get(0).add(bucle3, Bukkit.createInventory(null, 45, Traders.tradersByteToName((byte)bucle2)));

					Traders.traders.get(bucle1).get(bucle2).get(0).set(bucle3,
							Traders.traderAddColorMain(nbinv, bucle3,
									Traders.getInventory(bucle1, bucle2, 0, bucle3)));

					bucle3 ++;
				}

				Traders.tradersMain.addAll(Traders.traders.get(bucle1).get(bucle2).get(0));
				Traders.tradersAll.addAll(Traders.traders.get(bucle1).get(bucle2).get(0));

				bucle4 = 0;

				while (bucle4 < nbb) {

					Traders.traders.get(bucle1).get(bucle2).get(1).add(bucle4, Bukkit.createInventory(null, 27, Traders.tradersByteToName((byte)bucle2)));

					Traders.traders.get(bucle1).get(bucle2).get(1).set(bucle4,
							Traders.traderAddColor(nbb, bucle4,
									Traders.getInventory(bucle1, bucle2, 1, bucle4)));

					bucle4 ++;
				}

				Traders.tradersChange.addAll(Traders.traders.get(bucle1).get(bucle2).get(1));
				Traders.tradersAll.addAll(Traders.traders.get(bucle1).get(bucle2).get(1));

				bucle2 ++;
			}

			bucle1 ++;
		}

		int bcl1 = 0;

		while (bcl1 < 3) {
			Traders.addItemTrade(bcl1, 0, Items.sword_1, 3, Items.price_1);
			Traders.addItemTrade(bcl1, 0, Items.sword_2, 12, Items.price_1);
			Traders.addItemTrade(bcl1, 0, Items.sword_3, 24, Items.price_1);
			Traders.addItemTrade(bcl1, 0, Items.sword_4, 60, Items.price_1);
			if (bcl1 != 0) {
				Traders.addItemTrade(bcl1, 0, Items.bow_ni, 12, Items.price_1);
				Traders.addItemTrade(bcl1, 0, Items.bow_i, 24, Items.price_1);
				Traders.addItemTrade(bcl1, 0, Items.arrow, 4, Items.price_1);
			}
			Traders.addItemTrade(bcl1, 0, Items.tnt, 12, Items.price_1);
			bcl1 ++;
		}

		int bcl2 = 0;

		while (bcl2 < 3) {
			Traders.addItemTrade(bcl2, 1, Items.shield, 24, Items.price_2);
			Traders.addItemTrade(bcl2, 1, Items.chestplate_1, 3, Items.price_2);
			Traders.addItemTrade(bcl2, 1, Items.chestplate_2, 12, Items.price_2);
			Traders.addItemTrade(bcl2, 1, Items.chestplate_3, 24, Items.price_2);
			Traders.addItemTrade(bcl2, 1, Items.chestplate_4, 60, Items.price_2);
			bcl2 ++;
		}

		int bcl3 = 0;

		while (bcl3 < 3) {
			if (bcl3 == 0) {
				Traders.addItemTrade(bcl3, 2, Items.clay, 1, Items.price_3);
			}
			else if (bcl3 == 1) {
				Traders.addItemTrade(bcl3, 2, Items.elytra, 60, Items.price_3);
			}
			else if (bcl3 == 2) {
				Traders.addItemTrade(bcl3, 2, Items.boat, 6, Items.price_3);
				Traders.addItemTrade(bcl3, 2, Items.frostWalker, 24, Items.price_3);
			}

			if (bcl3 != 2) {
				Traders.addItemTrade(bcl3, 2, Items.pickaxe_1, 3, Items.price_3);
				Traders.addItemTrade(bcl3, 2, Items.pickaxe_2, 12, Items.price_3);
				Traders.addItemTrade(bcl3, 2, Items.pickaxe_3, 24, Items.price_3);
				Traders.addItemTrade(bcl3, 2, Items.pickaxe_4, 60, Items.price_3);
			}

			if (bcl3 != 0) {
				Traders.addItemTrade(bcl3, 2, Items.shovel_1, 3, Items.price_3);
				Traders.addItemTrade(bcl3, 2, Items.shovel_2, 12, Items.price_3);
				Traders.addItemTrade(bcl3, 2, Items.shovel_3, 24, Items.price_3);
				Traders.addItemTrade(bcl3, 2, Items.shovel_4, 60, Items.price_3);
			}
			bcl3 ++;
		}

		int bcl4 = 0;

		while (bcl4 < 3) {
			Traders.addItemTrade(bcl4, 3, Items.chest, 12, Items.price_4);
			Traders.addItemTrade(bcl4, 3, Items.goldenApple, 12, Items.price_4);
			Traders.addItemTrade(bcl4, 3, Items.potionPosion, 24, Items.price_4);
			Traders.addItemTrade(bcl4, 3, Items.potionStrenght, 24, Items.price_4);
			Traders.addItemTrade(bcl4, 3, Items.potionJump, 24, Items.price_4);
			Traders.addItemTrade(bcl4, 3, Items.potionHurt, 24, Items.price_4);
			Traders.addItemTrade(bcl4, 3, Items.potionRegen, 24, Items.price_4);
			Traders.addItemTrade(bcl4, 3, Items.potionSpeed, 24, Items.price_4);
			Traders.addItemTrade(bcl4, 3, Items.potionHeal, 24, Items.price_4);
			Traders.addItemTrade(bcl4, 3, Items.potionSlow, 24, Items.price_4);
			bcl4++;
		}

	}


	public static Inventory traderAddBlackMain(Inventory inventory) {

		int bucle = 0;

		while (bucle <= 44) {
			inventory.setItem(bucle, Items.windowBlack);
			bucle ++;
		}

		return inventory;

	}

	public static Inventory traderAddBlueMain(Inventory inventory) {

		inventory.setItem(23, Items.windowBlue);
		inventory.setItem(24, Items.windowBlue);
		inventory.setItem(25, Items.windowBlue);
		inventory.setItem(26, Items.windowBlue);

		return inventory;
	}

	public static Inventory traderAddRedMain(Inventory inventory) {

		inventory.setItem(18, Items.windowRed);
		inventory.setItem(19, Items.windowRed);
		inventory.setItem(20, Items.windowRed);
		inventory.setItem(21, Items.windowRed);

		return inventory;
	}

	public static Inventory traderAddColorMain(double nb, int bucle, Inventory inventory) {

		inventory = Traders.traderAddBlackMain(inventory);

		if (nb > 1) {

			if(bucle == 0) {
				inventory = Traders.traderAddBlueMain(inventory);
			}
			else if(bucle == (double)(nb-1)){
				inventory = Traders.traderAddRedMain(inventory);
			}
			else{
				inventory = Traders.traderAddBlueMain(inventory);
				inventory = Traders.traderAddRedMain(inventory);
			}
		}

		return inventory;
	}

	public static Inventory traderAddBlack(Inventory inventory) {

		int bucle = 0;

		while (bucle <= 26) {
			inventory.setItem(bucle, Items.windowBlack);
			bucle ++;
		}

		return inventory;
	}

	public static Inventory traderAddBlue(Inventory inventory) {

		inventory.setItem(7, Items.windowBlue);
		inventory.setItem(8, Items.windowBlue);
		inventory.setItem(16, Items.windowBlue);
		inventory.setItem(17, Items.windowBlue);
		inventory.setItem(25, Items.windowBlue);
		inventory.setItem(26, Items.windowBlue);

		return inventory;
	}

	public static Inventory traderAddRed(Inventory inventory) {

		inventory.setItem(0, Items.windowRed);
		inventory.setItem(1, Items.windowRed);
		inventory.setItem(9, Items.windowRed);
		inventory.setItem(10, Items.windowRed);
		inventory.setItem(18, Items.windowRed);
		inventory.setItem(19, Items.windowRed);

		return inventory;
	}

	public static Inventory traderAddGreen(Inventory inventory) {

		inventory.setItem(2, Items.windowGreen);
		inventory.setItem(3, Items.windowGreen);
		inventory.setItem(4, Items.windowGreen);
		inventory.setItem(5, Items.windowGreen);
		inventory.setItem(6, Items.windowGreen);
		inventory.setItem(11, Items.windowGreen);
		inventory.setItem(15, Items.windowGreen);
		inventory.setItem(20, Items.windowGreen);
		inventory.setItem(21, Items.windowGreen);
		inventory.setItem(22, Items.windowGreen);
		inventory.setItem(23, Items.windowGreen);
		inventory.setItem(24, Items.windowGreen);

		return inventory;
	}

	public static Inventory traderAddColor(double nb, int bucle, Inventory inventory) {

		inventory = Traders.traderAddBlack(inventory);
		inventory = Traders.traderAddGreen(inventory);

		if (nb > 1) {

			if(bucle == 0) {
				inventory = Traders.traderAddBlue(inventory);
			}
			else if(bucle == (double) (nb-1)){
				inventory = Traders.traderAddRed(inventory);
			}
			else{
				inventory = Traders.traderAddBlue(inventory);
				inventory = Traders.traderAddRed(inventory);
			}
		}


		return inventory;
	}

	public static Byte traderNameToByte(String name) {

		Byte type = (byte) 0;

		if (name.equals(Traders.trader__1_main_name)) {
			type = (byte) 0;
		}
		else if (name.equals(Traders.trader__2_main_name)) {
			type = (byte) 1;
		}
		else if (name.equals(Traders.trader__3_main_name)) {
			type = (byte) 2;
		}
		else if (name.equals(Traders.trader__4_main_name)) {
			type = (byte) 3;
		}

		return type;
	}

	public static String tradersByteToName(Byte type) {
		String name = "";

		if (type.equals((byte)0)) {
			name = Traders.trader__1_main_name;
		}
		else if (type.equals((byte)1)) {
			name = Traders.trader__2_main_name;
		}
		else if (type.equals((byte)2)) {
			name = Traders.trader__3_main_name;
		}
		else if (type.equals((byte)3)) {
			name = Traders.trader__4_main_name;
		}

		return name;
	}

	public static Inventory getInventory(int party, int name, int main , int nb) {

		Inventory inventory = Traders.traders.get(party).get(name).get(main).get(nb);

		return inventory;
	}

	public static int getParty(Inventory inventory) {

		int party = 0;
		int bucle = 0;
		int bucle2 = 0;

		while (bucle < 4) {

			bucle2 = 0;

			while (bucle2 < 2) {


				if (Traders.traders.get(0).get(bucle).get(bucle2).contains(inventory)) {
					party = 0;
				} else if (Traders.traders.get(1).get(bucle).get(bucle2).contains(inventory)) {
					party = 1;
				} else if (Traders.traders.get(2).get(bucle).get(bucle2).contains(inventory)) {
					party = 2;
				}

				bucle2 ++;
			}

			bucle ++;

		}

		return party;
	}

	public static int getName( Inventory inventory) {

		int name = 0;
		int bucle = 0;

		int party = Traders.getParty(inventory);

		while (bucle < 2) {

			if (Traders.traders.get(party).get(0).get(bucle).contains(inventory)) {
				name = 0;
			} else if (Traders.traders.get(party).get(1).get(bucle).contains(inventory)) {
				name = 1;
			} else if (Traders.traders.get(party).get(2).get(bucle).contains(inventory)) {
				name = 2;
			} else if (Traders.traders.get(party).get(3).get(bucle).contains(inventory)) {
				name = 3;
			}

			bucle ++;
		}


		return name;
	}

	public static int getMain(Inventory inventory) {

		int main = 0;

		if (Traders.tradersMain.contains(inventory)) {
			main = 0;
		}
		else if (Traders.tradersChange.contains(inventory)) {
			main = 1;
		}

		return main;
	}

	public static int getNb(Inventory inventory) {

		int nb = 0;

		int party = Traders.getParty(inventory);
		int name = Traders.getName(inventory);
		int main = Traders.getMain(inventory);


		nb = Traders.traders.get(party).get(name).get(main).indexOf(inventory);

		return nb;
	}

	public static Inventory addItemMain(Inventory inventory, int nb, ItemStack item) {

		if (nb == 1) {
			inventory.setItem(1, item);
			inventory.setItem(2, item);
			inventory.setItem(3, item);
			inventory.setItem(10, item);
			inventory.setItem(11, item);
			inventory.setItem(12, item);
		}
		else if (nb == 2) {
			inventory.setItem(5, item);
			inventory.setItem(6, item);
			inventory.setItem(7, item);
			inventory.setItem(14, item);
			inventory.setItem(15, item);
			inventory.setItem(16, item);
		}
		else if (nb == 3) {
			inventory.setItem(28, item);
			inventory.setItem(29, item);
			inventory.setItem(30, item);
			inventory.setItem(37, item);
			inventory.setItem(38, item);
			inventory.setItem(39, item);
		}
		else if (nb == 4) {
			inventory.setItem(32, item);
			inventory.setItem(33, item);
			inventory.setItem(34, item);
			inventory.setItem(41, item);
			inventory.setItem(42, item);
			inventory.setItem(43, item);
		}

		return inventory;
	}

	public static Inventory addItem(Inventory inventory, ItemStack item, int priceCount, ItemStack priceItem) {

		priceItem.setAmount(priceCount);

		inventory.setItem(12, item);
		inventory.setItem(13, priceItem);
		priceItem.setAmount(1);
		inventory.setItem(14, item);

		return inventory;
	}

	public static void addItemTrade(int party, int name, ItemStack item, int priceCount, ItemStack priceItem) {

		int[] tabl = Traders.testForItemIn(party,name,Items.windowBlack);
		int nb = tabl[0];
		int pos = tabl[1];
		int count = tabl[2];

		Traders.traders.get(party).get(name).get(0).set(nb,
				Traders.addItemMain(
						Traders.traders.get(party).get(name).get(0).get(nb), pos, item));

		Traders.traders.get(party).get(name).get(1).set(count,
				Traders.addItem(
						Traders.traders.get(party).get(name).get(1).get(count), item, priceCount, priceItem));



	}

	public static ItemStack getPriceItemTrade(int party, int name, ItemStack item) {

		int[] tabl = new int[3];
		ItemStack itemle = null;

		tabl = Traders.testForItemIn(party, name, item);

		itemle = (Traders.traders.get(party).get(name).get(1).get(tabl[2]).getItem(13)).clone();


		return itemle;
	}

	public static ItemStack getItem(Inventory inventory) {

		ItemStack item = (Traders.traders.get(
				Traders.getParty(inventory)).get(
				Traders.getName(inventory)).get(
				Traders.getMain(inventory)).get(
				Traders.getNb(inventory)).getItem(12)).clone();

		return item;
	}

	public static int[] testForItemIn(int party, int name, ItemStack item) {

		int[] tabl = new int[3];

		if (Traders.traders.get(party).get(name).get(0).get(0).getItem(1).equals(item)) {
			tabl[0] = 0;
			tabl[1] = 1;
			tabl[2] = 0;
		}
		else if (Traders.traders.get(party).get(name).get(0).get(0).getItem(5).equals(item)) {
			tabl[0] = 0;
			tabl[1] = 2;
			tabl[2] = 1;
		}
		else if (Traders.traders.get(party).get(name).get(0).get(0).getItem(28).equals(item)) {
			tabl[0] = 0;
			tabl[1] = 3;
			tabl[2] = 2;
		}
		else if (Traders.traders.get(party).get(name).get(0).get(0).getItem(32).equals(item)) {
			tabl[0] = 0;
			tabl[1] = 4;
			tabl[2] = 3;
		}
		else if (Traders.traders.get(party).get(name).get(0).get(1).getItem(1).equals(item)) {
			tabl[0] = 1;
			tabl[1] = 1;
			tabl[2] = 4;
		}
		else if (Traders.traders.get(party).get(name).get(0).get(1).getItem(5).equals(item)) {
			tabl[0] = 1;
			tabl[1] = 2;
			tabl[2] = 5;
		}
		else if (Traders.traders.get(party).get(name).get(0).get(1).getItem(28).equals(item)) {
			tabl[0] = 1;
			tabl[1] = 3;
			tabl[2] = 6;
		}
		else if (Traders.traders.get(party).get(name).get(0).get(1).getItem(32).equals(item)) {
			tabl[0] = 1;
			tabl[1] = 4;
			tabl[2] = 7;
		}
		else if (Traders.traders.get(party).get(name).get(0).get(2).getItem(1).equals(item)) {
			tabl[0] = 2;
			tabl[1] = 1;
			tabl[2] = 8;
		}
		else if (Traders.traders.get(party).get(name).get(0).get(2).getItem(5).equals(item)) {
			tabl[0] = 2;
			tabl[1] = 2;
			tabl[2] = 9;
		}
		else if (Traders.traders.get(party).get(name).get(0).get(2).getItem(28).equals(item)) {
			tabl[0] = 2;
			tabl[1] = 3;
			tabl[2] = 10;
		}
		else if (Traders.traders.get(party).get(name).get(0).get(2).getItem(32).equals(item)) {
			tabl[0] = 2;
			tabl[1] = 4;
			tabl[2] = 11;
		}
		else if (Traders.traders.get(party).get(name).get(0).get(3).getItem(1).equals(item)) {
			tabl[0] = 3;
			tabl[1] = 1;
			tabl[2] = 12;
		}
		else if (Traders.traders.get(party).get(name).get(0).get(3).getItem(5).equals(item)) {
			tabl[0] = 3;
			tabl[1] = 2;
			tabl[2] = 13;
		}
		else if (Traders.traders.get(party).get(name).get(0).get(3).getItem(28).equals(item)) {
			tabl[0] = 3;
			tabl[1] = 3;
			tabl[2] = 14;
		}
		else if (Traders.traders.get(party).get(name).get(0).get(3).getItem(32).equals(item)) {
			tabl[0] = 3;
			tabl[1] = 4;
			tabl[2] = 15;
		}
		else {
			tabl[0] = 100;
			tabl[1] = 100;
			tabl[2] = 100;
		}


		return tabl;
	}


}
