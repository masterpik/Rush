package com.masterpik.rush;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;

import java.util.ArrayList;

public class Settings {
	
	public static int diedAt = 194;
	
	public static int teamColor = 5;



	public static ArrayList<Material> Break = new ArrayList();
	public static void BreakInit() {
		Settings.Break.add(Material.STAINED_CLAY);
		Settings.Break.add(Material.DIRT);
		Settings.Break.add(Material.GRASS);
		Settings.Break.add(Material.GRASS_PATH);
		Settings.Break.add(Material.SAND);
		Settings.Break.add(Material.STONE);
		Settings.Break.add(Material.LONG_GRASS);
		Settings.Break.add(Material.YELLOW_FLOWER);
		Settings.Break.add(Material.RED_ROSE);
		Settings.Break.add(Material.DOUBLE_PLANT);
		Settings.Break.add(Material.LEAVES);
		Settings.Break.add(Material.VINE);
		Settings.Break.add(Material.HUGE_MUSHROOM_1);
		Settings.Break.add(Material.HUGE_MUSHROOM_2);
		Settings.Break.add(Material.LOG);
		Settings.Break.add(Material.TNT);
		
	}
	
	
	public static ArrayList<Material> BreakTNT = new ArrayList();
	public static void BreakTNTInit() {
		Settings.BreakTNT.add(Material.STAINED_CLAY);
		Settings.BreakTNT.add(Material.DIRT);
		Settings.BreakTNT.add(Material.GRASS);
		Settings.BreakTNT.add(Material.GRASS_PATH);
		Settings.BreakTNT.add(Material.SAND);
		Settings.BreakTNT.add(Material.STONE);
		Settings.BreakTNT.add(Material.LONG_GRASS);
		Settings.BreakTNT.add(Material.YELLOW_FLOWER);
		Settings.BreakTNT.add(Material.RED_ROSE);
		Settings.BreakTNT.add(Material.DOUBLE_PLANT);
		Settings.BreakTNT.add(Material.LEAVES);
		Settings.BreakTNT.add(Material.VINE);
		Settings.BreakTNT.add(Material.HUGE_MUSHROOM_1);
		Settings.BreakTNT.add(Material.HUGE_MUSHROOM_2);
		Settings.BreakTNT.add(Material.LOG);
		Settings.BreakTNT.add(Material.BED_BLOCK);
		Settings.Break.add(Material.TNT);
	}
	
	public static ArrayList<Material> TNTBreaker = new ArrayList();
	public static void TNTBreakerInit() {
		Settings.TNTBreaker.add(Material.STONE_SLAB2);
		Settings.TNTBreaker.add(Material.STEP);
		Settings.TNTBreaker.add(Material.BED_BLOCK);
		Settings.TNTBreaker.add(Material.CARPET);
		Settings.TNTBreaker.add(Material.LONG_GRASS);
		Settings.TNTBreaker.add(Material.YELLOW_FLOWER);
		Settings.TNTBreaker.add(Material.RED_ROSE);
		Settings.TNTBreaker.add(Material.DOUBLE_PLANT);
		Settings.TNTBreaker.add(Material.VINE);
		Settings.TNTBreaker.add(Material.TORCH);
		Settings.TNTBreaker.add(Material.REDSTONE_TORCH_ON);
		Settings.TNTBreaker.add(Material.REDSTONE_TORCH_OFF);
		Settings.Break.add(Material.TNT);
	}


	public static float volumeSound = (float) 1.0;
	public static float pitchSound = (float) 1.0;

	public static String playerConfigPath = "plugins/Rush/players.yml";
	public static String partyConfigPath = "plugins/Rush/party.yml";
	
	public static Location lobby1 = new Location(Bukkit.getWorld("lobby"), (double)500.5, (double)201, (double)0.5);
	public static Location lobby2 = new Location(Bukkit.getWorld("lobby"), (double)0.5, (double)201, (double)0.5);
	public static Location lobby3 = new Location(Bukkit.getWorld("lobby"), (double)-499.5, (double)201, (double)0.5);
	public static Location testitem = new Location(Bukkit.getWorld("world"), (double)34.5, (double)14, (double)-3.5);
	
	public static String surceParty(byte type, byte tip, byte part) {
		return ""+type+"-"+tip+"-"+part+"";
	}
	public static String nextParty(byte type, byte tip, byte part, int nb) {
		return ""+type+"-"+tip+"-"+part+"_"+nb+"";
	}
	public static String locationToString(Location location) {
		String loc = "";
		
		loc = ""+(Integer.toString((int) location.getX()))+"/"
				+(Integer.toString((int) location.getY()))+"/"
				+(Integer.toString((int) location.getZ()))+"";
		
		return loc;
		
	}

	public static int getFoi(byte tip) {
		int foi = 0;
		if (tip == (byte) 1) {
			foi = 2;
		}
		else if (tip == (byte) 2) {
			foi = 4;
		}
		else if (tip == (byte) 3) {
			foi = 8;
		}
		return foi;
	}

	public static int getNb(byte part) {
		int nb = 0;
		if (part == (byte) 1) {
			nb = 1;
		}
		else if (part == (byte) 2)  {
			nb = 2;
		}
		else if (part == (byte) 3) {
			nb = 4;
		}
		else if (part == (byte) 4) {
			nb = 8;
		}
		else if (part == (byte) 5) {
			nb = 10;
		}
		return nb;
	}

	public static int getNbPlayer(byte tip, byte part) {

		int nbMax = 0;
		int foi = Settings.getFoi(tip);
		int nb = Settings.getNb(part);

		nbMax = foi * nb;

		return nbMax;
	}
	
}
