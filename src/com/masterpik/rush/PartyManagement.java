package com.masterpik.rush;

import com.masterpik.api.util.UtilDate;
import com.masterpik.api.util.UtilWorld;
import com.masterpik.connect.enums.Statistics;
import com.masterpik.connect.enums.Times;
import com.masterpik.rush.confy.PartyConfy;
import com.masterpik.rush.confy.PlayersConfy;
import com.masterpik.rush.confy.RushPlayer;
import org.bukkit.*;
import org.bukkit.block.Banner;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.EnderCrystal;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scheduler.BukkitTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;


public class PartyManagement {

	public static void newParty(byte type, byte tip, byte part) {
		
		int nb = PartyConfy.addParty(type, tip, part);
		
		Location middleLocation = null;
		
		String surce = Settings.surceParty(type, tip, part);
		String next = Settings.nextParty(type, tip, part, nb);

		UtilWorld.cloneWorld(surce, next);

		/*Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
				"mv load "+surce+"");

		Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), 
				"mv clone "+surce+" "+next+"");
		
		Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), 
				"mvconfirm");
		
		Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), 
				"mv import "+next+" normal");*/


		PartyManagement.initGamerule(next);

		
		if (type == 1) {
			middleLocation = new Location(Bukkit.getWorld(""+next+""), 0, 200, 0);
		}
		else if (type == 2) {
			middleLocation = new Location(Bukkit.getWorld(""+next+""), 0, 200, 0);
		}
		else if (type == 3) {
			middleLocation = new Location(Bukkit.getWorld(""+next+""), 0, 211, 0);
		}
		
		
		ArrayList<Byte> colors = new ArrayList();
		
		
		if (tip == 1) {
			
			colors.add((byte)14);
			colors.add((byte)11);
			
			
		}
		else if (tip == 2) {
			
			colors.add((byte)14);
			colors.add((byte)5);
			colors.add((byte)4);
			colors.add((byte)11);
			
		}
		else if (tip == 3) {
			
			colors.add((byte)14);
			colors.add((byte)5);
			colors.add((byte)4);
			colors.add((byte)11);
			colors.add((byte)15);
			colors.add((byte)2);
			colors.add((byte)8);
			colors.add((byte)1);
		}
		
		int bucle = 0; 
		Iterator<Byte> iter = colors.iterator(); 
		while (iter.hasNext()) { 
		    Byte color = iter.next();
		    
		    PartyConfy.createBase(next, (byte) color);
		    
		    bucle = bucle + 1; 
		}
		
		
		Misc.find(middleLocation, type, tip, part, nb);

		PartyManagement.addWorldBorder(next, type);

		//Bukkit.getPlayer("axroy").sendMessage("maybeok");
		
	}

	public static void startParty(byte type, byte tip, byte part, int nb) {

		String party = Settings.nextParty(type, tip, part, nb);
		PartyConfy.setStartStatu(party, true);
		ArrayList<Player> namePlayers = PartyConfy.getPlayers(party);

		PartyConfy.partyNotStart.remove(party);

		ArrayList<Integer> in = new ArrayList<>();

		in.add((Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(TimeManagement.plugin, new Runnable() {
			int bucle = 0;
			int bucle2 = 0;
			int count = 10;
			Player player;
			@Override
			public void run() {

				bucle = 0;
				if (PartyConfy.RushPartys.get(party).isForced()
					|| PartyConfy.getPlayers(party).size() == Settings.getNbPlayer((byte) PartyConfy.getPartyTip(party), (byte) PartyConfy.getPartyPart(party))) {
					while (bucle < namePlayers.size()) {

						player = namePlayers.get(bucle);

						if (count > 1) {
							if (bucle == 0) {
								if (count == 10) {
									ChatManagement.brodcasteMessage("" + Messages.countRebour + "" + count + " §r§a§lsecondes", player);
								}
							}
							//Misc.setAllPartyXp(player, count);
							Misc.sendSecondTitle(player, Integer.toString(count));
							Misc.actionBarSecond(player, count);
							player.playSound(player.getLocation(), Sound.BLOCK_NOTE_PLING, Settings.volumeSound, Settings.pitchSound);
						} else if (count == 1) {
							if (bucle == 0) {
								if (count == 10) {
									ChatManagement.brodcasteMessage("" + Messages.countRebour + "" + count + " §r§a§lseconde", player);
								}
							}
							//Misc.setAllPartyXp(player, count);
							Misc.sendSecondTitle(player, Integer.toString(count));
							Misc.actionBarSecond(player, count);
							player.playSound(player.getLocation(), Sound.BLOCK_NOTE_PLING, Settings.volumeSound, Settings.pitchSound);
						} else {
							if (bucle == 0) {
								ChatManagement.brodcasteMessage(Messages.partyStart, player);
							}

							player.playSound(player.getLocation(), Sound.BLOCK_NOTE_PLING, Settings.volumeSound, (float) (Settings.pitchSound+1));
							//Location locate = PartyConfy.getBedPlayer(player);

							//Bukkit.getLogger().info("lele");

							if (PlayersConfy.RushPlayers.containsKey(player)) {

								Location tp = new Location(PartyConfy.getBedPlayer(player).getWorld(),
										PartyConfy.getBedPlayer(player).getX() + 0.5,
										PartyConfy.getBedPlayer(player).getY() + 0,
										PartyConfy.getBedPlayer(player).getZ() + 0.5,
										PlayersConfy.RushPlayers.get(player).getRespawnYaw().getYaw(),
										0);

								player.setBedSpawnLocation(tp, true);

								player.teleport(tp);
							}
							player.setGameMode(GameMode.SURVIVAL);
							player.setFoodLevel(20);
							player.setHealth(20);
							player.getInventory().clear();
							for(PotionEffect effect : player.getActivePotionEffects())
							{
								player.removePotionEffect(effect.getType());
							}
							//player.setScoreboard(ScoreboardManagement.addTeams(player));

							player.sendMessage(Messages.startingMessageDescription);
							Misc.setArmor(player);

						}

						bucle++;
					}

					if (count > 0) {
						count--;
					} else {

						bucle2 = 0;
						while (bucle2 < Bukkit.getWorld(party).getEntities().size()) {
							if (!(Bukkit.getWorld(party).getEntities().get(bucle2) instanceof Player)
									&& !(Bukkit.getWorld(party).getEntities().get(bucle2) instanceof EnderCrystal)) {
								Bukkit.getWorld(party).getEntities().get(bucle2).remove();
							}
							bucle2++;
						}
						Misc.setAllPartyXp(party, 0);
						Misc.sendSecondTitle(party, Messages.partyStartTitle);
						Misc.actionBarSecond(party, -1);

						PartyConfy.RushPartys.get(party).setStartTime(UtilDate.getCurrentDate().getTime());

						Bukkit.getScheduler().cancelTask(in.get(0));

					}
				} else {
					Misc.setAllPartyXp(party, 0);
					PartyConfy.setStartStatu(party, false);
					PartyConfy.partyNotStart.add(party);

					ArrayList<Player> pllist = PartyConfy.getPlayers(party);
					int bcl1 = 0;
					while (bcl1 < pllist.size()) {
						pllist.get(bcl1).setScoreboard(Main.scoreboardManager.getNewScoreboard());
						bcl1++;
					}

					Bukkit.getScheduler().cancelTask(in.get(0));
				}

			}
		}, 0, 20)));



	}

	public static void winParty(String party, Player player, Byte color) {

		if (!player.getWorld().equals(Bukkit.getWorld("lobby"))
				|| !PartyConfy.getPlayers(party).get(0).getWorld().equals(Bukkit.getWorld("lobby"))) {
			ChatManagement.brodcasteMessage("" + Messages.win1 + "" + Teams.colorToChatString(color) + "§l" + Teams.colorToString(color) + "" + Messages.win2 + "", player);
			PartyConfy.RushPartys.get(party).setWin(true);
			PartyConfy.RushPartys.get(party).setWinner(color);

			PartyConfy.RushPartys.get(party).setEndTime(UtilDate.getCurrentDate().getTime());

			for (Player pl : PartyConfy.RushPartys.get(party).getPlayers()) {
				if (PlayersConfy.RushPlayers.containsKey(pl)) {
					RushPlayer Rpl = PlayersConfy.RushPlayers.get(pl);

					pl.sendMessage("\n" + Messages.main + (Statistics.PIK.getMessage().replaceAll("%v", Integer.toString(RushStatistics.getPiksWin(pl, (Rpl.getColor() == PartyConfy.RushPartys.get(party).getWinner()))))));
					pl.sendMessage(Messages.main + (Statistics.SPENT_TIME.getMessage().replaceAll("%v", Times.getTime(UtilDate.millisecondsToMinutes(PartyConfy.RushPartys.get(party).getEndTime() - PartyConfy.RushPartys.get(party).getStartTime())))));
					pl.sendMessage(Messages.main + (Statistics.KILL_AMOUNT.getMessage().replaceAll("%v", Integer.toString(Rpl.getKills()))));
					pl.sendMessage(Messages.main + (Statistics.DEATH_AMOUNT.getMessage().replaceAll("%v", Integer.toString(Rpl.getDeaths()))));
					pl.sendMessage(Messages.main + (Statistics.BED_BREAK_AMOUNT.getMessage().replaceAll("%v", Integer.toString(Rpl.getBeds_break()))));
					pl.sendMessage(Messages.main + Messages.statsRegister + "\n ");
				}
			}

			//Statistics.partyFinish(PartyConfy.RushPartys.get(party));


			BukkitTask time = Bukkit.getServer().getScheduler().runTaskLater(TimeManagement.plugin, new Runnable() {
				@Override
				public void run() {
					int bucle = 0;

					if (!PartyConfy.getEnd(party)) {



						ArrayList<Player> players = PartyConfy.getPlayers(party);


						while (bucle < players.size()) {

							players.get(bucle).sendMessage(Messages.endParty);
							BungeeWork.sendPlayer(players.get(bucle), "hub");

							//players.get(bucle).kickPlayer(Messages.endParty);

							bucle++;
						}


						//PartyManagement.deleteParty(party);
					}
				}
			}, 100);
		}

	}

	
	public static void removeBed(String party, Location location, Player player) {

		Location locDown = location.getBlock().getRelative(BlockFace.DOWN).getLocation();
		Byte color = locDown.getBlock().getData();

		if (!PartyConfy.getBedStatu(party, color)) {

			PartyConfy.setBedStatu(party, color, true);


			Location beaconLocation = PartyConfy.getBeaconLocation(party, color);
			Location locUp = beaconLocation.getBlock().getRelative(BlockFace.UP).getLocation();
			Location locUp2 = locUp.getBlock().getRelative(BlockFace.UP).getLocation();
			locUp2.getBlock().setType(Material.QUARTZ_BLOCK);

			Location bannerLocation;
			Banner banner;
			int bucle = 0;
			int parc = PartyConfy.getBanners(party, color).size();
			while (bucle < parc) {
				bannerLocation = PartyConfy.getBannerLocation(party, color, bucle);
				banner = (Banner) bannerLocation.getBlock().getState();
				Misc.setCross(banner);
				bucle++;
			}

			Misc.explodeFirework(location, 3, Teams.colorToColor(color));

			ChatManagement.brodcasteMessage(""+Messages.bedExplode1+""+Teams.colorToChatString(color)+"§l"+Teams.colorToString(color)+""+Messages.bedEXplode2+"§r"+Teams.colorToChatString(PlayersConfy.getColor(player))+"§l§o("+player.getName()+")", player);

			if (PartyConfy.RushPartys != null
					&& PartyConfy.RushPartys.containsKey(party)
					&& PartyConfy.RushPartys.get(party).getBases().containsKey(PlayersConfy.getColor(player))) {
				for (Player pl : PartyConfy.RushPartys.get(party).getBases().get(PlayersConfy.getColor(player)).getPlayers()) {
					pl.setHealth(player.getMaxHealth());
				}
			}
			if (PlayersConfy.RushPlayers.containsKey(player)) {

				RushPlayer Rplayer = PlayersConfy.RushPlayers.get(player);
				Rplayer.setBeds_break(Rplayer.getBeds_break() + 1);

			}
			if (PartyManagement.bedCount(party, (byte) PlayersConfy.getTip(player)) == 0) {
				ChatManagement.brodcasteMessage(Messages.reduction, player);
				PartyManagement.startReduction(party, (byte) PlayersConfy.getType(player));
			}

			ArrayList<Player> playersInTeam = new ArrayList<>();
			playersInTeam = PartyConfy.getPlayersTeam(party, color);

			if (playersInTeam != null && playersInTeam.size() != 0) {
				int bucle2 = 0;

				while (bucle2 < playersInTeam.size()) {

					playersInTeam.get(bucle2).playSound(playersInTeam.get(bucle2).getLocation(), Sound.ENTITY_WITHER_SPAWN, Settings.volumeSound, Settings.pitchSound);

					bucle2++;
				}
			}

			ScoreboardManagement.refreshScoreboard(party);

		}
		
	}
	
	public static void addTrader(String party, Location tradersLocation, String type) {
		PartyConfy.addTrader(party, tradersLocation, type);
		Misc.trader(tradersLocation, type);
		
	}
	
	public static void deleteParty(String next) {

		int bucl = 0;
		while (bucl < Main.spectators.size()) {
			//Bukkit.getPlayer("axroy").sendRawMessage(Main.spectators.get(bucl).getWorld().getName()+" | "+next);
			if (Main.spectators.get(bucl).getWorld().getName().equalsIgnoreCase(next)) {
				Main.spectators.get(bucl).teleport(Settings.lobby1);
			}
			bucl ++;
		}

		UtilWorld.deleteWorld(next);

		Bukkit.getScheduler().runTaskLater(com.masterpik.api.Main.plugin, new Runnable() {
			@Override
			public void run() {
				PartyConfy.removeParty(next);
			}
		}, 20L);

		/*Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
				"mv delete " + next + "");
		
		Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
				"mvconfirm");*/
		
		//Bukkit.getPlayer("axroy").sendMessage("maybeok");
		
		
	}

	public static void findr(Location cursor, byte type, byte tip,byte part, int nb) {
		
		Byte color = null;
		
		String surce = Settings.surceParty(type, tip, part);
		String next = Settings.nextParty(type, tip, part, nb);
		
		Location locDown = cursor.getBlock().getRelative(BlockFace.DOWN).getLocation();
		Location locUp = cursor.getBlock().getRelative(BlockFace.UP).getLocation();
		Location locUp2 = locUp.getBlock().getRelative(BlockFace.UP).getLocation();
		
		
		
		if (cursor.getBlock().getType().equals(Material.BED_BLOCK)
				&& locDown.getBlock().getType().equals(Material.STAINED_GLASS)) {
			
			color = locDown.getBlock().getData();
			
			PartyConfy.addBed(next, color, cursor);
			
		}
		else if (cursor.getBlock().getType().equals(Material.SEA_LANTERN) 
				&& locDown.getBlock().getType().equals(Material.QUARTZ_BLOCK) 
				&& locUp.getBlock().getType().equals(Material.AIR)
				&& locUp2.getBlock().getType().equals(Material.QUARTZ_STAIRS)) {
			
			PartyConfy.addSpawner(next, locUp);
			
		}
		
		else if (cursor.getBlock().getType().equals(Material.WALL_BANNER)) {
			
			Banner banner = (Banner) cursor.getBlock().getState();
			
			color = banner.getBaseColor().getData();
			
			PartyConfy.addBanner(next, color, cursor);
			
		}
		
		else if (cursor.getBlock().getType().equals(Material.REDSTONE_BLOCK)) {
			PartyManagement.addTrader(next, locUp, Traders.trader__1_main_name);
		}
		else if (cursor.getBlock().getType().equals(Material.EMERALD_BLOCK)) {
			PartyManagement.addTrader(next, locUp, Traders.trader__2_main_name);
		}
		else if (cursor.getBlock().getType().equals(Material.GOLD_BLOCK)) {
			PartyManagement.addTrader(next, locUp, Traders.trader__3_main_name);
		}
		else if (cursor.getBlock().getType().equals(Material.DIAMOND_BLOCK)) {
			PartyManagement.addTrader(next, locUp, Traders.trader__4_main_name);
		}
		
		else if (cursor.getBlock().getType().equals(Material.BEACON)) {
			color = locUp.getBlock().getData();
			PartyConfy.addBeacon(next, color, cursor);
		}
		
	}

	public static void initGamerule(String party) {

		World world = Bukkit.getWorld(party);

		String trou = "true";
		String folse = "false";

		world.setFullTime(6000L);

		world.setGameRuleValue("commandBlockOutput", folse);
		world.setGameRuleValue("doDaylightCycle", folse);
		world.setGameRuleValue("doEntityDrops", folse);
		world.setGameRuleValue("doFireTick", folse);
		world.setGameRuleValue("doMobLoot", folse);
		world.setGameRuleValue("doMobSpawning", folse);
		world.setGameRuleValue("doTileDrops", trou);
		world.setGameRuleValue("keepInventory", folse);
		world.setGameRuleValue("logAdminCommands", trou);
		world.setGameRuleValue("mobGriefing", folse);
		world.setGameRuleValue("naturalRegeneration", trou);
		world.setGameRuleValue("randomTickSpeed", "3");
		world.setGameRuleValue("reducedDebugInfo", trou);
		world.setGameRuleValue("sendCommandFeedback", folse);
		world.setGameRuleValue("showDeathMessages", folse);

		world.setAmbientSpawnLimit(0);
		world.setAnimalSpawnLimit(0);
		world.setMonsterSpawnLimit(0);
		world.setWaterAnimalSpawnLimit(0);

		world.setAutoSave(false);

		if (party == "lobby") {
			world.setPVP(false);
			world.setDifficulty(Difficulty.PEACEFUL);
		} else {
			world.setDifficulty(Difficulty.NORMAL);
			world.setPVP(true);
		}

		/*Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
				"mvmset animals false "+party);
		Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
				"mvmset monsters false "+party);

		if (party == "lobby") {
			Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
					"mvmset pvp false "+party);
		} else {
			Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
					"mvmset pvp true "+party);
		}

		Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
				"mvmset diff 2 "+party);
		Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
				"mvmset weather false "+party);*/


	}

	public static Byte testForWin(Player player) {
		Byte winner = (byte) -1;
		Byte tip = (byte) PlayersConfy.getTip(player);

		ArrayList<Byte> teamsWithPlayer = new ArrayList<Byte>();

		ArrayList<Byte> teamsInParty = Teams.tipToColor(tip);

		int bucle = 0;


		while (bucle < teamsInParty.size()) {
			if (PartyManagement.testForPlayerInTeam(PlayersConfy.getParty(player), teamsInParty.get(bucle))) {
				teamsWithPlayer.add(teamsInParty.get(bucle));
			}
			bucle ++;
		}

		if (teamsWithPlayer.size() == 1) {
			winner = teamsWithPlayer.get(0);
		}

		return winner;
	}

	public static boolean testForPlayerInTeam(String party, Byte color) {
		boolean statu = true;

		if (PartyConfy.getPlayersTeam(party, color).size() == 0) {
			statu = false;
		}
		else {
			statu = true;
		}

		return statu;
	}

	public static int bedCount(String party, byte tip) {
		int count = 0;
		ArrayList<Byte> teams = Teams.tipToColor(tip);
		int bucle = 0;

		while(bucle < teams.size()) {

			if (!PartyConfy.RushPartys.get(party).getBase(teams.get(bucle)).isBedStatu()) {
				count ++;
			}

			bucle ++;
		}

		return count;
	}

	public static void addWorldBorder(String party, byte type) {

		WorldBorder worldBorder = Bukkit.getWorld(party).getWorldBorder();

		double borderWidth = 0;

		if (type == (byte) 1) {
			borderWidth = 128;
		}
		else if (type == (byte) 2) {
			borderWidth = 238;
		}
		else if (type == (byte) 3) {
			borderWidth = 212;
		}
		borderWidth = borderWidth + 1.5;

		worldBorder.setCenter(0, 0);
		worldBorder.setSize(borderWidth);
		worldBorder.setWarningDistance(5);
		worldBorder.setWarningTime(10);
		worldBorder.setDamageAmount(3);
		worldBorder.setDamageBuffer(1);

	}

	public static void startReduction(String party, byte type){
		WorldBorder worldBorder = Bukkit.getWorld(party).getWorldBorder();

		int second = 0;

		if (type == (byte) 1) {
			second = 300;
		}
		else if (type == (byte) 2) {
			second = 600;
		}
		else if (type == (byte) 3) {
			second  = 600;
		}

		worldBorder.setSize(10, second);

	}

	public static void changeTeamPlayer(Player player, ItemStack banner) {
		Byte newcolor = Teams.bannerByteToByte(banner.getData().getData());
		Byte oldColor = PlayersConfy.getColor(player);

		if (PlayersConfy.RushPlayers.containsKey(player)) {

			if (newcolor != oldColor) {
				player.closeInventory();
				if (PartyConfy.getPlayersTeam(PlayersConfy.getParty(player), newcolor).size() < Settings.getNb((byte) PlayersConfy.getPart(player))) {


					PlayersConfy.RushPlayers.get(player).setColor(newcolor);
					PartyConfy.RushPartys.get(PlayersConfy.getParty(player)).getBase(newcolor).addPlayer(player);
					PartyConfy.RushPartys.get(PlayersConfy.getParty(player)).getBase(oldColor).removePlayer(player);

					player.setPlayerListName(Teams.colorToChatString(PlayersConfy.getColor(player)) + "§l" + player.getName());

					ScoreboardManagement.refreshScoreboard(PlayersConfy.getParty(player));

					Misc.setArmor(player);

					player.sendMessage(Messages.joinTeam + Teams.colorToChatString(newcolor) + "§l§o" + Teams.colorToString(newcolor));
				} else {
					player.sendMessage(Messages.teamIsFull1 + Teams.colorToChatString(newcolor) + "§l§o" + Teams.colorToString(newcolor) + Messages.teamIsFull2);
				}
			}
		}
	}

}
