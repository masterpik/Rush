package com.masterpik.rush;

import com.masterpik.api.util.UtilShield;
import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Items {

	public static ArrayList<ItemStack> tradeItems = new ArrayList<ItemStack>();
	
	public static ItemStack sword_1;
	public static NbtFactory.NbtCompound sword_1Comp;
	public static ItemStack sword_2;
	public static NbtFactory.NbtCompound sword_2Comp;
	public static ItemStack sword_3;
	public static NbtFactory.NbtCompound sword_3Comp;
	public static ItemStack sword_4;
	public static NbtFactory.NbtCompound sword_4Comp;
	
	public static ItemStack tnt;
	public static NbtFactory.NbtCompound tntComp;
	
	public static ItemStack bow_ni;
	public static NbtFactory.NbtCompound bow_niComp;
	public static ItemStack bow_i;
	public static NbtFactory.NbtCompound bow_iComp;
	
	public static ItemStack arrow;
	public static NbtFactory.NbtCompound arrowComp;
	
	
	public static ItemStack chestplate_1;
	public static NbtFactory.NbtCompound chestplate_1Comp;
	public static ItemStack chestplate_2;
	public static NbtFactory.NbtCompound chestplate_2Comp;
	public static ItemStack chestplate_3;
	public static NbtFactory.NbtCompound chestplate_3Comp;
	public static ItemStack chestplate_4;
	public static NbtFactory.NbtCompound chestplate_4Comp;

	public static ItemStack shield;
	public static NbtFactory.NbtCompound shieldComp;
	
	
	public static ItemStack clay;
	public static NbtFactory.NbtCompound clayComp;
	
	public static ItemStack pickaxe_1;
	public static NbtFactory.NbtCompound pickaxe_1Comp;
	public static ItemStack pickaxe_2;
	public static NbtFactory.NbtCompound pickaxe_2Comp;
	public static ItemStack pickaxe_3;
	public static NbtFactory.NbtCompound pickaxe_3Comp;
	public static ItemStack pickaxe_4;
	public static NbtFactory.NbtCompound pickaxe_4Comp;
	
	public static ItemStack shovel_1;
	public static NbtFactory.NbtCompound shovel_1Comp;
	public static ItemStack shovel_2;
	public static NbtFactory.NbtCompound shovel_2Comp;
	public static ItemStack shovel_3;
	public static NbtFactory.NbtCompound shovel_3Comp;
	public static ItemStack shovel_4;
	public static NbtFactory.NbtCompound shovel_4Comp;
	
	public static ItemStack boat;
	public static NbtFactory.NbtCompound boatComp;
	
	
	public static ItemStack goldenApple;
	public static NbtFactory.NbtCompound goldenAppleComp;
	
	public static ItemStack potionPosion;
	public static NbtFactory.NbtCompound potionPosionComp;
	public static ItemStack potionStrenght;
	public static NbtFactory.NbtCompound potionStrenghtComp;
	public static ItemStack potionJump;
	public static NbtFactory.NbtCompound potionJumpComp;
	public static ItemStack potionHurt;
	public static NbtFactory.NbtCompound potionHurtComp;
	public static ItemStack potionRegen;
	public static NbtFactory.NbtCompound potionRegenComp;
	public static ItemStack potionSpeed;
	public static NbtFactory.NbtCompound potionSpeedComp;
	public static ItemStack potionHeal;
	public static NbtFactory.NbtCompound potionHealComp;
	public static ItemStack potionSlow;
	public static NbtFactory.NbtCompound potionSlowComp;

	public static ItemStack elytra;
	public static NbtFactory.NbtCompound elytraComp;

	public static ItemStack frostWalker;
	public static NbtFactory.NbtCompound frostWalkerComp;

	public static ItemStack chest;
	public static NbtFactory.NbtCompound chestComp;
	
	
	
	public static ItemStack windowBlack;
	public static NbtFactory.NbtCompound windowBlackComp;
	public static ItemStack windowBlue;
	public static NbtFactory.NbtCompound windowBlueComp;
	public static ItemStack windowRed;
	public static NbtFactory.NbtCompound windowRedComp;
	public static ItemStack windowGreen;
	public static NbtFactory.NbtCompound windowGreenComp;

	public static ItemStack price_1;
	public static NbtFactory.NbtCompound price_1Comp;

	public static ItemStack price_2;
	public static NbtFactory.NbtCompound price_2Comp;

	public static ItemStack price_3;
	public static NbtFactory.NbtCompound price_3Comp;

	public static ItemStack price_4;
	public static NbtFactory.NbtCompound price_4Comp;


	public static ItemStack helmet;
	public static NbtFactory.NbtCompound helmetComp;

	public static ItemStack legg;
	public static NbtFactory.NbtCompound leggComp;

	public static ItemStack boot;
	public static NbtFactory.NbtCompound bootComp;

	public static void itemsInit() {

		int pickupdelay = 1;
		int age = 6000;

		Items.sword_1 = NbtFactory.getCraftItemStack(new ItemStack(Material.IRON_SWORD));
		Items.sword_1.addEnchantment(Enchantment.DAMAGE_ALL, 1);
		Items.sword_1Comp = NbtFactory.fromItemTag(Items.sword_1);
		Items.sword_1Comp.putPath("display.Name", "§c§l►§r§b§lépée [1]§r§c§l◄");
		Items.sword_1Comp.putPath("Unbreakable.", 1);
		Items.sword_1Comp.putPath("PickupDelay.", pickupdelay);
		Items.sword_1Comp.putPath("Age.", age);
		Items.tradeItems.add(Items.sword_1);

		Items.sword_2 = NbtFactory.getCraftItemStack(new ItemStack(Material.IRON_SWORD));
		Items.sword_2.addEnchantment(Enchantment.DAMAGE_ALL, 2);
		Items.sword_2Comp = NbtFactory.fromItemTag(Items.sword_2);
		Items.sword_2Comp.putPath("display.Name", "§c§l►§r§b§lépée [2]§r§c§l◄");
		Items.sword_2Comp.putPath("Unbreakable.", 1);
		Items.sword_2Comp.putPath("PickupDelay.", pickupdelay);
		Items.sword_2Comp.putPath("Age.", age);
		Items.tradeItems.add(Items.sword_2);
		
		Items.sword_3 = NbtFactory.getCraftItemStack(new ItemStack(Material.IRON_SWORD));
		Items.sword_3.addEnchantment(Enchantment.DAMAGE_ALL, 3);
		Items.sword_3Comp = NbtFactory.fromItemTag(Items.sword_3);
		Items.sword_3Comp.putPath("display.Name", "§c§l►§r§b§lépée [3]§r§c§l◄");
		Items.sword_3Comp.putPath("Unbreakable.", 1);
		Items.sword_3Comp.putPath("PickupDelay.", pickupdelay);
		Items.sword_3Comp.putPath("Age.", age);
		Items.tradeItems.add(Items.sword_3);

		Items.sword_4 = NbtFactory.getCraftItemStack(new ItemStack(Material.IRON_SWORD));
		Items.sword_4.addEnchantment(Enchantment.DAMAGE_ALL, 4);
		Items.sword_4Comp = NbtFactory.fromItemTag(Items.sword_4);
		Items.sword_4Comp.putPath("display.Name", "§c§l►§r§b§lépée [4]§r§c§l◄");
		Items.sword_4Comp.putPath("Unbreakable.", 1);
		Items.sword_4Comp.putPath("PickupDelay.", pickupdelay);
		Items.sword_4Comp.putPath("Age.", age);
		Items.tradeItems.add(Items.sword_4);
		
		
		Items.tnt = NbtFactory.getCraftItemStack(new ItemStack(Material.TNT));
		Items.tntComp = NbtFactory.fromItemTag(Items.tnt);
		Items.tntComp.putPath("display.Name", "§c§l►§r§b§lTNT§r§c§l◄");
		Items.tntComp.putPath("PickupDelay.", pickupdelay);
		Items.tntComp.putPath("Age.", age);
		ItemMeta tntMeta = Items.tnt.getItemMeta();
		ArrayList<String> tntLore = new ArrayList<>();
		tntLore.addAll(Arrays.asList(Messages.behindTntText.split("\n")));
		tntMeta.setLore(tntLore);
		Items.tnt.setItemMeta(tntMeta);
		Items.tradeItems.add(Items.tnt);
		
		
		Items.bow_ni = NbtFactory.getCraftItemStack(new ItemStack(Material.BOW));
		Items.bow_niComp = NbtFactory.fromItemTag(Items.bow_ni);
		Items.bow_niComp.putPath("display.Name", "§c§l►§r§b§larc [1]§r§c§l◄");
		Items.bow_niComp.putPath("Unbreakable.", 1);
		Items.bow_niComp.putPath("PickupDelay.", pickupdelay);
		Items.bow_niComp.putPath("Age.", age);
		Items.tradeItems.add(Items.bow_ni);
		
		Items.bow_i = NbtFactory.getCraftItemStack(new ItemStack(Material.BOW));
		Items.bow_i.addEnchantment(Enchantment.ARROW_INFINITE, 1);
		Items.bow_iComp = NbtFactory.fromItemTag(Items.bow_i);
		Items.bow_iComp.putPath("display.Name", "§c§l►§r§b§larc [2]§r§c§l◄");
		Items.bow_iComp.putPath("Unbreakable.", 1);
		Items.bow_iComp.putPath("PickupDelay.", pickupdelay);
		Items.bow_iComp.putPath("Age.", age);
		Items.tradeItems.add(Items.bow_i);

		Items.arrow = NbtFactory.getCraftItemStack(new ItemStack(Material.ARROW));
		Items.arrowComp = NbtFactory.fromItemTag(Items.arrow);
		Items.arrowComp.putPath("display.Name", "§c§l►§r§b§lflèche§r§c§l◄");
		Items.arrowComp.putPath("PickupDelay.", pickupdelay);
		Items.arrowComp.putPath("Age.", age);
		Items.tradeItems.add(Items.arrow);
		
		
		
		Items.chestplate_1 = NbtFactory.getCraftItemStack(new ItemStack(Material.IRON_CHESTPLATE));
		Items.chestplate_1.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
		Items.chestplate_1Comp = NbtFactory.fromItemTag(Items.chestplate_1);
		Items.chestplate_1Comp.putPath("display.Name", "§c§l►§r§c§lplastron [1]§r§c§l◄");
		Items.chestplate_1Comp.putPath("Unbreakable.", 1);
		Items.chestplate_1Comp.putPath("PickupDelay.", pickupdelay);
		Items.chestplate_1Comp.putPath("Age.", age);
		Items.tradeItems.add(Items.chestplate_1);
		
		Items.chestplate_2 = NbtFactory.getCraftItemStack(new ItemStack(Material.IRON_CHESTPLATE));
		Items.chestplate_2.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
		Items.chestplate_2Comp = NbtFactory.fromItemTag(Items.chestplate_2);
		Items.chestplate_2Comp.putPath("display.Name", "§c§l►§r§c§lplastron [2]§r§c§l◄");
		Items.chestplate_2Comp.putPath("Unbreakable.", 1);
		Items.chestplate_2Comp.putPath("PickupDelay.", pickupdelay);
		Items.chestplate_2Comp.putPath("Age.", age);
		Items.tradeItems.add(Items.chestplate_2);
		
		Items.chestplate_3 = NbtFactory.getCraftItemStack(new ItemStack(Material.IRON_CHESTPLATE));
		Items.chestplate_3.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
		Items.chestplate_3Comp = NbtFactory.fromItemTag(Items.chestplate_3);
		Items.chestplate_3Comp.putPath("display.Name", "§c§l►§r§c§lplastron [3]§r§c§l◄");
		Items.chestplate_3Comp.putPath("Unbreakable.", 1);
		Items.chestplate_3Comp.putPath("PickupDelay.", pickupdelay);
		Items.chestplate_3Comp.putPath("Age.", age);
		Items.tradeItems.add(Items.chestplate_3);
		
		Items.chestplate_4 = NbtFactory.getCraftItemStack(new ItemStack(Material.IRON_CHESTPLATE));
		Items.chestplate_4.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
		Items.chestplate_4Comp = NbtFactory.fromItemTag(Items.chestplate_4);
		Items.chestplate_4Comp.putPath("display.Name", "§c§l►§r§c§lplastron [4]§r§c§l◄");
		Items.chestplate_4Comp.putPath("Unbreakable.", 1);
		Items.chestplate_4Comp.putPath("PickupDelay.", pickupdelay);
		Items.chestplate_4Comp.putPath("Age.", age);
		Items.tradeItems.add(Items.chestplate_4);

		Items.shield = NbtFactory.getCraftItemStack(new ItemStack(Material.SHIELD));
		Items.shield.setItemMeta(UtilShield.patternOnShield(new ItemStack(Material.SHIELD), DyeColor.WHITE, null).getItemMeta());
		Items.shieldComp = NbtFactory.fromItemTag(Items.shield);
		Items.shieldComp.putPath("display.Name", "§c§l►§r§c§lbouclier§r§c§l◄");
		Items.shieldComp.putPath("Unbreakable.", 1);
		Items.shieldComp.putPath("PickupDelay.", pickupdelay);
		Items.shieldComp.putPath("Age.", age);
		Items.tradeItems.add(Items.shield);
		
		
		Items.clay = NbtFactory.getCraftItemStack(new ItemStack(Material.STAINED_CLAY, 8));
		Items.clayComp = NbtFactory.fromItemTag(Items.clay);
		Items.clayComp.putPath("display.Name", "§c§l►§r§2§lBLOCKS§r§c§l◄");
		Items.clayComp.putPath("PickupDelay.", pickupdelay);
		Items.clayComp.putPath("Age.", age);
		Items.tradeItems.add(Items.clay);
		
		
		//Items.pickaxe_1 = NbtFactory.getCraftItemStack(new ItemStack(Material.IRON_PICKAXE));
		Items.pickaxe_1 = NbtFactory.getCraftItemStack(new ItemStack(Material.STONE_PICKAXE));
		Items.pickaxe_1.addEnchantment(Enchantment.DIG_SPEED, 1);
		Items.pickaxe_1Comp = NbtFactory.fromItemTag(Items.pickaxe_1);
		Items.pickaxe_1Comp.putPath("display.Name", "§c§l►§r§2§lpioche [1]§r§c§l◄");
		Items.pickaxe_1Comp.putPath("Unbreakable.", 1);
		Items.pickaxe_1Comp.putPath("PickupDelay.", pickupdelay);
		Items.pickaxe_1Comp.putPath("Age.", age);
		Items.tradeItems.add(Items.pickaxe_1);

		//Items.pickaxe_2 = NbtFactory.getCraftItemStack(new ItemStack(Material.IRON_PICKAXE));
		Items.pickaxe_2 = NbtFactory.getCraftItemStack(new ItemStack(Material.STONE_PICKAXE));
		Items.pickaxe_2.addEnchantment(Enchantment.DIG_SPEED, 2);
		Items.pickaxe_2Comp = NbtFactory.fromItemTag(Items.pickaxe_2);
		Items.pickaxe_2Comp.putPath("display.Name", "§c§l►§r§2§lpioche [2]§r§c§l◄");
		Items.pickaxe_2Comp.putPath("Unbreakable.", 1);
		Items.pickaxe_2Comp.putPath("PickupDelay.", pickupdelay);
		Items.pickaxe_2Comp.putPath("Age.", age);
		Items.tradeItems.add(Items.pickaxe_2);
		
		//Items.pickaxe_3 = NbtFactory.getCraftItemStack(new ItemStack(Material.IRON_PICKAXE));
		Items.pickaxe_3 = NbtFactory.getCraftItemStack(new ItemStack(Material.STONE_PICKAXE));
		Items.pickaxe_3.addEnchantment(Enchantment.DIG_SPEED, 3);
		Items.pickaxe_3Comp = NbtFactory.fromItemTag(Items.pickaxe_3);
		Items.pickaxe_3Comp.putPath("display.Name", "§c§l►§r§2§lpioche [3]§r§c§l◄");
		Items.pickaxe_3Comp.putPath("Unbreakable.", 1);
		Items.pickaxe_3Comp.putPath("PickupDelay.", pickupdelay);
		Items.pickaxe_3Comp.putPath("Age.", age);
		Items.tradeItems.add(Items.pickaxe_3);
		
		//Items.pickaxe_4 = NbtFactory.getCraftItemStack(new ItemStack(Material.IRON_PICKAXE));
		Items.pickaxe_4 = NbtFactory.getCraftItemStack(new ItemStack(Material.STONE_PICKAXE));
		Items.pickaxe_4.addEnchantment(Enchantment.DIG_SPEED, 4);
		Items.pickaxe_4Comp = NbtFactory.fromItemTag(Items.pickaxe_4);
		Items.pickaxe_4Comp.putPath("display.Name", "§c§l►§r§2§lpioche [4]§r§c§l◄");
		Items.pickaxe_4Comp.putPath("Unbreakable.", 1);
		Items.pickaxe_4Comp.putPath("PickupDelay.", pickupdelay);
		Items.pickaxe_4Comp.putPath("Age.", age);
		Items.tradeItems.add(Items.pickaxe_4);
		
		
		//Items.shovel_1 = NbtFactory.getCraftItemStack(new ItemStack(Material.IRON_SPADE));
		Items.shovel_1 = NbtFactory.getCraftItemStack(new ItemStack(Material.STONE_SPADE));
		Items.shovel_1.addEnchantment(Enchantment.DIG_SPEED, 1);
		Items.shovel_1Comp = NbtFactory.fromItemTag(Items.shovel_1);
		Items.shovel_1Comp.putPath("display.Name", "§c§l►§r§2§lpelle [1]§r§c§l◄");
		Items.shovel_1Comp.putPath("Unbreakable.", 1);
		Items.shovel_1Comp.putPath("PickupDelay.", pickupdelay);
		Items.shovel_1Comp.putPath("Age.", age);
		Items.tradeItems.add(Items.shovel_1);
		
		//Items.shovel_2 = NbtFactory.getCraftItemStack(new ItemStack(Material.IRON_SPADE));
		Items.shovel_2 = NbtFactory.getCraftItemStack(new ItemStack(Material.STONE_SPADE));
		Items.shovel_2.addEnchantment(Enchantment.DIG_SPEED, 2);
		Items.shovel_2Comp = NbtFactory.fromItemTag(Items.shovel_2);
		Items.shovel_2Comp.putPath("display.Name", "§c§l►§r§2§lpelle [2]§r§c§l◄");
		Items.shovel_2Comp.putPath("Unbreakable.", 1);
		Items.shovel_2Comp.putPath("PickupDelay.", pickupdelay);
		Items.shovel_2Comp.putPath("Age.", age);
		Items.tradeItems.add(Items.shovel_2);
		
		//Items.shovel_3 = NbtFactory.getCraftItemStack(new ItemStack(Material.IRON_SPADE));
		Items.shovel_3 = NbtFactory.getCraftItemStack(new ItemStack(Material.STONE_SPADE));
		Items.shovel_3.addEnchantment(Enchantment.DIG_SPEED, 3);
		Items.shovel_3Comp = NbtFactory.fromItemTag(Items.shovel_3);
		Items.shovel_3Comp.putPath("display.Name", "§c§l►§r§2§lpelle [3]§r§c§l◄");
		Items.shovel_3Comp.putPath("Unbreakable.", 1);
		Items.shovel_3Comp.putPath("PickupDelay.", pickupdelay);
		Items.shovel_3Comp.putPath("Age.", age);
		Items.tradeItems.add(Items.shovel_3);
		
		//Items.shovel_4 = NbtFactory.getCraftItemStack(new ItemStack(Material.IRON_SPADE));
		Items.shovel_4 = NbtFactory.getCraftItemStack(new ItemStack(Material.STONE_SPADE));
		Items.shovel_4.addEnchantment(Enchantment.DIG_SPEED, 4);
		Items.shovel_4Comp = NbtFactory.fromItemTag(Items.shovel_4);
		Items.shovel_4Comp.putPath("display.Name", "§c§l►§r§2§lpelle [4]§r§c§l◄");
		Items.shovel_4Comp.putPath("Unbreakable.", 1);
		Items.shovel_4Comp.putPath("PickupDelay.", pickupdelay);
		Items.shovel_4Comp.putPath("Age.", age);
		Items.tradeItems.add(Items.shovel_4);
		
		
		Items.boat = NbtFactory.getCraftItemStack(new ItemStack(Material.BOAT));
		Items.boatComp = NbtFactory.fromItemTag(Items.boat);
		Items.boatComp.putPath("display.Name", "§c§l►§r§2§lbarque§r§c§l◄");
		Items.boatComp.putPath("PickupDelay.", pickupdelay);
		Items.boatComp.putPath("Age.", age);
		Items.tradeItems.add(Items.boat);
		
		
		Items.goldenApple = NbtFactory.getCraftItemStack(new ItemStack(Material.GOLDEN_APPLE));
		Items.goldenAppleComp = NbtFactory.fromItemTag(Items.goldenApple);
		Items.goldenAppleComp.putPath("display.Name", "§c§l►§r§d§lpomme d\'or§r§c§l◄");
		Items.goldenAppleComp.putPath("PickupDelay.", pickupdelay);
		Items.goldenAppleComp.putPath("Age.", age);
		Items.tradeItems.add(Items.goldenApple);

		ItemStack pPosion = new ItemStack(Material.SPLASH_POTION,1);
		/*PotionMeta PpotionMeta = (PotionMeta) pPosion.getItemMeta();
		PpotionMeta.setMainEffect(PotionEffectType.POISON);
		PpotionMeta.addCustomEffect(new PotionEffect(PotionEffectType.POISON, 15, 1), true);
		pPosion.setItemMeta(PpotionMeta);*/
		Items.potionPosion = NbtFactory.getCraftItemStack(pPosion);
		Items.potionPosionComp = NbtFactory.fromItemTag(Items.potionPosion);
		Items.potionPosionComp.putPath("Potion", "minecraft:poison");
		Items.potionPosionComp.putPath("display.Name", "§c§l►§r§d§lpoison§r§c§l◄");
		Items.potionPosionComp.putPath("PickupDelay.", pickupdelay);
		Items.potionPosionComp.putPath("Age.", age);
		Items.tradeItems.add(Items.potionPosion);
		//Bukkit.getLogger().info("Potion poison nbt : "+Items.potionPosionComp.getPath("CustomPotionEffects"));
		
		//Items.potionStrenght = NbtFactory.getCraftItemStack((new Potion(PotionType.STRENGTH,1,true)).toItemStack(1));
		Items.potionStrenght = NbtFactory.getCraftItemStack(new ItemStack(Material.SPLASH_POTION));
		Items.potionStrenghtComp = NbtFactory.fromItemTag(Items.potionStrenght);
		Items.potionStrenghtComp.putPath("display.Name", "§c§l►§r§d§lforce§r§c§l◄");
		Items.potionStrenghtComp.putPath("Potion", "minecraft:strength");
		Items.potionStrenghtComp.putPath("PickupDelay.", pickupdelay);
		Items.potionStrenghtComp.putPath("Age.", age);
		Items.tradeItems.add(Items.potionStrenght);
		
		//Items.potionJump = NbtFactory.getCraftItemStack((new Potion(PotionType.JUMP,1,false)).toItemStack(1));
		Items.potionJump = NbtFactory.getCraftItemStack(new ItemStack(Material.LINGERING_POTION));
		Items.potionJumpComp = NbtFactory.fromItemTag(Items.potionJump);
		Items.potionJumpComp.putPath("Potion", "minecraft:leaping");
		Items.potionJumpComp.putPath("display.Name", "§c§l►§r§d§lsaut§r§c§l◄");
		Items.potionJumpComp.putPath("PickupDelay.", pickupdelay);
		Items.potionJumpComp.putPath("Age.", age);
		Items.tradeItems.add(Items.potionJump);
		
		//Items.potionHurt = NbtFactory.getCraftItemStack((new Potion(PotionType.INSTANT_DAMAGE,1,true)).toItemStack(1));
		Items.potionHurt = NbtFactory.getCraftItemStack(new ItemStack(Material.SPLASH_POTION));
		Items.potionHurtComp = NbtFactory.fromItemTag(Items.potionHurt);
		Items.potionHurtComp.putPath("Potion", "minecraft:harming");
		Items.potionHurtComp.putPath("display.Name", "§c§l►§r§d§ldegats§r§c§l◄");
		Items.potionHurtComp.putPath("PickupDelay.", pickupdelay);
		Items.potionHurtComp.putPath("Age.", age);
		Items.tradeItems.add(Items.potionHurt);

		//Items.potionRegen = NbtFactory.getCraftItemStack((new Potion(PotionType.REGEN,1,false)).toItemStack(1));
		Items.potionRegen = NbtFactory.getCraftItemStack(new ItemStack(Material.LINGERING_POTION));
		Items.potionRegenComp = NbtFactory.fromItemTag(Items.potionRegen);
		Items.potionRegenComp.putPath("Potion", "minecraft:regeneration");
		Items.potionRegenComp.putPath("display.Name", "§c§l►§r§d§lregen§r§c§l◄");
		Items.potionRegenComp.putPath("PickupDelay.", pickupdelay);
		Items.potionRegenComp.putPath("Age.", age);
		Items.tradeItems.add(Items.potionRegen);

		//Items.potionSpeed = NbtFactory.getCraftItemStack((new Potion(PotionType.SPEED,1,false)).toItemStack(1));
		Items.potionSpeed = NbtFactory.getCraftItemStack(new ItemStack(Material.LINGERING_POTION));
		Items.potionSpeedComp = NbtFactory.fromItemTag(Items.potionSpeed);
		Items.potionSpeedComp.putPath("Potion", "minecraft:swiftness");
		Items.potionSpeedComp.putPath("display.Name", "§c§l►§r§d§lvitesse§r§c§l◄");
		Items.potionSpeedComp.putPath("PickupDelay.", pickupdelay);
		Items.potionSpeedComp.putPath("Age.", age);
		Items.tradeItems.add(Items.potionSpeed);
		
		//Items.potionHeal = NbtFactory.getCraftItemStack((new Potion(PotionType.INSTANT_HEAL,1,false)).toItemStack(1));
		Items.potionHeal = NbtFactory.getCraftItemStack(new ItemStack(Material.POTION));
		Items.potionHealComp = NbtFactory.fromItemTag(Items.potionHeal);
		Items.potionHealComp.putPath("Potion", "minecraft:healing");
		Items.potionHealComp.putPath("display.Name", "§c§l►§r§d§lsoin§r§c§l◄");
		Items.potionHealComp.putPath("PickupDelay.", pickupdelay);
		Items.potionHealComp.putPath("Age.", age);
		Items.tradeItems.add(Items.potionHeal);
		
		//Items.potionSlow = NbtFactory.getCraftItemStack((new Potion(PotionType.SLOWNESS,1,true)).toItemStack(1));
		Items.potionSlow = NbtFactory.getCraftItemStack(new ItemStack(Material.SPLASH_POTION));
		Items.potionSlowComp = NbtFactory.fromItemTag(Items.potionSlow);
		Items.potionSlowComp.putPath("Potion", "minecraft:slowness");
		Items.potionSlowComp.putPath("display.Name", "§c§l►§r§d§llenteur§r§c§l◄");
		Items.potionSlowComp.putPath("PickupDelay.", pickupdelay);
		Items.potionSlowComp.putPath("Age.", age);
		Items.tradeItems.add(Items.potionSlow);

		Items.chest = NbtFactory.getCraftItemStack(new ItemStack(Material.ENDER_CHEST));
		Items.chestComp = NbtFactory.fromItemTag(Items.chest);
		Items.chestComp.putPath("display.Name", "§c§l►§r§d§lender inventory§r§c§l◄");
		Items.chestComp.putPath("PickupDelay.", pickupdelay);
		Items.chestComp.putPath("Age.", age);
		ItemMeta chestMeta = Items.chest.getItemMeta();
		ArrayList<String> chestLore = new ArrayList<>();
		chestLore.addAll(Arrays.asList(Messages.behindECText.split("\n")));
		chestMeta.setLore(chestLore);
		Items.chest.setItemMeta(chestMeta);
		Items.tradeItems.add(Items.chest);


		Items.elytra = NbtFactory.getCraftItemStack(new ItemStack(Material.ELYTRA));
		Items.elytraComp = NbtFactory.fromItemTag(Items.elytra);
		Items.elytraComp.putPath("display.Name", "§c§l►§r§2§lailes§r§c§l◄");
		Items.elytraComp.putPath("Unbreakable.", 1);
		Items.elytraComp.putPath("PickupDelay.", pickupdelay);
		Items.elytraComp.putPath("Age.", age);
		Items.tradeItems.add(Items.elytra);

		Items.frostWalker = NbtFactory.getCraftItemStack(new ItemStack(Material.ENCHANTED_BOOK));
		Items.frostWalkerComp = NbtFactory.fromItemTag(Items.frostWalker);
		Items.frostWalkerComp.putPath("display.Name", "§c§l►§r§2§lfrozone§r§c§l◄");
		Items.frostWalkerComp.putPath("PickupDelay.", pickupdelay);
		Items.frostWalkerComp.putPath("Age.", age);
		EnchantmentStorageMeta frostWalkerMeta = (EnchantmentStorageMeta) Items.frostWalker.getItemMeta();
		ArrayList<String> frostWalkerLore = new ArrayList<>();
		frostWalkerLore.addAll(Arrays.asList(Messages.behindFrostWalkerText.split("\n")));
		frostWalkerMeta.setLore(frostWalkerLore);
		frostWalkerMeta.addStoredEnchant(Enchantment.FROST_WALKER, 1, true);
		Items.frostWalker.setItemMeta(frostWalkerMeta);
		Items.tradeItems.add(Items.frostWalker);
		
		
		Items.windowBlack = NbtFactory.getCraftItemStack(new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 0, (byte) 15));
		Items.windowBlackComp = NbtFactory.fromItemTag(Items.windowBlack);
		Items.windowBlackComp.putPath("display.Name", " ");
		
		Items.windowBlue = NbtFactory.getCraftItemStack(new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 0, (byte) 11));
		Items.windowBlueComp = NbtFactory.fromItemTag(Items.windowBlue);
		Items.windowBlueComp.putPath("display.Name", "§9§l►►►");
		//Items.windowBlueComp.putPath("display.Lore", NbtFactory.createList("§9§l►►►"));

		Items.windowRed = NbtFactory.getCraftItemStack(new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 0, (byte) 14));
		Items.windowRedComp = NbtFactory.fromItemTag(Items.windowRed);
		Items.windowRedComp.putPath("display.Name", "§c§l◄◄◄");
		//Items.windowRedComp.putPath("display.Lore", NbtFactory.createList("§c§l◄◄◄"));
		
		Items.windowGreen = NbtFactory.getCraftItemStack(new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 0, (byte) 5));
		Items.windowGreenComp = NbtFactory.fromItemTag(Items.windowGreen);
		Items.windowGreenComp.putPath("display.Name", "§2§l§nACHETER");



		Items.price_1 = NbtFactory.getCraftItemStack(new ItemStack(Material.GOLD_INGOT, 1));
		Items.price_1Comp = NbtFactory.fromItemTag(Items.price_1);
		Items.price_1Comp.putPath("display.Name", "§b§l§omoney");
		Items.price_1Comp.putPath("PickupDelay.", pickupdelay);
		Items.price_1Comp.putPath("Age.", age);
		ItemMeta price_1Meta = Items.price_1.getItemMeta();
		price_1Meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		price_1Meta.addItemFlags(ItemFlag.HIDE_DESTROYS);
		price_1Meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
		Items.price_1.setAmount(1);
		Items.price_1.setItemMeta(price_1Meta);

		Items.price_2 = NbtFactory.getCraftItemStack(new ItemStack(Material.IRON_INGOT, 1));
		Items.price_2Comp = NbtFactory.fromItemTag(Items.price_2);
		Items.price_2Comp.putPath("display.Name", "§c§l§omoney");
		Items.price_2Comp.putPath("PickupDelay.", pickupdelay);
		Items.price_2Comp.putPath("Age.", age);
		ItemMeta price_2Meta = Items.price_2.getItemMeta();
		price_2Meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		price_2Meta.addItemFlags(ItemFlag.HIDE_DESTROYS);
		price_2Meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
		Items.price_2.setAmount(1);
		Items.price_2.setItemMeta(price_2Meta);

		Items.price_3 = NbtFactory.getCraftItemStack(new ItemStack(Material.REDSTONE, 1));
		Items.price_3Comp = NbtFactory.fromItemTag(Items.price_3);
		Items.price_3Comp.putPath("display.Name", "§2§l§omoney");
		Items.price_3Comp.putPath("PickupDelay.", pickupdelay);
		Items.price_3Comp.putPath("Age.", age);
		ItemMeta price_3Meta = Items.price_3.getItemMeta();
		price_3Meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		price_3Meta.addItemFlags(ItemFlag.HIDE_DESTROYS);
		price_3Meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
		Items.price_3.setAmount(1);
		Items.price_3.setItemMeta(price_3Meta);

		Items.price_4 = NbtFactory.getCraftItemStack(new ItemStack(Material.DIAMOND, 1));
		Items.price_4Comp = NbtFactory.fromItemTag(Items.price_4);
		Items.price_4Comp.putPath("display.Name", "§d§l§omoney");
		Items.price_4Comp.putPath("PickupDelay.", pickupdelay);
		Items.price_4Comp.putPath("Age.", age);
		ItemMeta price_4Meta = Items.price_4.getItemMeta();
		price_4Meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		price_4Meta.addItemFlags(ItemFlag.HIDE_DESTROYS);
		price_4Meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
		Items.price_4.setAmount(1);
		Items.price_4.setItemMeta(price_4Meta);

		int bcl1 = 0;
		ItemStack itembcl;
		ItemMeta itemetabcl;

		Items.helmet = NbtFactory.getCraftItemStack(new ItemStack(Material.LEATHER_HELMET));
		Items.helmetComp = NbtFactory.fromItemTag(Items.helmet);
		Items.helmetComp.putPath("display.Name", "§c§l►§r§c§lcasque§r§c§l◄");
		Items.helmetComp.putPath("Unbreakable.", 1);
		Items.helmetComp.putPath("PickupDelay.", pickupdelay);
		Items.helmetComp.putPath("Age.", age);
		ItemMeta helmetMeta = Items.helmet.getItemMeta();
		Items.helmet.setItemMeta(helmetMeta);
		Items.tradeItems.add(Items.helmet);

		Items.legg = NbtFactory.getCraftItemStack(new ItemStack(Material.LEATHER_LEGGINGS));
		Items.leggComp = NbtFactory.fromItemTag(Items.legg);
		Items.leggComp.putPath("display.Name", "§c§l►§r§c§lpantalon§r§c§l◄");
		Items.leggComp.putPath("Unbreakable.", 1);
		Items.leggComp.putPath("PickupDelay.", pickupdelay);
		Items.leggComp.putPath("Age.", age);
		Items.tradeItems.add(Items.legg);

		Items.boot = NbtFactory.getCraftItemStack(new ItemStack(Material.LEATHER_BOOTS));
		Items.bootComp = NbtFactory.fromItemTag(Items.boot);
		Items.bootComp.putPath("display.Name", "§c§l►§r§c§lchaussures§r§c§l◄");
		Items.bootComp.putPath("Unbreakable.", 1);
		Items.bootComp.putPath("PickupDelay.", pickupdelay);
		Items.bootComp.putPath("Age.", age);
		Items.tradeItems.add(Items.boot);

		while (bcl1 < Items.tradeItems.size()) {

			itembcl = Items.tradeItems.get(bcl1);
			itemetabcl = itembcl.getItemMeta();
			itemetabcl.addItemFlags(ItemFlag.HIDE_DESTROYS);
			itemetabcl.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
			itembcl.setItemMeta(itemetabcl);

			bcl1 ++;
		}



	}
	
}
