package com.masterpik.rush.confy;

import com.masterpik.connect.enums.StatsPiks;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;

public class RushParty {

    ArrayList<Location> spawners;

    ArrayList<Location> traders1;
    ArrayList<Location> traders2;
    ArrayList<Location> traders3;
    ArrayList<Location> traders4;
    HashMap<String, ArrayList> tradersConverter;

    ArrayList<Player> players;

    boolean start;
    boolean end;
    boolean isForced;
    boolean win;

    HashMap<Byte, RushPartyBase> bases;

    byte type;
    byte tip;
    byte part;

    byte winner;

    long startTime;
    long endTime;

    StatsPiks statsPiks;

    public RushParty(byte Ptype, byte Ptip, byte Ppart, String trader1, String trader2, String trader3, String trader4) {

        this.spawners = new ArrayList<Location>();

        this.traders1 = new ArrayList<Location>();
        this.traders2 = new ArrayList<Location>();
        this.traders3 = new ArrayList<Location>();
        this.traders4 = new ArrayList<Location>();

        this.tradersConverter = new HashMap<String, ArrayList>();

        this.tradersConverter.put(trader1, this.traders1);
        this.tradersConverter.put(trader2, this.traders2);
        this.tradersConverter.put(trader3, this.traders3);
        this.tradersConverter.put(trader4, this.traders4);

        this.players = new ArrayList<Player>();

        this.start = false;
        this.end = false;
        this.isForced = false;
        this.win = false;

        this.bases = new HashMap<Byte, RushPartyBase>();

        this.type = Ptype;
        this.tip = Ptip;
        this.part = Ppart;

        this.winner = -1;

        this.startTime = 0;
        this.endTime = 0;

        switch (type) {
            case ((byte)1):
                statsPiks = StatsPiks.RUSH_AIR;
                break;
            case ((byte)2) :
                statsPiks = StatsPiks.RUSH_MOUNTAIN;
                break;
            case ((byte)3) :
                statsPiks = StatsPiks.RUSH_WATER;
                break;
            default:
                break;
        }
    }

    public ArrayList<Location> getSpawners() {
        return spawners;
    }
    public void addSpawner(Location location) {
        spawners.add(location.clone());
    }
    public void removeSpawner(Location location) {
        spawners.remove(location);
    }
    public void setSpawners(ArrayList<Location> spawners) {
        this.spawners = spawners;
    }

    public void addTrader(String type, Location location) {
        tradersConverter.get(type).add(location.clone());
    }
    public ArrayList<Location> getTraders(String type) {
        return this.tradersConverter.get(type);
    }

    public ArrayList<Location> getTraders1() {
        return traders1;
    }
    public void addTrader1(Location location) {
        traders1.add(location.clone());
    }
    public void removeTrader1(Location location) {
        traders1.remove(location);
    }
    public void setTraders1(ArrayList<Location> traders1) {
        this.traders1 = traders1;
    }

    public ArrayList<Location> getTraders2() {
        return traders2;
    }
    public void addTrader2(Location location) {
        traders2.add(location.clone());
    }
    public void removeTrader2(Location location) {
        traders2.remove(location);
    }
    public void setTraders2(ArrayList<Location> traders2) {
        this.traders2 = traders2;
    }

    public ArrayList<Location> getTraders3() {
        return traders3;
    }
    public void addTrader3(Location location) {
        traders3.add(location.clone());
    }
    public void removeTrader3(Location location) {
        traders3.remove(location);
    }
    public void setTraders3(ArrayList<Location> traders3) {
        this.traders3 = traders3;
    }

    public ArrayList<Location> getTraders4() {
        return traders4;
    }
    public void addTrader4(Location location) {
        traders4.add(location.clone());
    }
    public void removeTrader4(Location location) {
        traders4.remove(location);
    }
    public void setTraders4(ArrayList<Location> traders4) {
        this.traders4 = traders4;
    }

    public ArrayList<Player> getPlayers() {
        return players;
    }
    public void addPlayer(Player player) {
        players.add(player);
    }
    public void removePlayer(Player player) {
        players.remove(player);
    }
    public void setPlayers(ArrayList<Player> players) {
        this.players = players;
    }

    public boolean isStart() {
        return start;
    }

    public void setStart(boolean Pstart) {
        this.start = Pstart;
    }

    public boolean isEnd() {
        return end;
    }

    public void setEnd(boolean Pend) {
        this.end = Pend;
    }

    public void addBase(byte color) {
        this.bases.put(color, new RushPartyBase());
    }
    public RushPartyBase getBase(byte color) {
        return this.bases.get(color);
    }

    public boolean isForced() {
        return isForced;
    }

    public void setIsForced(boolean PisForced) {
        this.isForced = PisForced;
    }

    public byte getType() {
        return type;
    }

    public byte getTip() {
        return tip;
    }

    public byte getPart() {
        return part;
    }


    public byte getWinner() {
        return winner;
    }
    public void setWinner(byte winner) {
        this.winner = winner;
    }

    public long getStartTime() {
        return startTime;
    }
    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public boolean isWin() {
        return win;
    }

    public void setWin(boolean win) {
        this.win = win;
    }

    public HashMap<String, ArrayList> getTradersConverter() {
        return tradersConverter;
    }

    public HashMap<Byte, RushPartyBase> getBases() {
        return bases;
    }

    public StatsPiks getStatsPiks() {
        return statsPiks;
    }
}
