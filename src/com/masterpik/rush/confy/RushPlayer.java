package com.masterpik.rush.confy;

import com.masterpik.api.util.location.Yaws;
import com.masterpik.rush.Settings;

public class RushPlayer {

    String party;
    byte type;
    byte tip;
    byte part;
    int nb;
    byte color;
    boolean isDied;
    boolean freeze;


    int kills;
    int deaths;
    int beds_break;

    Yaws respawnYaw;

    public RushPlayer(byte Ptype, byte Ptip, byte Ppart, int Pnb, byte Pcolor, boolean PisDied, boolean Pfreeze) {
        this.party = Settings.nextParty(Ptype, Ptip, Ppart, Pnb);
        this.type = Ptype;
        this.tip = Ptip;
        this.part = Ppart;
        this.nb = Pnb;
        this.color = Pcolor;
        this.isDied = PisDied;
        this.freeze = Pfreeze;


        this.kills = 0;
        this.deaths = 0;
        this.beds_break = 0;

        this.loadRespawnYaw();

    }

    public void loadRespawnYaw() {
        if (this.tip == (byte) 1) {

            switch (this.color) {

                case (byte)14:
                    respawnYaw = Yaws.WEST;
                    break;

                case (byte) 11:
                    respawnYaw = Yaws.EAST;
                    break;

                default:
                    respawnYaw = Yaws.SOUTH;
                    break;
            }

        }
        else if (this.tip == (byte) 2) {

            switch (this.color) {

                case (byte)14:
                    respawnYaw = Yaws.WEST;
                    break;

                case (byte) 5:
                    respawnYaw = Yaws.NORTH;
                    break;

                case (byte) 4:
                    respawnYaw = Yaws.EAST;
                    break;

                case (byte) 11:
                    respawnYaw = Yaws.SOUTH;
                    break;

                default:
                    respawnYaw = Yaws.SOUTH;
                    break;
            }

        }
        else if (this.tip == (byte) 3) {

            switch (this.color) {

                case (byte)14:
                    respawnYaw = Yaws.WEST;
                    break;

                case (byte) 5:
                    respawnYaw = Yaws.NORTH;
                    break;

                case (byte) 4:
                    respawnYaw = Yaws.EAST;
                    break;

                case (byte) 11:
                    respawnYaw = Yaws.SOUTH;
                    break;

                case (byte) 15:
                    respawnYaw = Yaws.SOUTH;
                    break;

                case (byte) 2:
                    respawnYaw = Yaws.WEST;
                    break;

                case (byte) 8:
                    respawnYaw = Yaws.NORTH;
                    break;

                case (byte) 1:
                    respawnYaw = Yaws.EAST;
                    break;

                default:
                    respawnYaw = Yaws.SOUTH;
                    break;
            }
        }
    }

    /**
     * if (tip == (byte) 1) {

     colors.add((byte)14);
     colors.add((byte)11);


     }
     else if (tip == (byte) 2) {

     colors.add((byte)14);
     colors.add((byte)5);
     colors.add((byte)4);
     colors.add((byte)11);

     }
     else if (tip == (byte) 3) {

     colors.add((byte)14);
     colors.add((byte)5);
     colors.add((byte)4);
     colors.add((byte)11);
     colors.add((byte)15);
     colors.add((byte)2);
     colors.add((byte)8);
     colors.add((byte)1);
     }
     *
     */





    public String getParty() {
        return party;
    }

    public void setParty(String Pparty) {
        this.party = Pparty;
    }

    public byte getType() {
        return type;
    }

    public void setType(byte Ptype) {
        this.type = Ptype;
    }

    public byte getTip() {
        return tip;
    }

    public void setTip(byte Ptip) {
        this.tip = Ptip;
    }

    public byte getPart() {
        return part;
    }

    public void setPart(byte Ppart) {
        this.part = Ppart;
    }

    public int getNb() {
        return nb;
    }

    public void setNb(int Pnb) {
        this.nb = Pnb;
    }

    public byte getColor() {
        return color;
    }

    public void setColor(byte Pcolor) {
        this.color = Pcolor;
        this.loadRespawnYaw();
    }

    public boolean isDied() {
        return isDied;
    }

    public void setIsDied(boolean PisDied) {
        this.isDied = PisDied;
    }

    public boolean isFreeze() {
        return freeze;
    }

    public void setFreeze(boolean Pfreeze) {
        this.freeze = Pfreeze;
    }


    public int getKills() {
        return kills;
    }
    public void setKills(int kills) {
        this.kills = kills;
    }
    public int getDeaths() {
        return deaths;
    }
    public void setDeaths(int deaths) {
        this.deaths = deaths;
    }
    public int getBeds_break() {
        return beds_break;
    }
    public void setBeds_break(int beds_break) {
        this.beds_break = beds_break;
    }

    public Yaws getRespawnYaw() {
        return respawnYaw;
    }

    public void setRespawnYaw(Yaws respawnYaw) {
        this.respawnYaw = respawnYaw;
    }
}
