package com.masterpik.rush.confy;

import com.masterpik.api.util.location.Yaws;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class RushPartyBase {

    ArrayList<Player> players;

    ArrayList<Location> banners;

    Location beacon;

    boolean bedStatu;
    Location bed;

    Yaws respawnYaw;

    public RushPartyBase() {

        this.players = new ArrayList<Player>();
        this.banners = new ArrayList<Location>();

        this.beacon = null;

        this.bedStatu = false;

        this.bed = null;

    }

    public ArrayList<Player> getPlayers() {
        return players;
    }
    public void addPlayer(Player player){
        players.add(player);
    }
    public void removePlayer(Player player) {
        players.remove(player);
    }
    public void setPlayers(ArrayList<Player> players) {
        this.players = players;
    }

    public ArrayList<Location> getBanners() {
        return banners;
    }
    public void addBanner(Location location){
        banners.add(location.clone());
    }
    public void removeBanner(Location location) {
        banners.remove(location);
    }
    public void setBanners(ArrayList<Location> banners) {
        this.banners = banners;
    }

    public Location getBeacon() {
        return beacon;
    }

    public void setBeacon(Location location) {
        this.beacon = location.clone();
    }

    public boolean isBedStatu() {
        return bedStatu;
    }

    public void setBedStatu(boolean PbedStatu) {
        this.bedStatu = PbedStatu;
    }

    public Location getBed() {
        return bed;
    }

    public void setBed(Location location) {
        this.bed = location.clone();
    }
}
