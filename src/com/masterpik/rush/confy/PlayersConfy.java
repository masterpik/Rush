package com.masterpik.rush.confy;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.HashMap;

public class PlayersConfy {

	public static HashMap<Player, RushPlayer> RushPlayers;

	public static void playerInit() {

		RushPlayers = new HashMap<Player, RushPlayer>();

		/*File file = new File(Settings.playerConfigPath);
		
		if (!file.exists()) {
			
			FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);
			
			fileConfig.createSection("Players");
			
			try {fileConfig.save(file);
			Bukkit.getLogger().info(Messages.newFile);
			} catch (IOException e1) {e1.printStackTrace();}
			
		}*/
		
	}
	
	public static void addPlayer(Player player, byte type, byte tip, byte part, int nb, byte color) {

		RushPlayers.put(player, new RushPlayer(type, tip, part, nb, color, false, false));

		/*File file = new File(Settings.playerConfigPath);
		FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);
		
		fileConfig.createSection("Players." + player.getName());
		fileConfig.set("Players."+player.getName()+".party", Settings.nextParty(type, tip, part, nb));
		fileConfig.set("Players."+player.getName()+".type", type);
		fileConfig.set("Players."+player.getName()+".tip", tip);
		fileConfig.set("Players."+player.getName()+".part", part);
		fileConfig.set("Players."+player.getName()+".nb", nb);
		fileConfig.set("Players."+player.getName()+".color", color);
		fileConfig.set("Players."+player.getName()+".isDied", false);
		fileConfig.set("Players."+player.getName()+".freeze", false);

		try {fileConfig.save(file);
		Bukkit.getLogger().info(player.getName() + " ajout�");
		} catch (IOException e1) {e1.printStackTrace();}*/
		
	}
	
	public static void deletePlayer(Player player) {

		if (RushPlayers.containsKey(player)) {
			RushPlayers.remove(player);
		}

		/*File file = new File(Settings.playerConfigPath);
		FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);
		
		fileConfig.set("Players." + player.getName(), null);
		
		try {fileConfig.save(file);
		Bukkit.getLogger().info(player.getName() + " effac�");
		} catch (IOException e1) {e1.printStackTrace();}*/
	}

	
	public static int getType(Player player) {

		if (RushPlayers.containsKey(player)) {
			return (int) RushPlayers.get(player).getType();
		} else {
			return -1;
		}

		/*File file = new File(Settings.playerConfigPath);
		FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);
		
		int type = fileConfig.getInt("Players."+player.getName()+".type");
		
		return type;*/
	}

	public static int getTip(Player player) {

		if (RushPlayers.containsKey(player)) {
			return (int) RushPlayers.get(player).getTip();
		} else {
			return -1;
		}

		/*File file = new File(Settings.playerConfigPath);
		FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);

		int tip = fileConfig.getInt("Players."+player.getName()+".tip");

		return tip;*/
	}

	public static int getPart(Player player) {

		if (RushPlayers.containsKey(player)) {
			return (int) RushPlayers.get(player).getPart();
		} else {
			return -1;
		}

		/*File file = new File(Settings.playerConfigPath);
		FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);

		int part = fileConfig.getInt("Players."+player.getName()+".part");

		return part;*/
	}

	public static int getNb(Player player) {

		if (RushPlayers.containsKey(player)) {
			return RushPlayers.get(player).getNb();
		} else {
			return  -1;
		}

		/*File file = new File(Settings.playerConfigPath);
		FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);

		int nb = fileConfig.getInt("Players."+player.getName()+".nb");

		return nb;*/
	}

	public static byte getColor(Player player) {

		if (RushPlayers.containsKey(player)) {
			return RushPlayers.get(player).getColor();
		} else {
			return -1;
		}

		/*File file = new File(Settings.playerConfigPath);
		FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);

		byte color = (byte) fileConfig.getInt("Players."+player.getName()+".color");

		return color;*/
	}

	public static String getParty(Player player) {

		if (player == null) {
			return "";
		}

		if (RushPlayers.containsKey(player)) {

			if (RushPlayers.get(player).getParty() != null) {

				return RushPlayers.get(player).getParty();
			} else {
				return "";
			}
		} else {
			return "";
		}

		/*File file = new File(Settings.playerConfigPath);
		FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);

		String party = Settings.nextParty(
				(byte) PlayersConfy.getType(player),
				(byte) PlayersConfy.getTip(player),
				(byte) PlayersConfy.getPart(player),
				PlayersConfy.getNb(player));

		return party;*/
	}

	public static boolean getDiedStatu(Player player) {

		if (RushPlayers.containsKey(player)) {
			return RushPlayers.get(player).isDied;
		} else {
			return false;
		}

		/*File file = new File(Settings.playerConfigPath);
		FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);

		boolean died = fileConfig.getBoolean("Players." + player.getName() + ".isDied");

		return died;*/
	}

	public static boolean getFreezeStatu(Player player) {

		if (RushPlayers.containsKey(player)) {
			return RushPlayers.get(player).isFreeze();
		} else {
			return false;
		}

		/*File file = new File(Settings.playerConfigPath);
		FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);

		boolean freeze = fileConfig.getBoolean("Players." + player.getName() + ".freeze");

		return freeze;*/
	}

	public static void setDiedStatu(Player player, boolean died) {

		if (RushPlayers.containsKey(player)) {
			RushPlayers.get(player).setIsDied(died);
		}

		/*File file = new File(Settings.playerConfigPath);
		FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);

		fileConfig.set("Players."+player.getName()+".isDied", died);

		try {fileConfig.save(file);} catch (IOException e1) {e1.printStackTrace();}*/
	}

	public static void setFreezeStatu(Player player, boolean freeze) {

		if (RushPlayers.containsKey(player)) {
			if (RushPlayers.get(player) != null) {
				RushPlayers.get(player).setFreeze(freeze);
			}
		}

		/*File file = new File(Settings.playerConfigPath);
		FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);

		fileConfig.set("Players."+player.getName()+".freeze", freeze);

		try {fileConfig.save(file);} catch (IOException e1) {e1.printStackTrace();}*/
	}

	public static HashMap<String, Inventory> chest = new HashMap();

	public static boolean isRegister(Player player) {

		/*File file = new File(Settings.playerConfigPath);
		FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);*/

		boolean exist = false;

		if (RushPlayers != null
				&& RushPlayers.containsKey(player)) {
			exist = true;
		}

		return exist;
	}


}
