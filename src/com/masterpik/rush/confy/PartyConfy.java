package com.masterpik.rush.confy;

import com.masterpik.rush.Settings;
import com.masterpik.rush.Traders;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;

public class PartyConfy {

	public static ArrayList<Location> spawners;
	public static ArrayList<String> partyNotStart;
	public static HashMap<String, RushParty> RushPartys;


	//----------Initialization----------
	
	public static void partyInit() {

		RushPartys = new HashMap<String, RushParty>();
		spawners = new ArrayList<Location>();
		partyNotStart = new ArrayList<String>();

		/*File file = new File(Settings.partyConfigPath);
		
		if (!file.exists()) {
			
			FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);
			
			fileConfig.createSection("Party");
			
			try {fileConfig.save(file);
			Bukkit.getLogger().info(Messages.newFile);
			} catch (IOException e1) {e1.printStackTrace();}
			
		}*/
		
	}

	//----------Initialization----------
	//----------Adding----------
	
	public static int addParty(byte type, byte tip, byte part) {

		/*File file = new File(Settings.partyConfigPath);
		FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);*/

		int nb = PartyConfy.getNext(type, tip, part);
		String next = Settings.nextParty(type, tip, part, nb);

		//fileConfig.createSection("Party." + next + "");

		RushPartys.put(next, new RushParty(type, tip, part, Traders.trader__1_main_name, Traders.trader__2_main_name, Traders.trader__3_main_name, Traders.trader__4_main_name));
		RushPartys.get(next).setIsForced(false);
		RushPartys.get(next).setEnd(false);
		RushPartys.get(next).setStart(false);


		/*fileConfig.createSection("Party." + next + ".spawners.list");
		ArrayList spawnerList = new ArrayList();
		spawnerList.add("null");
		fileConfig.set("Party." + next + ".spawners.list", spawnerList);*/
		
		
		/*ArrayList tradersList = new ArrayList();
		tradersList.add("null");
		fileConfig.createSection("Party." + next + ".traders." + Traders.trader__1_main_name + ".list");
		fileConfig.set("Party." + next + ".traders." + Traders.trader__1_main_name + ".list", tradersList);
		fileConfig.createSection("Party." + next + ".traders." + Traders.trader__2_main_name + ".list");
		fileConfig.set("Party." + next + ".traders." + Traders.trader__2_main_name + ".list", tradersList);
		fileConfig.createSection("Party." + next + ".traders." + Traders.trader__3_main_name + ".list");
		fileConfig.set("Party." + next + ".traders." + Traders.trader__3_main_name + ".list", tradersList);
		fileConfig.createSection("Party." + next + ".traders." + Traders.trader__4_main_name + ".list");
		fileConfig.set("Party." + next + ".traders." + Traders.trader__4_main_name + ".list", tradersList);*/

		/*fileConfig.createSection("Party." + next + ".players");
		ArrayList playersList = new ArrayList();
		playersList.add("null");
		fileConfig.set("Party." + next + ".players", playersList);*/

		/*fileConfig.createSection("Party." + next + ".start");
		fileConfig.set("Party." + next + ".start", false);*/

		/*fileConfig.createSection("Party." + next + ".end");
		fileConfig.set("Party." + next + ".end", false);*/

		PartyConfy.partyNotStart.add(next);
		
		//try {fileConfig.save(file);} catch (IOException e1) {e1.printStackTrace();}

		return nb;
	}

	public static void addBed(String party, byte color, Location bedLocation) {

		RushPartys.get(party).getBase(color).setBedStatu(false);
		RushPartys.get(party).getBase(color).setBed(bedLocation);

		//Bukkit.getLogger().info("lele");

		/*File file = new File(Settings.partyConfigPath);
		FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);
		
		fileConfig.set("Party."+party+".bases."+color+".bed.isbroken", false);
		
		fileConfig.set("Party."+party+".bases."+color+".bed.x", bedLocation.getBlockX() );
		fileConfig.set("Party."+party+".bases."+color+".bed.y", bedLocation.getBlockY() );
		fileConfig.set("Party."+party+".bases."+color+".bed.Z", bedLocation.getBlockZ() );
		
		try {fileConfig.save(file);} catch (IOException e1) {e1.printStackTrace();}*/
	}
	
	public static void addSpawner(String party, Location spawnerLocation) {
		/*File file = new File(Settings.partyConfigPath);
		FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);*/

		PartyConfy.spawners.add(spawnerLocation);

		RushPartys.get(party).addSpawner(spawnerLocation);

		/*String loc = (String) Settings.locationToString(spawnerLocation);
		
		ArrayList<String> list = new ArrayList<String>();
		list = (ArrayList<String>) PartyConfy.getSpawners(party);
		
		list.add((String) loc );
		
		if (list.contains("null")) {
			list.remove("null");
		}
		
		fileConfig.set("Party."+party+".spawners.list", list);
		
		
		fileConfig.createSection("Party."+party+".spawners."+loc);
		fileConfig.set("Party."+party+".spawners."+loc+".x", spawnerLocation.getBlockX());
		fileConfig.set("Party."+party+".spawners."+loc+".y", spawnerLocation.getBlockY());
		fileConfig.set("Party."+party+".spawners."+loc+".z", spawnerLocation.getBlockZ());
		

		try {fileConfig.save(file);} catch (IOException e1) {e1.printStackTrace();}*/
	}
	
	public static void addBanner(String party, Byte color, Location bannerLocation) {

		RushPartys.get(party).getBase(color).addBanner(bannerLocation);

		/*File file = new File(Settings.partyConfigPath);
		FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);
		
		String loc = (String) Settings.locationToString(bannerLocation);
		
		ArrayList<String> list = new ArrayList<String>();
		list = (ArrayList<String>) PartyConfy.getBanners(party, color);
		
		list.add((String) loc );
		
		if (list.contains("null")) {
			list.remove("null");
		}
		
		fileConfig.set("Party."+party+".bases."+color+".banners.list", list);
		
		
		fileConfig.createSection("Party."+party+".bases."+color+".banners."+loc);
		fileConfig.set("Party."+party+".bases."+color+".banners."+loc+".x", bannerLocation.getBlockX());
		fileConfig.set("Party."+party+".bases."+color+".banners."+loc+".y", bannerLocation.getBlockY());
		fileConfig.set("Party."+party+".bases."+color+".banners."+loc+".z", bannerLocation.getBlockZ());
		
		try {fileConfig.save(file);} catch (IOException e1) {e1.printStackTrace();}*/
	}
	
	public static void addTrader(String party, Location tradersLocation, String type) {

		RushPartys.get(party).addTrader(type, tradersLocation);

		/*File file = new File(Settings.partyConfigPath);
		FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);
		
		String loc = (String) Settings.locationToString(tradersLocation);
		
		ArrayList<String> list =  PartyConfy.getTraders(party, type);
		
		list.add(loc);
		
		if (list.contains("null")) {
			list.remove("null");
		}
		
		fileConfig.set("Party."+party+".traders."+type+".list", list);
		
		
		fileConfig.createSection("Party."+party+".traders."+type+"."+loc);
		fileConfig.set("Party."+party+".traders."+type+"."+loc+".x", tradersLocation.getBlockX());
		fileConfig.set("Party."+party+".traders."+type+"."+loc+".y", tradersLocation.getBlockY());
		fileConfig.set("Party."+party+".traders."+type+"."+loc+".z", tradersLocation.getBlockZ());
		
		try {fileConfig.save(file);} catch (IOException e1) {e1.printStackTrace();}*/
	}
	
	public static void addBeacon(String party, Byte color, Location beaconLocation) {

		RushPartys.get(party).getBase(color).setBeacon(beaconLocation);

		/*File file = new File(Settings.partyConfigPath);
		FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);
		
		fileConfig.set("Party."+party+".bases."+color+".beacon.x", beaconLocation.getBlockX());
		fileConfig.set("Party."+party+".bases."+color+".beacon.y", beaconLocation.getBlockY());
		fileConfig.set("Party."+party+".bases."+color+".beacon.z", beaconLocation.getBlockZ());
		
		
		try {fileConfig.save(file);} catch (IOException e1) {e1.printStackTrace();}*/
	}

	public static void addPlayer(Player player) {
		/*File file = new File(Settings.partyConfigPath);
		FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);*/

		String party = PlayersConfy.getParty(player);
		byte color = PlayersConfy.getColor(player);

		RushPartys.get(party).addPlayer(player);
		RushPartys.get(party).getBase(color).addPlayer(player);

		/*String playerName = player.getName();
		ArrayList<String> listTeam = PartyConfy.getPlayersTeam(party, color);
		listTeam.add(playerName);
		if (listTeam.contains("null")) {
			listTeam.remove("null");
		}
		fileConfig.set("Party."+party+".bases."+color+".players", listTeam);

		ArrayList<String> list = PartyConfy.getPlayers(party);
		list.add(playerName);
		if (list.contains("null")) {
			list.remove("null");
		}
		fileConfig.set("Party."+party+".players", list);*/

		//try {fileConfig.save(file);} catch (IOException e1) {e1.printStackTrace();}
	}

	//----------Adding----------
	//----------Creations----------

	public static void createBase(String party, byte color) {

		RushPartys.get(party).addBase(color);
		RushPartys.get(party).getBase(color).setBedStatu(false);

		/*File file = new File(Settings.partyConfigPath);
		FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);

		fileConfig.createSection("Party."+party+".bases."+color);

		fileConfig.createSection("Party."+party+".bases."+color+".players");
		ArrayList playerList = new ArrayList();
		playerList.add("null");
		fileConfig.set("Party." + party + ".bases." + color + ".players", playerList);

		fileConfig.createSection("Party." + party + ".bases." + color + ".banners.list");
		ArrayList bannersList = new ArrayList();
		bannersList.add("null");
		fileConfig.set("Party." + party + ".bases." + color + ".banners.list", bannersList);

		fileConfig.createSection("Party." + party + ".bases." + color + ".beacon");

		try {fileConfig.save(file);} catch (IOException e1) {e1.printStackTrace();}*/

	}

	//----------Creations----------
	//----------Setting----------
	
	public static void setBedStatu(String party, Byte color, boolean isBroken) {
		/*File file = new File(Settings.partyConfigPath);
		FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);*/
		
		//Location locDown = location.getBlock().getRelative(BlockFace.DOWN).getLocation();
		//Byte color = locDown.getBlock().getData();

		RushPartys.get(party).getBase(color).setBedStatu(isBroken);

		//fileConfig.set("Party."+party+".bases."+color+".bed.isbroken", isBroken);
		
		//try {fileConfig.save(file);} catch (IOException e1) {e1.printStackTrace();}
	}

	public static void setStartStatu(String party, boolean start) {

		RushPartys.get(party).setStart(start);

		/*File file = new File(Settings.partyConfigPath);
		FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);

		fileConfig.set("Party."+party+".start", start);

		try {fileConfig.save(file);} catch (IOException e1) {e1.printStackTrace();}*/
	}

	public static void setEndStatu(String party, boolean start) {

		RushPartys.get(party).setEnd(start);

		/*File file = new File(Settings.partyConfigPath);
		FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);

		fileConfig.set("Party."+party+".end", start);

		try {fileConfig.save(file);} catch (IOException e1) {e1.printStackTrace();}*/
	}

	//----------Setting----------
	//----------Removing----------
	
	public static void removeParty(String next) {

		/*File file = new File(Settings.partyConfigPath);
		FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);*/

		/*Location locationSpawner;

		int bucle = 0;

		while (bucle < RushPartys.get(next).getSpawners().size()) {

			/*locationSpawner = new Location(Bukkit.getWorld(next),
					fileConfig.getDouble("Party."+next+".spawners."+
							fileConfig.getList("Party."+next+".spawners.list").get(bucle)+".x"),
					fileConfig.getDouble("Party."+next+".spawners."+
						fileConfig.getList("Party."+next+".spawners.list").get(bucle)+".y"),
					fileConfig.getDouble("Party."+next+".spawners."+
						fileConfig.getList("Party."+next+".spawners.list").get(bucle)+".z") );

			PartyConfy.spawners.remove(locationSpawner);

			bucle++;
		}*/

		PartyConfy.spawners.removeAll(RushPartys.get(next).getSpawners());

		RushPartys.remove(next);

		//fileConfig.set("Party."+next, null);
		
		//try {fileConfig.save(file);} catch (IOException e1) {e1.printStackTrace();}
		
	}

	public static void removePlayer(Player player) {
		/*File file = new File(Settings.partyConfigPath);
		FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);*/

		String party = PlayersConfy.getParty(player);

		byte color = PlayersConfy.getColor(player);

		RushPartys.get(party).getBase(color).removePlayer(player);

		/*String playerName = player.getName();

		ArrayList<String> listTeam = PartyConfy.getPlayersTeam(party, color);
		listTeam.remove(playerName);
		fileConfig.set("Party."+party+".bases."+color+".players", listTeam);*/

		//try {fileConfig.save(file);} catch (IOException e1) {e1.printStackTrace();}
	}

	public static void deletePlayer(Player player) {
		/*File file = new File(Settings.partyConfigPath);
		FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);*/

		String party = PlayersConfy.getParty(player);

		byte color = PlayersConfy.getColor(player);

		RushPartys.get(party).removePlayer(player);
		RushPartys.get(party).getBase(color).removePlayer(player);

		/*String playerName = player.getName();

		ArrayList<String> listTeam = PartyConfy.getPlayersTeam(party, color);
		listTeam.remove(playerName);
		fileConfig.set("Party."+party+".bases."+color+".players", listTeam);

		ArrayList<String> list = PartyConfy.getPlayers(party);
		list.remove(playerName);
		fileConfig.set("Party."+party+".players", list);

		try {fileConfig.save(file);} catch (IOException e1) {e1.printStackTrace();}*/
	}

	//----------Removing----------
	//----------Getting----------
	
	public static int getNext(byte type, byte tip, byte part) {
		
		String next;
		
		/*File file = new File(Settings.partyConfigPath);
		FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);*/
		
		int bucle = 1;
		boolean ok = false;
		
		while (ok == false) {
			
			next = Settings.nextParty(type, tip, part, bucle);
			
			if (PartyConfy.isCreated(next)) {
				bucle = bucle + 1;
			}
			else {
				ok = true;
			}
			
		}
		
		return bucle;
	}

	public static int getNow(byte type, byte tip, byte part) {
		/*File file = new File(Settings.partyConfigPath);
		FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);*/

		String party = "";

		int bucle = 1;
		boolean ok = false;
		boolean start;

		int bucle2 = 0;

		while (bucle2 < PartyConfy.partyNotStart.size()) {

			party = PartyConfy.partyNotStart.get(bucle2);

			if (PartyConfy.isCreated(party)
					&& !PartyConfy.getStart(party)
					&& PartyConfy.RushPartys.get(party).getType() == type
					&& PartyConfy.RushPartys.get(party).getTip() == tip
					&& PartyConfy.RushPartys.get(party).getPart() == part) {
				ok = true;
				return PartyConfy.getPartyNb(party);
			}
			else {
				bucle2 ++;
			}
		}


		while (ok == false) {

			party = Settings.nextParty(type, tip, part, bucle);

			if (!PartyConfy.isCreated(party)
					|| !PartyConfy.getStart(party)) {
				ok = true;
				return bucle;
			}
			else {
				bucle++;
			}
		}

		return 1;
	}

	public static boolean getBedStatu(String party, byte color) {

		return RushPartys.get(party).getBase(color).isBedStatu();

		/*File file = new File(Settings.partyConfigPath);
		FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);

		boolean statu = fileConfig.getBoolean("Party."+party+".bases."+color+".bed.isbroken");

		return statu;*/
	}

	public static boolean getStart(String party) {
		/*File file = new File(Settings.partyConfigPath);
		FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);*/

		boolean start;

		if (PartyConfy.isCreated(party)) {
			//start = fileConfig.getBoolean("Party." + party + ".start");
			start = RushPartys.get(party).isStart();
		}
		else {
			start = false;
		}

		return start;
	}

	public static boolean getEnd(String party) {
		/*File file = new File(Settings.partyConfigPath);
		FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);*/

		boolean end;

		if (PartyConfy.isCreated(party)) {
			//end = fileConfig.getBoolean("Party." + party + ".end");
			end = RushPartys.get(party).isEnd();
		}
		else {
			end = true;
		}

		return end;
	}

	/*public static Location getLocation(String path, String party) {
		File file = new File(Settings.partyConfigPath);
		FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);

		int x = fileConfig.getInt(""+path+".x");
		int y = fileConfig.getInt(""+path+".y");
		int z = fileConfig.getInt(""+path+".z");

		Location location = new Location(Bukkit.getWorld(party), (double)x, (double) y, (double) z);

		return location;
	}*/

	public static ArrayList<Location> getSpawners(String party) {

		return RushPartys.get(party).getSpawners();

		/*File file = new File(Settings.partyConfigPath);
		FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);
		
		ArrayList<String> list = (ArrayList) fileConfig.getList("Party."+party+".spawners.list");
		
		return list;*/
	}
	
	public static ArrayList<Location> getBanners(String party, Byte color) {

		return RushPartys.get(party).getBase(color).getBanners();

		/*File file = new File(Settings.partyConfigPath);
		FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);
		
		ArrayList<String> list = (ArrayList) fileConfig.getList("Party."+party+".bases."+color+".banners.list");
		
		return list;*/
	}
	
	public static ArrayList<Location> getTraders(String party, String type) {

		return RushPartys.get(party).getTraders(type);

		/*File file = new File(Settings.partyConfigPath);
		FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);

		ArrayList<String> list = (ArrayList) fileConfig.getList("Party."+party+".traders."+type+".list");
		
		return list;*/
	}

	public static ArrayList<Player> getPlayersTeam(String party, Byte color) {

		return RushPartys.get(party).getBase(color).getPlayers();

		/*File file = new File(Settings.partyConfigPath);
		FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);

		ArrayList<String> list = (ArrayList) fileConfig.getList("Party."+party+".bases."+color+".players");

		return list;*/
	}

	public static ArrayList<Player> getPlayers(String party) {

		return RushPartys.get(party).getPlayers();

		/*File file = new File(Settings.partyConfigPath);
		FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);

		ArrayList<String> list = (ArrayList) fileConfig.getList("Party."+party+".players");

		return list;*/
	}
	
	/*public static Location getTraderLocation(String party, String type, String stringLocation) {

		File file = new File(Settings.partyConfigPath);
		FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);
		
		int x = fileConfig.getInt("Party."+party+".traders."+type+"."+stringLocation+".x");
		int y = fileConfig.getInt("Party."+party+".traders."+type+"."+stringLocation+".y");
		int z = fileConfig.getInt("Party."+party+".traders."+type+"."+stringLocation+".z");
		
		Location location = new Location(Bukkit.getWorld(party), (double) x, (double) y, (double) z);
		
		return location;
	}*/

	public static Location getBeaconLocation(String party, Byte color) {

		return RushPartys.get(party).getBase(color).getBeacon();

		/*File file = new File(Settings.partyConfigPath);
		FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);

		int x = fileConfig.getInt("Party."+party+".bases."+color+".beacon.x");
		int y = fileConfig.getInt("Party."+party+".bases."+color+".beacon.y");
		int z = fileConfig.getInt("Party."+party+".bases."+color+".beacon.z");

		Location location = new Location(Bukkit.getWorld(party), (double) x, (double) y, (double) z );

		return location;*/
	}

	public static Location getBannerLocation(String party, Byte color, int nb) {

		ArrayList<Location> list = getBanners(party, color);

		//Location loc = list.get(nb);

		//String path = "Party."+party+".bases."+color+".banners."+loc+"";

		Location location =  list.get(nb)/*getLocation(path, party)*/;

		return location;
	}

	public static Location getBed(String party, Byte color) {

		Location location = RushPartys.get(party).getBase(color).getBed();

		return location;

		/*File file = new File(Settings.partyConfigPath);
		FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);

		Location location = new Location(
				Bukkit.getWorld(party),
				fileConfig.getDouble("Party."+party+".bases."+color+".bed.x"),
				fileConfig.getDouble("Party."+party+".bases."+color+".bed.y"),
				fileConfig.getDouble("Party."+party+".bases."+color+".bed.z"));

		return location;*/
	}

	public static Location getBedPlayer(Player player) {
		/*File file = new File(Settings.partyConfigPath);
		FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);*/

		String party = PlayersConfy.getParty(player);
		Byte color = PlayersConfy.getColor(player);

		Location location = PartyConfy.getBed(party, color);

		return location;
	}

	public static boolean isCreated(String party) {
		/*File file = new File(Settings.partyConfigPath);
		FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);*/

		boolean bool;

		if ( RushPartys.containsKey(party) /*fileConfig.contains("Party."+party)*/) {
			bool = true;
		}
		else {
			bool = false;
		}

		return bool;
	}

	public static String locationToParty(Location location) {

		String party =  location.getWorld().getName();

		return party;
	}

	public static int getPartyNb(String party) {
		int nb = 0;

		if (party.length() == 7) {
			nb = Integer.parseInt("" + party.charAt(6));
		} else if (party.length() == 8) {
			nb = Integer.parseInt(""+party.charAt(6)+""+party.charAt(7));
		} else if (party.length() == 9) {
			nb = Integer.parseInt(""+party.charAt(6)+""+party.charAt(7)+""+party.charAt(8));
		}

		return nb;
	}

	public static int getPartyTip(String party) {
		return PlayersConfy.getTip(PartyConfy.getPlayers(party).get(0));
	}
	public static int getPartyPart(String party) {
		return PlayersConfy.getPart(PartyConfy.getPlayers(party).get(0));
	}
	public static int getPartyType(String party) {
		return PlayersConfy.getType(PartyConfy.getPlayers(party).get(0));
	}

	public static String bedStatuToString(boolean bed) {
		String statu = "";

		if (bed) {
			statu = "§4§l✖";
		}
		else {
			statu = "§2§l✔";
		}

		return statu;
	}





	//----------Getting----------

	
}

