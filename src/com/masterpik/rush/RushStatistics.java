package com.masterpik.rush;

import com.masterpik.api.util.UtilDate;
import com.masterpik.api.util.UtilInteger;
import com.masterpik.connect.api.PlayersStatistics;
import com.masterpik.connect.enums.Statistics;
import com.masterpik.connect.enums.StatsPiks;
import com.masterpik.connect.enums.StatsPlayers;
import com.masterpik.rush.confy.PartyConfy;
import com.masterpik.rush.confy.PlayersConfy;
import com.masterpik.rush.confy.RushParty;
import com.masterpik.rush.confy.RushPlayer;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.UUID;

public class RushStatistics {

    public static void partyFinish(RushParty party) {

        /*long diff = party.getEndTime()-party.getStartTime();

        for (Player bPlayer : party.getPlayers()) {
            RushPlayer player = PlayersConfy.RushPlayers.get(bPlayer);

            PlayersStatistics.addSpentTime(bPlayer.getUniqueId(), diff, StatsPlayers.RUSH);

        }*/
    }

    public static ArrayList<Integer> getPiks(Player player) {
        if (PlayersConfy.RushPlayers.containsKey(player)) {
            RushPlayer Rplayer = PlayersConfy.RushPlayers.get(player);
            RushParty party = PartyConfy.RushPartys.get(Rplayer.getParty());
            ArrayList<Integer> piks = new ArrayList<>();
            piks.add(StatsPiks.PARTY.getPiks());
            piks.add(StatsPiks.RUSH.getPiks());
            piks.add(party.getStatsPiks().getPiks());
            piks.add(Settings.getFoi(party.getTip()));
            piks.add(11 - Settings.getNb(party.getPart()));
            return piks;
        } else {
            return new ArrayList<>();
        }
    }

    public static int getPiksWin(Player player, boolean winStatu) {
        return PlayersStatistics.getPikPointsWin(UtilInteger.addInts(getPiks(player)), winStatu);
    }

    public static int getPiksWin(Player player) {
        return UtilInteger.addInts(getPiks(player));
    }

    public static void playerQuit(Player player) {
        if (PlayersConfy.RushPlayers.containsKey(player)) {
            RushPlayer Rplayer = PlayersConfy.RushPlayers.get(player);
            RushParty party = PartyConfy.RushPartys.get(Rplayer.getParty());

            UUID uuid = player.getUniqueId();

            if (party.isStart()) {

                long diff;

                if (party.isWin()) {

                    diff = party.getEndTime() - party.getStartTime();


                    if (Rplayer.getColor() == party.getWinner()) {
                        PlayersStatistics.incrementStatistic(StatsPlayers.RUSH, uuid, Statistics.WIN_AMOUNT);
                        PlayersStatistics.addPikPoint(uuid, getPiksWin(player), StatsPlayers.RUSH, true);

                    } else {
                        PlayersStatistics.incrementStatistic(StatsPlayers.RUSH, uuid, Statistics.LOST_AMOUNT);
                        PlayersStatistics.addPikPoint(uuid, getPiksWin(player), StatsPlayers.RUSH, false);
                    }

                } else {

                    diff = UtilDate.getCurrentDate().getTime() - party.getStartTime();

                }
                PlayersStatistics.addSpentTime(uuid, diff, StatsPlayers.RUSH);


                PlayersStatistics.incrementStatistic(StatsPlayers.RUSH, uuid, Statistics.KILL_AMOUNT, Rplayer.getKills());
                PlayersStatistics.incrementStatistic(StatsPlayers.RUSH, uuid, Statistics.DEATH_AMOUNT, Rplayer.getDeaths());
                PlayersStatistics.incrementStatistic(StatsPlayers.RUSH, uuid, Statistics.BED_BREAK_AMOUNT, Rplayer.getBeds_break());


                PlayersStatistics.incrementStatistic(StatsPlayers.RUSH, uuid, Statistics.GAMES_AMOUNT);
            }
        }
    }

}
