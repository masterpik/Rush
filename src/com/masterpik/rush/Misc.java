package com.masterpik.rush;

import com.comphenix.protocol.wrappers.EnumWrappers;
import com.masterpik.api.json.ChatText;
import com.masterpik.api.nms.player.Title;
import com.masterpik.api.util.UtilChat;
import com.masterpik.api.util.UtilString;
import com.masterpik.api.util.UtilTitle;
import com.masterpik.rush.confy.PartyConfy;
import com.masterpik.rush.confy.PlayersConfy;
import com.masterpik.rush.confy.RushPlayer;
import net.minecraft.server.v1_9_R1.PacketPlayOutTitle;
import org.bukkit.*;
import org.bukkit.block.Banner;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.banner.PatternType;
import org.bukkit.entity.*;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.material.Bed;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.Vector;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;

public class Misc extends JavaPlugin{
	
	public static void placeBlock(Block block, Player player) {

		if (block.getLocation().getY() >= 254) {
			block.breakNaturally();
			return;
		}
		else if (block.getRelative(BlockFace.DOWN).getType().equals(Material.SEA_LANTERN)
				&& block.getRelative(BlockFace.UP).getType().equals(Material.QUARTZ_STAIRS)) {
			block.breakNaturally();
			return;
		}
		else if (block.getType() == Material.TNT) {

			//Bukkit.getLogger().info("A tnt has been place");

			Location cursor = block.getLocation();
			
			int bucle = 0;
			boolean continu = true;
			boolean isBlock = false;
			while (continu == true) {
				
				cursor.setY(cursor.getY()-1);
				
				if (!cursor.getBlock().getType().equals(Material.AIR)) {
					continu = false;
					isBlock = true;
				}
				else if (bucle == 200) {
					continu = false;
					isBlock = false;		
				} else {
					continu = true;
					isBlock = false;
				}
				
				bucle = bucle + 1;
			}
			
			Location location = block.getLocation();
			block.setType(Material.AIR);
			
			
			if (isBlock == true 
					&& Settings.TNTBreaker.contains(cursor.getBlock().getType()) ) {

				//Bukkit.getLogger().info("the tnt has been pos on a solid block, an explosion is begin");
				
				block.setType(Material.AIR);
				Misc.explode(location.getBlock().getLocation(), player.getName());
				return;

				
			}
			else {
				//Bukkit.getLogger().info("the tnt has been pos on a AIR block a falling sand tnt will spawn, the next solid block is to "+(location.getBlockY()-cursor.getBlockY()));

				FallingBlock tnt = location.getWorld().spawnFallingBlock(location, Material.TNT, (byte) 0);

				tnt.setMetadata("player", new FixedMetadataValue(TimeManagement.plugin,player.getName()));

				tnt.setDropItem(false);

				return;
				
			}
			
			
		} else if ((block.getType() != Material.BED_BLOCK || block.getType() != Material.BED) && !player.getGameMode().equals(GameMode.SPECTATOR)) {

			Block blockDown = block.getRelative(BlockFace.DOWN);
			Block blockDown2 = blockDown.getRelative(BlockFace.DOWN);

			if(blockDown2.getType().equals(Material.BED_BLOCK)
					|| blockDown.getType().equals(Material.BED_BLOCK)) {
				block.breakNaturally();
				return;
			} else {


				Location beaconNorm = new Location(block.getWorld(), block.getLocation().getX(), (double) 198, block.getLocation().getZ());
				Location beaconMont = new Location(block.getWorld(), block.getLocation().getX(), (double) 242, block.getLocation().getZ());
				Location beaconWat = new Location(block.getWorld(), block.getLocation().getX(), (double) 199, block.getLocation().getZ());

				if (beaconNorm.getBlock().getType().equals(Material.BEACON)) {
					block.setTypeIdAndData(95, beaconNorm.getBlock().getRelative(BlockFace.UP).getData(), false);
					return;
				} else if (beaconMont.getBlock().getType().equals(Material.BEACON)) {
					block.setTypeIdAndData(95, beaconMont.getBlock().getRelative(BlockFace.UP).getData(), false);
					return;
				} else if (beaconWat.getBlock().getType().equals(Material.BEACON)) {
					block.setTypeIdAndData(95, beaconWat.getBlock().getRelative(BlockFace.UP).getData(), false);
					return;
				}
			}


			if (block.getType().equals(Material.STAINED_CLAY)) {
				block.setTypeIdAndData(159, PlayersConfy.getColor(player), false);
			}
		}

		/*else if (block.getType().equals(Material.DIAMOND_ORE)) {
			Misc.find(block.getLocation(), 1, "1-1_1");
		}
		else if (block.getType().equals(Material.EMERALD_ORE)) {
			Misc.find(block.getLocation(), 2, "2-1_1");
		}*/
		/*else if (block.getType().equals(Material.MYCEL)) {
			PartyManagement.newParty( (byte) 1, (byte) 1, (byte) 1);
		}
		else if (block.getType().equals(Material.BRICK)) {
			PartyManagement.deleteParty(Settings.nextParty((byte)1,(byte) 1, (byte) 1, 1 ));
		}
		else if (block.getType().equals(Material.BOOKSHELF)) {
			Misc.trader(block.getLocation(), Traders.trader__1_main_name);
		}*/
		/*else if (block.getType().equals(Material.NETHERRACK)) {
			player.getInventory().addItem(Items.price_1);
			player.getInventory().addItem(Items.price_2);
			player.getInventory().addItem(Items.price_3);
			player.getInventory().addItem(Items.price_4);
		}
		else if (block.getType().equals(Material.BEACON)) {
			PartyManagement.startParty( (byte) 1, (byte) 1, (byte) 1, 1 );
		}*/
		/*else if (block.getType().equals(Material.COBBLESTONE)) {
			Misc.explodeFirework(block.getLocation(), 1);
		}*/
		else {

		}
	}
	
	public static void explode(Location location, String player) {

		//Bukkit.getLogger().info("an explosion is lunch by "+player);

		location.getBlock().setType(Material.AIR);

		Location loc = new Location(location.getWorld(), (double) 0, (double) 0, (double) 0);
		
		loc.setX(location.getBlockX()+0.5);
		loc.setY(location.getBlockY());
		loc.setZ(location.getBlockZ() + 0.5);
		
		TNTPrimed tnt = loc.getWorld().spawn(loc, TNTPrimed.class);

		tnt.setMetadata("player", new FixedMetadataValue(TimeManagement.plugin, player));

		tnt.setVelocity(new Vector(0, 0, 0));
		tnt.setFuseTicks(80);
		
	}
	
	public static void kill(Player player, EntityDamageEvent.DamageCause cause, Location dieddLocation, String killer) {
		player.spigot().respawn();
		player.teleport(dieddLocation);

		if (PlayersConfy.RushPlayers.containsKey(player)) {

			if (!player.getWorld().equals(Bukkit.getWorld("lobby"))) {

				player.getInventory().clear();
				player.getInventory().setChestplate(new ItemStack(Material.AIR));

				String party = PlayersConfy.getParty(player);
				Byte color = PlayersConfy.getColor(player);

				Location bed = new Location(PartyConfy.getBedPlayer(player).getWorld(),
						PartyConfy.getBedPlayer(player).getX() + 0.5,
						PartyConfy.getBedPlayer(player).getY() + 0,
						PartyConfy.getBedPlayer(player).getZ() + 0.5,
						PlayersConfy.RushPlayers.get(player).getRespawnYaw().getYaw(),
						0);

			/*for (ItemStack item : player.getInventory().getContents()) {
				if (item != null) {
					player.getLocation().getWorld().dropItemNaturally(dieddLocation, item.clone());
				}
			}*/


				String diedName = "";

				if (cause.equals(EntityDamageEvent.DamageCause.FALL)) {
					diedName = "Degats de chute";
				} else if (cause.equals(EntityDamageEvent.DamageCause.DROWNING)) {
					diedName = "Noyade";
				} else if (cause.equals(EntityDamageEvent.DamageCause.MAGIC)) {
					diedName = "Potions";
				} else if (cause.equals(EntityDamageEvent.DamageCause.POISON)) {
					diedName = "Potions";
				} else if (cause.equals(EntityDamageEvent.DamageCause.VOID)) {
					diedName = "Vide";
				} else if (cause.equals(EntityDamageEvent.DamageCause.BLOCK_EXPLOSION)) {
					diedName = "TNT";
				} else if (cause.equals(EntityDamageEvent.DamageCause.ENTITY_EXPLOSION)) {
					diedName = "TNT";
				} else if (cause.equals(EntityDamageEvent.DamageCause.SUFFOCATION)) {
					diedName = "Bordures";
				} else if (cause.equals(EntityDamageEvent.DamageCause.PROJECTILE)
						|| cause.equals(EntityDamageEvent.DamageCause.ENTITY_ATTACK)) {
				/*Player killer = player.getKiller();
				String name = killer.getName();*/

					if (PlayersConfy.RushPlayers.containsKey(Bukkit.getPlayer(killer))) {

						RushPlayer Rkiller = PlayersConfy.RushPlayers.get(Bukkit.getPlayer(killer));

						Rkiller.setKills(Rkiller.getKills() + 1);
					}

					String asassins = "" + Teams.colorToChatString(PlayersConfy.getColor(Bukkit.getPlayer(killer))) + "§o§l§n" + killer;
					diedName = "Tué par " + asassins + "";
				}


				//player.sendMessage(Messages.youDied);
				ChatManagement.sendMessage("@" + Messages.youDied + " §o§n" + diedName, player);

				RushPlayer Rplayer = PlayersConfy.RushPlayers.get(player);
				Rplayer.setDeaths(Rplayer.getDeaths() + 1);

				BukkitTask time = Bukkit.getServer().getScheduler().runTaskLater(TimeManagement.plugin, new Runnable() {
					@Override
					public void run() {

						if (!PartyConfy.getBedStatu(party, color)) {

							Misc.explodeFirework(player.getLocation(), 1, Teams.colorToColor(PlayersConfy.getColor(player)));

							player.teleport(bed);
							player.setFoodLevel(20);
							player.setHealth(20);

							for (PotionEffect effect : player.getActivePotionEffects()) {
								player.removePotionEffect(effect.getType());
							}

							player.getInventory().clear();
							player.getInventory().setChestplate(new ItemStack(Material.AIR));
							Misc.setArmor(player);

							PlayersConfy.setFreezeStatu(player, true);
							player.setGameMode(GameMode.SPECTATOR);

							BukkitTask time = Bukkit.getServer().getScheduler().runTaskLater(TimeManagement.plugin, new Runnable() {
								@Override
								public void run() {
									PlayersConfy.setFreezeStatu(player, false);
									Location beda = new Location(PartyConfy.getBedPlayer(player).getWorld(),
											PartyConfy.getBedPlayer(player).getX() + 0.5,
											PartyConfy.getBedPlayer(player).getY() + 0,
											PartyConfy.getBedPlayer(player).getZ() + 0.5,
											player.getLocation().getYaw(),
											player.getLocation().getPitch());
									player.teleport(beda);
									player.setFoodLevel(20);
									player.setHealth(20);
									player.setGameMode(GameMode.SURVIVAL);
									player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 100, 99, true, false));
								}
							}, 100);

						} else {


							for (PotionEffect effect : player.getActivePotionEffects()) {
								player.removePotionEffect(effect.getType());
							}

							player.getInventory().clear();
							player.getInventory().setHelmet(new ItemStack(Material.AIR));
							player.getInventory().setChestplate(new ItemStack(Material.AIR));
							player.getInventory().setLeggings(new ItemStack(Material.AIR));
							player.getInventory().setBoots(new ItemStack(Material.AIR));

							PlayersConfy.setDiedStatu(player, true);
							PartyConfy.removePlayer(player);
							PlayersConfy.chest.remove(player.getName());
							if (!player.getGameMode().equals(GameMode.SPECTATOR)) {

								Misc.explodeFirework(player.getLocation(), 2, Teams.colorToColor(PlayersConfy.getColor(player)));

								player.teleport(new Location(Bukkit.getWorld(party), 0.5, 201, 0.5));
								player.setGameMode(GameMode.SPECTATOR);
								ChatManagement.sendMessage("@" + Messages.diedWithoutBed, player);

								Byte winColor = PartyManagement.testForWin(player);
								if (winColor != (byte) -1) {
									PartyManagement.winParty(PlayersConfy.getParty(player), player, winColor);
								}
							} else {
								player.teleport(new Location(Bukkit.getWorld(party), 0.5, 201, 0.5));
							}

							ScoreboardManagement.refreshScoreboard(party);
						}
					}
				}, 1);


			/*player.setFoodLevel(20);
			player.setHealth(20);

			player.getInventory().clear();
			player.getInventory().setChestplate(new ItemStack(Material.AIR));*/


			} else {
				Location teleportLocation;

				if (PlayersConfy.getType(player) == (byte) 1) {
					teleportLocation = Settings.lobby1.clone();
				} else if (PlayersConfy.getType(player) == (byte) 2) {
					teleportLocation = Settings.lobby2.clone();
				} else if (PlayersConfy.getType(player) == (byte) 3) {
					teleportLocation = Settings.lobby3.clone();
				} else {
					teleportLocation = Bukkit.getWorld("world").getSpawnLocation();
				}

				teleportLocation.add(0, 2, 0);
				teleportLocation.setYaw(player.getLocation().getYaw());
				teleportLocation.setPitch(player.getLocation().getPitch());

				player.teleport(teleportLocation);
			}
		}
		
	}
	
	
	
	public static void find(Location location, byte type, byte tip,byte part, int nb) {
			
		Location loc = location;
		
		int nbz = 0;
		int nby = 0;
		int nbx = 0;
		
		if(type == 1) {
			loc.setX(loc.getX()+64);
			loc.setY(loc.getY()+4);
			loc.setZ(loc.getZ()+64);
			nbz = 128;
			nby = 7;
			nbx = 128;
		}
		else if (type == 2) {
			loc.setX(loc.getX()+109);
			loc.setY(loc.getY()+48);
			loc.setZ(loc.getZ()+109);
			nbz = 218;
			nby = 7;
			nbx = 218;
		}
		else if (type == 3) {
			loc.setX(loc.getX()+100);
			loc.setY(loc.getY()-4);
			loc.setZ(loc.getZ()+100);
			nbz = 200;
			nby = 9;
			nbx = 200;
		}
		
		Location cursor = loc;

		int x = 0;
		int y = 0;
		int z = 0;
		
		
		z = 0;
		while (z < nbz) {
			
			y = 0;
			while (y < nby) {
				
				x = 0;
				while (x < nbx) {
					
					PartyManagement.findr(cursor, type, tip, part, nb);
					
					cursor.setX(cursor.getX()-1);
					
					x++;
				}
				cursor.setX(cursor.getX()+nbx);
				
				PartyManagement.findr(cursor, type, tip, part, nb);
				
				cursor.setY(cursor.getY()-1);
				
				y++;
				
			}
			cursor.setY(cursor.getY()+nby);
			
			PartyManagement.findr(cursor, type, tip, part, nb);
			
			cursor.setZ(cursor.getZ()-1);
			
			z++;
			
		}
		cursor.setZ(cursor.getZ() + nbz);
		
		
	}
	

	
	public static void trader(Location location, String type){


		location.getBlock().setType(Material.AIR);
		
		location.setX(location.getBlockX()+0.5);
		location.setY(location.getBlockY());
		location.setZ(location.getBlockZ()+0.5);
		
		EnderCrystal trader = (EnderCrystal) location.getWorld().spawnEntity(location, EntityType.ENDER_CRYSTAL);

		trader.setShowingBottom(false);

		trader.setCustomName(type);
		trader.setCustomNameVisible(true);
		
	}

	public static void setCross(Banner banner) {

		byte color = 15;

		if (banner.getBaseColor().getData() == (byte)15) {
			color = (byte)0;
		}


		banner.addPattern(new Pattern(DyeColor.getByWoolData(color), PatternType.CREEPER));

		banner.update(true);


	}

	public static Block getBedHead(Block b) {
		/*byte data = b.getData();
		return data == 8 || data == 9 || data == 10 || data == 11 ? b : b.getWorld().getBlockAt(b.getX() + (data == 3 ? 1 : data == 1 ? -1 : 0), b.getY(), b.getZ() + (data == 0 ? 1 : data == 2 ? -1 : 0));*/
		/*byte data = b.getData();
		return b.getWorld().getBlockAt(b.getX() + (data == 3 || data == 9 ? 1 : data == 1 || data == 11 ? -1 : 0), b.getY(), b.getZ() + +(data == 0 || data == 10 ? 1 : data == 2 || data == 8 ? -1 : 0));*/

		Block head = b;

		if (b.getType().equals(Material.BED_BLOCK)) {
			Bed bed = (Bed) b.getState().getData();
			if (!bed.isHeadOfBed()) {
				head = b.getRelative(bed.getFacing());
			}
		}

		return head;
	}


	public static void setArmor(Player player) {
		Byte color = PlayersConfy.getColor(player);

		ItemStack helmet = Items.helmet.clone();
		LeatherArmorMeta helmetMeta = (LeatherArmorMeta) helmet.getItemMeta();
		helmetMeta.setColor(Teams.colorToColor(color));
		helmet.setItemMeta(helmetMeta);

		ItemStack legg = Items.legg.clone();
		LeatherArmorMeta leggMeta = (LeatherArmorMeta) legg.getItemMeta();
		leggMeta.setColor(Teams.colorToColor(color));
		legg.setItemMeta(leggMeta);

		ItemStack boot = Items.boot.clone();
		LeatherArmorMeta bootMeta = (LeatherArmorMeta) boot.getItemMeta();
		bootMeta.setColor(Teams.colorToColor(color));
		boot.setItemMeta(bootMeta);

		player.getInventory().setHelmet(helmet);
		player.getInventory().setLeggings(legg);
		player.getInventory().setBoots(boot);

	}


	public static void setAllPartyXp(String party, int nb) {
		ArrayList<Player> players = PartyConfy.getPlayers(party);
		int bucle = 0;
		while (bucle < players.size()) {
			players.get(bucle).setLevel(nb);
			bucle++;
		}
	}
	public static void setAllPartyXp(Player player, int nb) {
		player.setLevel(nb);
	}

	public static void sendSecondTitle(String party, String sc) {
		ArrayList<Player> players = PartyConfy.getPlayers(party);

		UtilTitle.sendTitle(new Title(EnumWrappers.TitleAction.TITLE, new ChatText("§2§l"+sc), 0, 20, 0), players);

		/*int bucle = 0;
		while (bucle < players.size()) {

			TitleAPI.sendTitle(players.get(bucle), 0, 20, 0, "§2§l"+sc,  "");
			bucle++;
		}*/
	}
	public static void sendSecondTitle(Player player, String sc) {
		UtilTitle.sendTitle(new Title(EnumWrappers.TitleAction.TITLE, new ChatText("§2§l"+sc), 0, 20, 0), player);
		//TitleAPI.sendTitle(player, 0, 20, 0, "§2§l"+sc,  "");
	}

	public static void explodeFirework(Location location, int occase, Color color) {

		boolean trail = false;
		boolean flicker = true;
		FireworkEffect.Type type = FireworkEffect.Type.BALL;

		//normal died
		if (occase == 1) {
			type = FireworkEffect.Type.BALL;
		}
		//eliminated died
		else if (occase == 2) {
			type = FireworkEffect.Type.BALL_LARGE;
		}
		//bed explode
		else if (occase == 3) {
			type = FireworkEffect.Type.CREEPER;
		}


		Location explosing = location.clone();
		explosing.add(0, 1, 0);

		Firework firework = explosing.getWorld().spawn(explosing, Firework.class);

		FireworkEffect effect = FireworkEffect.builder().trail(trail).flicker(flicker).withColor(color).with(type).build();

		FireworkMeta fireworkMeta = firework.getFireworkMeta();

		fireworkMeta.clearEffects();
		fireworkMeta.addEffect(effect);
		Field field;

		try {
			field = fireworkMeta.getClass().getDeclaredField("power");
			field.setAccessible(true);
			field.set(fireworkMeta, -1);
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

		firework.setFireworkMeta(fireworkMeta);

	}

	public static ArrayList<String> StringToArray(String string) {
		/*ArrayList<String> list = new ArrayList<String>();

		int bucle1 = 0;
		int bucle2 = 0;
		int count = 0;
		int bcl = 0;
		boolean okay1 = false;
		boolean finish = false;
		int calculsavant = 0;

		bcl = 0;
		while (!finish) {

			//Bukkit.getLogger().info("buclefinish, bcl : "+bcl);

			if (string.length() > bcl) {

				//Bukkit.getLogger().info("firstCond : "+string.charAt(bcl));
				//Bukkit.getLogger().info("secondCon : "+Integer.toString(count).charAt(0));

				if (string.charAt(bcl) == Integer.toString(count).charAt(0)
						&& string.charAt((bcl + 1)) == ':'
						&& string.charAt((bcl + 2)) == '(') {

					bucle1 = bcl + 3;

					okay1 = false;
					while (!okay1) {

						//Bukkit.getLogger().info("bucle1 while : "+bucle1);

						if (string.charAt(bucle1) == ')') {
							String fin = "";

							calculsavant = 0;
							calculsavant = (bucle1 - ((bcl + 3)));

							bucle1 = bucle1 - calculsavant;

							bucle2 = 0;

							while (bucle2 < calculsavant) {

								//Bukkit.getLogger().info("resultOfCalculSavant : "+calculsavant);

								//Bukkit.getLogger().info("CharAt : "+(bucle1+bucle2));
								fin = "" + fin + "" + string.charAt(bucle1 + bucle2);

								bucle2++;
							}
							//Bukkit.getLogger().info("bucle2-1 : "+(bucle2));

							list.add(fin);
							//Bukkit.getLogger().info("La final : "+ fin);
							okay1 = true;
						}

						bucle1++;
					}
					//Bukkit.getLogger().info("bucle1-1 : "+(bucle1-1));

				}
			}
			else {
				finish = true;
			}
			bcl = bcl+4+(bucle2);
			count++;
		}

		return list;*/
		return UtilString.CryptedStringToArrayList(string);
	}

	public static void actionBarSecond(String party, int sc) {
		String action[] = {"§4▇","§4▇","§4▇","§4▇","§4▇","§4▇","§4▇","§4▇","§4▇","§4▇"};

		switch (sc) {
			case 10 :
				action[0] = "§2▇";
				break;

			case 9 :
				action[0] = "§2▇";
				action[1] = "§2▇";
				break;

			case 8 :
				action[0] = "§2▇";
				action[1] = "§2▇";
				action[2] = "§2▇";
				break;

			case 7 :
				action[0] = "§2▇";
				action[1] = "§2▇";
				action[2] = "§2▇";
				action[3] = "§2▇";
				break;

			case 6 :
				action[0] = "§2▇";
				action[1] = "§2▇";
				action[2] = "§2▇";
				action[3] = "§2▇";
				action[4] = "§2▇";
				break;

			case 5 :
				action[0] = "§2▇";
				action[1] = "§2▇";
				action[2] = "§2▇";
				action[3] = "§2▇";
				action[4] = "§2▇";
				action[5] = "§2▇";
				break;

			case 4 :
				action[0] = "§2▇";
				action[1] = "§2▇";
				action[2] = "§2▇";
				action[3] = "§2▇";
				action[4] = "§2▇";
				action[5] = "§2▇";
				action[6] = "§2▇";
				break;

			case 3 :
				action[0] = "§2▇";
				action[1] = "§2▇";
				action[2] = "§2▇";
				action[3] = "§2▇";
				action[4] = "§2▇";
				action[5] = "§2▇";
				action[6] = "§2▇";
				action[7] = "§2▇";
				break;

			case 2 :
				action[0] = "§2▇";
				action[1] = "§2▇";
				action[2] = "§2▇";
				action[3] = "§2▇";
				action[4] = "§2▇";
				action[5] = "§2▇";
				action[6] = "§2▇";
				action[7] = "§2▇";
				action[8] = "§2▇";
				break;

			case 1 :
				action[0] = "§2▇";
				action[1] = "§2▇";
				action[2] = "§2▇";
				action[3] = "§2▇";
				action[4] = "§2▇";
				action[5] = "§2▇";
				action[6] = "§2▇";
				action[7] = "§2▇";
				action[8] = "§2▇";
				action[9] = "§2▇";
				break;
			case -1 :
				action[0] = "";
				action[1] = "";
				action[2] = "";
				action[3] = "";
				action[4] = "";
				action[5] = "";
				action[6] = "";
				action[7] = "";
				action[8] = "";
				action[9] = "";
				break;
			default:
				action = action;
		}

		String msg = "";

		int bucle1 = 0;
		while (bucle1 < action.length) {
			msg = msg+action[bucle1];
			bucle1++;
		}

		ArrayList<Player> players = PartyConfy.getPlayers(party);

		UtilChat.sendActionBar(new ChatText(msg), players);

		/*int bucle = 0;
		while (bucle < players.size()) {
			//Action.playAction(players.get(bucle), msg);
			//ActionBarAPI.sendActionBar(players.get(bucle), action.toString());
			bucle++;
		}*/
	}
	public static void actionBarSecond(Player player, int sc) {
		String action[] = {"§4▇","§4▇","§4▇","§4▇","§4▇","§4▇","§4▇","§4▇","§4▇","§4▇"};

		switch (sc) {
			case 10 :
				action[0] = "§2▇";
				break;

			case 9 :
				action[0] = "§2▇";
				action[1] = "§2▇";
				break;

			case 8 :
				action[0] = "§2▇";
				action[1] = "§2▇";
				action[2] = "§2▇";
				break;

			case 7 :
				action[0] = "§2▇";
				action[1] = "§2▇";
				action[2] = "§2▇";
				action[3] = "§2▇";
				break;

			case 6 :
				action[0] = "§2▇";
				action[1] = "§2▇";
				action[2] = "§2▇";
				action[3] = "§2▇";
				action[4] = "§2▇";
				break;

			case 5 :
				action[0] = "§2▇";
				action[1] = "§2▇";
				action[2] = "§2▇";
				action[3] = "§2▇";
				action[4] = "§2▇";
				action[5] = "§2▇";
				break;

			case 4 :
				action[0] = "§2▇";
				action[1] = "§2▇";
				action[2] = "§2▇";
				action[3] = "§2▇";
				action[4] = "§2▇";
				action[5] = "§2▇";
				action[6] = "§2▇";
				break;

			case 3 :
				action[0] = "§2▇";
				action[1] = "§2▇";
				action[2] = "§2▇";
				action[3] = "§2▇";
				action[4] = "§2▇";
				action[5] = "§2▇";
				action[6] = "§2▇";
				action[7] = "§2▇";
				break;

			case 2 :
				action[0] = "§2▇";
				action[1] = "§2▇";
				action[2] = "§2▇";
				action[3] = "§2▇";
				action[4] = "§2▇";
				action[5] = "§2▇";
				action[6] = "§2▇";
				action[7] = "§2▇";
				action[8] = "§2▇";
				break;

			case 1 :
				action[0] = "§2▇";
				action[1] = "§2▇";
				action[2] = "§2▇";
				action[3] = "§2▇";
				action[4] = "§2▇";
				action[5] = "§2▇";
				action[6] = "§2▇";
				action[7] = "§2▇";
				action[8] = "§2▇";
				action[9] = "§2▇";
				break;
			case -1 :
				action[0] = "";
				action[1] = "";
				action[2] = "";
				action[3] = "";
				action[4] = "";
				action[5] = "";
				action[6] = "";
				action[7] = "";
				action[8] = "";
				action[9] = "";
				break;
			default:
				action = action;
		}

		String msg = "";

		int bucle1 = 0;
		while (bucle1 < action.length) {
			msg = msg+action[bucle1];
			bucle1++;
		}

		UtilChat.sendActionBar(new ChatText(msg), player);

		//Action.playAction(player, msg);
		//ActionBarAPI.sendActionBar(player, action.toString());
	}

	public static ItemStack lobbyItemHub;
	public static ItemStack teamitem;
	public static HashMap<Byte, Inventory> lobbyInventoryMapTeams;
	public static HashMap<Byte, ItemStack> lobbyItemMapTeams;

	public static void initLobbyItem() {
		lobbyItemHub = new ItemStack(Material.IRON_DOOR, 1);
		ItemMeta lobbyItemHubMeta = lobbyItemHub.getItemMeta();
		lobbyItemHubMeta.setDisplayName("§6§l►§r§6§lHUB§r§6§l◄");
		lobbyItemHub.setItemMeta(lobbyItemHubMeta);

		teamitem = new ItemStack(Material.NETHER_STAR);
		ItemMeta teamitemmeta = teamitem.getItemMeta();
		teamitemmeta.setDisplayName(Messages.chooseInventoryTitle);
		teamitem.setItemMeta(teamitemmeta);

		lobbyItemMapTeams = new HashMap<Byte, ItemStack>();
		lobbyInventoryMapTeams = new HashMap<>();

		int bucle1 = 0;
		ArrayList<Byte> teamsList = Teams.tipToColor((byte)3);
		teamsList.add((byte)0);

		while (bucle1 < teamsList.size()) {

			lobbyItemMapTeams.put(teamsList.get(bucle1), new ItemStack(Material.BANNER, 1, (short) 0, Teams.byteToBannerByte(teamsList.get(bucle1))));
			ItemMeta meta = lobbyItemMapTeams.get(teamsList.get(bucle1)).getItemMeta();
			meta.setDisplayName(Teams.colorToChatString(teamsList.get(bucle1)) + "§l" + (Teams.colorToString(teamsList.get(bucle1)).toUpperCase()));
			lobbyItemMapTeams.get(teamsList.get(bucle1)).setItemMeta(meta.clone());
			/*BannerMeta banner = (BannerMeta) lobbyItemMapTeams.get(teamsList.get(bucle1)).getItemMeta();
			banner.setBaseColor(DyeColor.getByColor(Teams.colorToColor(teamsList.get(bucle1))));
			lobbyItemMapTeams.get(teamsList.get(bucle1)).setItemMeta(banner.clone());*/


			bucle1++;
		}

		int bucle2 = 1;
		while (bucle2 <= 3) {

			lobbyInventoryMapTeams.put((byte)bucle2, Bukkit.createInventory(null, 9, Messages.chooseInventoryTitle));
			Misc.giveLobbyItems((byte) bucle2, lobbyInventoryMapTeams.get((byte)bucle2));

			bucle2++;
		}



	}

	public static void giveLobbyItems(Byte tip, Inventory inventory) {

		int bucle = 0;
		ArrayList<Byte> teamsList = Teams.tipToColor(tip);

		while (bucle < teamsList.size()) {

			inventory.setItem(bucle, lobbyItemMapTeams.get(teamsList.get(bucle)));

			bucle++;
		}

	}

	public static void giveLobbyItems(Player player) {
		player.getInventory().setItem(8, lobbyItemHub);

		player.getInventory().setItem(0, teamitem);

		/*int bucle = 0;
		ArrayList<Byte> teamsList = Teams.tipToColor((byte) PlayersConfy.getTip(player));

		while (bucle < teamsList.size()) {

			player.getInventory().setItem(bucle, lobbyItemMapTeams.get(teamsList.get(bucle)));

			bucle++;
		}*/

	}

}




