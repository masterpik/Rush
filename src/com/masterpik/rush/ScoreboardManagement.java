package com.masterpik.rush;

import com.masterpik.rush.confy.PartyConfy;
import com.masterpik.rush.confy.PlayersConfy;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.*;

import java.util.ArrayList;

public class ScoreboardManagement {

    public static int howMainScore = 20;

    public static Scoreboard getMain() {

        Scoreboard scoreboard = Main.scoreboardManager.getNewScoreboard();

        Objective sidebar = scoreboard.registerNewObjective("sidebar", "dummy");

        sidebar.setDisplayName(Messages.masterpik);
        sidebar.setDisplaySlot(DisplaySlot.SIDEBAR);

        Score sidebarScore1 = sidebar.getScore(" ");
        sidebarScore1.setScore(howMainScore);

        Score sidebarScore2 = sidebar.getScore(Messages.main);
        sidebarScore2.setScore(howMainScore - 1);

        Score sidebarScore3 = sidebar.getScore("  ");
        sidebarScore3.setScore(howMainScore - 2);


        return scoreboard;
    }


    public static Scoreboard addTeams(Player player) {
        Scoreboard scoreboard = ScoreboardManagement.getMain();

        Objective sidebar = scoreboard.getObjective("sidebar");

        ArrayList<Byte> teams = Teams.tipToColor((byte) PlayersConfy.getTip(player));

        ArrayList<Player> playersInTeam = new ArrayList<Player>();

        int bucle = 0;
        int count = 3;
        int bucle2 = 0;



        Score teamsScore = sidebar.getScore(Messages.main);

        while (bucle < teams.size()) {

            bucle2 = 0;

            Team tim = scoreboard.registerNewTeam(Teams.colorToString(teams.get(bucle)));
            playersInTeam = PartyConfy.getPlayersTeam(PlayersConfy.getParty(player), teams.get(bucle));
            tim.setAllowFriendlyFire(false);
            tim.setCanSeeFriendlyInvisibles(false);
            tim.setPrefix(""+Teams.colorToChatString(teams.get(bucle))+"§l");
            tim.setOption(Team.Option.COLLISION_RULE, Team.OptionStatus.NEVER);

            while (bucle2 < playersInTeam.size()) {

                tim.addEntry(playersInTeam.get(bucle2).getName());
                //playersInTeam.get(bucl2).sendMessage();

                bucle2 ++;
            }



            teamsScore = sidebar.getScore(ScoreboardManagement.getNameScore(teams.get(bucle), player));
            teamsScore.setScore(howMainScore - count);
            count++;

            bucle++;
        }


        return scoreboard;
    }

    public static String getNameScore(Byte colore,Player player) {
        String score = "";

        Scoreboard scoreboard = player.getScoreboard();

        Objective sidebar = scoreboard.getObjective("sidebar");
        String color = "§r";
        String bed = "§2§l✔";


        int players = 0;
        int plMax = (Settings.getNbPlayer((byte)PlayersConfy.getTip(player), (byte) PlayersConfy.getPart(player)))/(Teams.tipToColor((byte)PlayersConfy.getTip(player)).size());

        color = ""+Teams.colorToChatString(colore)+"§l";

        players = PartyConfy.getPlayersTeam(PlayersConfy.getParty(player), colore).size();

        bed = PartyConfy.bedStatuToString(PartyConfy.getBedStatu(PlayersConfy.getParty(player), colore));

        String teamstr =""+color+Messages.bloc1+" "+Teams.colorToString(colore);

        String prefix = players+"/"+plMax+" "+bed;

        String space = "";

        int bucle1 = 0;
        int maxscor = 0;

        if (PlayersConfy.getTip(player) == 1) {
            maxscor = 21;
        } else if (PlayersConfy.getTip(player) == 2) {
            maxscor = 21;
        } else if (PlayersConfy.getTip(player) == 3) {
            maxscor = 23;
        }

        /*Bukkit.getLogger().info("teamstr = "+Integer.toString(teamstr.length()));
        Bukkit.getLogger().info("prefix = "+Integer.toString(prefix.length()));
        Bukkit.getLogger().info("result = "+Integer.toString((23-(prefix.length())-(teamstr.length()))));*/
        while (bucle1 < (maxscor-(prefix.length())-(teamstr.length())) ) {

            space = space+" ";

            bucle1++;
        }

        String fix = Teams.colorToFix(colore);

        score = teamstr+fix+space+prefix;

        return score;
    }


    public static void refreshScoreboard(String party) {
        ArrayList<Player> playersParty = PartyConfy.getPlayers(party);

        int bucle1 = 0;

        while (bucle1 < playersParty.size()) {

            playersParty.get(bucle1).setScoreboard(ScoreboardManagement.addTeams(playersParty.get(bucle1)));

            bucle1 ++;
        }
    }
}
