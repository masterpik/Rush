package com.masterpik.rush;

import com.masterpik.api.util.UtilShield;
import com.masterpik.rush.confy.PartyConfy;
import com.masterpik.rush.confy.PlayersConfy;
import com.masterpik.rush.confy.RushPlayer;
import org.bukkit.*;
import org.bukkit.block.Banner;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.*;
import org.bukkit.event.inventory.*;
import org.bukkit.event.player.*;
import org.bukkit.event.vehicle.VehicleDestroyEvent;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Bed;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitTask;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class Events implements Listener{
	
	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void onMove(PlayerMoveEvent event) {
		Player player = event.getPlayer();
		Location location = player.getLocation();
		
		
		Location locationDown = location;
		locationDown.setY(locationDown.getY()-1);
		Block walkedBlock = locationDown.getBlock();
		
		if (!player.getGameMode().equals(GameMode.SPECTATOR)) {
			//Misc.placeBlock(walkedBlock, player);

			if (walkedBlock.getType().equals(Material.STAINED_CLAY)) {
				walkedBlock.setTypeIdAndData(159, PlayersConfy.getColor(player), false);
			}

		}
		
		Location locationFall = location;
		locationFall.setY(Settings.diedAt);
		Block blockFall = locationFall.getBlock();
		
		if (player.getLocation().getBlockY() <= blockFall.getY() 
				&& !player.isOnGround() && !player.isOp()
				&& !player.getGameMode().equals(GameMode.SPECTATOR)) {
			Misc.kill(player, EntityDamageEvent.DamageCause.VOID, player.getLocation(), "");
		}

		if (player.getLocation().getBlockY() >= 256
				&& !player.isOp() && player.isOnGround()) {
			if (player.getHealth() == 1
					|| player.getHealth() == 2) {
				Misc.kill(player, EntityDamageEvent.DamageCause.SUFFOCATION, player.getLocation(), "");
			}
			else {
				player.setHealth(player.getHealth() - 2);
			}
		}

		if (PlayersConfy.isRegister(player) &&
				PlayersConfy.getFreezeStatu(player)
				&& !walkedBlock.getRelative(BlockFace.UP).getType().equals(Material.BED_BLOCK)) {
			Location bed = new Location(PartyConfy.getBedPlayer(player).getWorld(),
					PartyConfy.getBedPlayer(player).getX() + 0.5,
					PartyConfy.getBedPlayer(player).getY() + 0,
					PartyConfy.getBedPlayer(player).getZ() + 0.5,
					player.getLocation().getYaw(),
					player.getLocation().getPitch());
			player.teleport(bed);
			player.setFoodLevel(20);
			player.setHealth(20);
		}
		
		
		/*Block seeBlock = player.getTargetBlock((HashSet<Byte>) null, 10);
		player.sendMessage("tu regarde un bloc de : " + seeBlock.getType());*/
		
		/*Block seeBlock = player.getTargetBlock((Set<Material>) null, 100);
		
		int x = seeBlock.getLocation().getBlockX() - Settings.statLocation.getBlockX();
		int y = seeBlock.getLocation().getBlockY() - Settings.statLocation.getBlockY();
		int z = seeBlock.getLocation().getBlockZ() - Settings.statLocation.getBlockZ();
				
		
		player.sendMessage("+ x:"+x+" y:"+y+" z:"+z); */
		
		
	}
	
	
	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void onPlaceBlock(BlockPlaceEvent event) {
		Player player = event.getPlayer();
		Block placedBlock = event.getBlock();

		ArrayList<Block> blockArround = new ArrayList<Block>();

		blockArround.add(placedBlock.getRelative(BlockFace.UP));
		blockArround.add(placedBlock.getRelative(BlockFace.DOWN));
		blockArround.add(placedBlock.getRelative(BlockFace.EAST));
		blockArround.add(placedBlock.getRelative(BlockFace.NORTH));
		blockArround.add(placedBlock.getRelative(BlockFace.WEST));
		blockArround.add(placedBlock.getRelative(BlockFace.SOUTH));

		int bucle = 0;

		while (bucle < blockArround.size()){

			Block block = blockArround.get(bucle);

			if (block.getType().equals(Material.BARRIER) && !placedBlock.getType().equals(Material.BARRIER)) {
				event.setCancelled(true);
				bucle = blockArround.size()+1;
			}

			bucle ++;
		}

		if (placedBlock.getType().equals(Material.REDSTONE_WIRE)) {
			event.setCancelled(true);
		}
		/*if (placedBlock.getType().equals(Material.ENDER_CHEST)) {
			event.setCancelled(true);
			player.openInventory(PlayersConfy.chest.get(player.getName()));
		}*/
		/*else if (placedBlock.getType().equals(Material.IRON_DOOR)
				|| placedBlock.getType().equals(Material.IRON_DOOR_BLOCK)) {
			event.setCancelled(true);
			BungeeWork.sendPlayer(player, "hub");
		}*/

		Misc.placeBlock(placedBlock, player);

		
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onBlockChange(EntityChangeBlockEvent event) {

		//Bukkit.getLogger().info("entity change block event "+event.getEntity().getType());

		if(!(event.getEntity() instanceof FallingBlock))
            return;
        
        FallingBlock fb = (FallingBlock) event.getEntity();
        
        if (/*fb.isOnGround() 
        		&& */fb.getBlockId() == 46
				&& fb.hasMetadata("player")) {

			//Bukkit.getLogger().info("a falling tnt touch solid block, she will explode");

			event.getEntity().remove();
			fb.remove();

        	Misc.explode(fb.getLocation(), fb.getMetadata("player").get(0).asString());
        	event.setCancelled(true);
        }
	}
	
	
	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void onBreak(BlockBreakEvent event) {
		
		Block block = event.getBlock();
		Player player = event.getPlayer();
		
		
		Block blockDown = block.getRelative(BlockFace.DOWN);
		Block blockUp = block.getRelative(BlockFace.UP);
		Block blockDown2 = blockDown.getRelative(BlockFace.DOWN);
		
		if(!player.isOp()) {

			if(!Settings.Break.contains(block.getType())
					|| (blockDown.getType().equals(Material.PUMPKIN) && block.getType().equals(Material.LOG))
					|| (blockUp.getType().equals(Material.PUMPKIN) && block.getType().equals(Material.LOG))
					|| (blockDown2.getType().equals(Material.PUMPKIN) && block.getType().equals(Material.LOG))
					|| (blockUp.getType().equals(Material.COBBLE_WALL) && block.getType().equals(Material.LOG))
					|| (blockDown2.getType().equals(Material.COBBLE_WALL) && block.getType().equals(Material.LOG))
					|| (block.getType().equals(Material.DIRT) && blockUp.getType().equals(Material.BARRIER))
					|| (block.getType().equals(Material.GRASS) && blockUp.getType().equals(Material.BARRIER))) {
				event.setCancelled(true);
				//player.sendMessage("tu ne pe pas casser ça !");
			}
		
		}
		
		
	}

	/*@EventHandler(priority = EventPriority.HIGHEST)
	public void onDig(BlockDamageEvent event) {
		Block block = event.getBlock();
		Player player = event.getPlayer();

		Block blockDown = block.getRelative(BlockFace.DOWN);
		Block blockUp = block.getRelative(BlockFace.UP);
		Block blockDown2 = blockDown.getRelative(BlockFace.DOWN);

		if(!player.isOp()) {

			if(!Settings.Break.contains(block.getType())
					|| blockDown.getType().equals(Material.PUMPKIN) && block.getType().equals(Material.LOG)
					|| blockUp.getType().equals(Material.PUMPKIN) && block.getType().equals(Material.LOG)
					|| blockDown2.getType().equals(Material.PUMPKIN) && block.getType().equals(Material.LOG)
					|| blockUp.getType().equals(Material.COBBLE_WALL) && block.getType().equals(Material.LOG)
					|| blockDown2.getType().equals(Material.COBBLE_WALL) && block.getType().equals(Material.LOG)
					|| block.getType().equals(Material.DIRT) && blockUp.getType().equals(Material.BARRIER)
					|| block.getType().equals(Material.GRASS) && blockUp.getType().equals(Material.BARRIER)) {
				event.setCancelled(true);
			}
			else {


				/*ItemStack item = player.getItemInHand();
				if(!item.getType().equals(Material.IRON_SWORD)) {

					if (!item.containsEnchantment(Enchantment.DIG_SPEED)) {
						player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 20, 4, true, false));
					} else if (item.containsEnchantment(Enchantment.DIG_SPEED) && item.getEnchantmentLevel(Enchantment.DIG_SPEED) == 1) {
						player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 20, 3, true, false));
					} else if (item.containsEnchantment(Enchantment.DIG_SPEED) && item.getEnchantmentLevel(Enchantment.DIG_SPEED) == 2) {
						player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 20, 2, true, false));
					} else if (item.containsEnchantment(Enchantment.DIG_SPEED) && item.getEnchantmentLevel(Enchantment.DIG_SPEED) == 3) {
						player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 20, 1, true, false));
					} else if (item.containsEnchantment(Enchantment.DIG_SPEED) && item.getEnchantmentLevel(Enchantment.DIG_SPEED) == 4) {
						player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 20, 0, true, false));
					}
				}*/
			/*}
		}
	}*/

	@EventHandler(priority = EventPriority.HIGHEST)
	public void PlayerDropItemEvent(PlayerDropItemEvent event) {
		Player player = event.getPlayer();

		if (player.getWorld().equals(Bukkit.getWorld("lobby"))) {
			event.setCancelled(true);
		}

	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onClick(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		Action action = event.getAction();
		
		if (action == Action.RIGHT_CLICK_BLOCK) {
			Block block = event.getClickedBlock();
			
			if(block.getType().equals(Material.BED_BLOCK)) {
				event.setCancelled(true);
				player.sendMessage(Messages.cannotSleep);
			}
			else if(block.getType().equals(Material.FURNACE) || block.getType().equals(Material.WORKBENCH)) {
				event.setCancelled(true);
			}
			
		}
		/*else if (action.equals(Action.LEFT_CLICK_BLOCK)) {
			Block block = player.getTargetBlock((Set<Material>) null, 10);

			if(!player.isOp()) {

				Block blockDown = block.getRelative(BlockFace.DOWN);
				Block blockUp = block.getRelative(BlockFace.UP);
				Block blockDown2 = blockDown.getRelative(BlockFace.DOWN);

				if(!Settings.Break.contains(block.getType())
						|| blockDown.getType().equals(Material.PUMPKIN) && block.getType().equals(Material.LOG)
						|| blockUp.getType().equals(Material.PUMPKIN) && block.getType().equals(Material.LOG)
						|| blockDown2.getType().equals(Material.PUMPKIN) && block.getType().equals(Material.LOG)
						|| blockUp.getType().equals(Material.COBBLE_WALL) && block.getType().equals(Material.LOG)
						|| blockDown2.getType().equals(Material.COBBLE_WALL) && block.getType().equals(Material.LOG)
						|| block.getType().equals(Material.DIRT) && blockUp.getType().equals(Material.BARRIER)
						|| block.getType().equals(Material.GRASS) && blockUp.getType().equals(Material.BARRIER)) {
					event.setCancelled(true);
				}
				else {
					/*ItemStack item = player.getItemInHand();
					if(!item.getType().equals(Material.IRON_SWORD)) {
						if (!item.containsEnchantment(Enchantment.DIG_SPEED)) {
							player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 20, 4, true, false));
						} else if (item.containsEnchantment(Enchantment.DIG_SPEED) && item.getEnchantmentLevel(Enchantment.DIG_SPEED) == 1) {
							player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 20, 3, true, false));
						} else if (item.containsEnchantment(Enchantment.DIG_SPEED) && item.getEnchantmentLevel(Enchantment.DIG_SPEED) == 2) {
							player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 20, 2, true, false));
						} else if (item.containsEnchantment(Enchantment.DIG_SPEED) && item.getEnchantmentLevel(Enchantment.DIG_SPEED) == 3) {
							player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 20, 1, true, false));
						} else if (item.containsEnchantment(Enchantment.DIG_SPEED) && item.getEnchantmentLevel(Enchantment.DIG_SPEED) == 4) {
							player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 20, 0, true, false));
						}
					}*/
				/*}
			}
		}*/

		if (event.getItem() != null) {

			if (event.getItem().isSimilar(Items.chest)) {
				event.setCancelled(true);
				player.openInventory(PlayersConfy.chest.get(player.getName()));
			} else if (event.getItem().isSimilar(Misc.lobbyItemHub)) {
				event.setCancelled(true);
				BungeeWork.sendPlayer(player, "hub");
			} else if (event.getItem().getType().equals(Material.BANNER)) {
				event.setCancelled(true);
				PartyManagement.changeTeamPlayer(player, event.getItem());
			} else if (event.getItem().isSimilar(Misc.teamitem)) {
				event.setCancelled(true);
				player.openInventory(Misc.lobbyInventoryMapTeams.get((byte) PlayersConfy.getTip(player)));
			} else if (event.getItem().isSimilar(Items.frostWalker)) {
				event.setCancelled(true);
				player.sendMessage(Messages.frostWalking);

				//player.getInventory().remove(event.getItem());

				//player.getInventory().getievent.getItem()
				//event.getItem().setType(Material.AIR);
				//player.getInventory().clear(event.getPlayer().);

				//player.sendMessage(event.getHand().name());

				if (event.getHand().equals(EquipmentSlot.HAND)) {
					player.getInventory().setItemInMainHand(new ItemStack(Material.AIR));
				} else if (event.getHand().equals(EquipmentSlot.OFF_HAND)) {
					player.getInventory().setItemInOffHand(new ItemStack(Material.AIR));
				}

				player.getInventory().getBoots().addEnchantment(Enchantment.FROST_WALKER, 1);

				BukkitTask time = Bukkit.getScheduler().runTaskLater(TimeManagement.plugin, new Runnable() {
					@Override
					public void run() {
						player.getInventory().getBoots().removeEnchantment(Enchantment.FROST_WALKER);
					}
				}, 10 * 20);

			}

		}
		if (event.getClickedBlock() != null
				&& event.getClickedBlock().getType().equals(Material.BEACON)) {
			event.setCancelled(true);
		}
	
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onBlockExplode(EntityExplodeEvent event){
		
		if((event.getEntity() instanceof EnderCrystal)) {
			event.setCancelled(true);
			Misc.trader(event.getEntity().getLocation(), event.getEntity().getCustomName());
		}
		else {
			boolean destroyBed = false;
			if (event.getEntity().hasMetadata("player")) {
				Player player = Bukkit.getPlayer(event.getEntity().getMetadata("player").get(0).asString());
				List<Block> destroyed = event.blockList();
				Iterator<Block> it = destroyed.iterator();
				while (it.hasNext()) {
					Block block = it.next();

					Block blockDown = block.getRelative(BlockFace.DOWN);
					Block blockUp = block.getRelative(BlockFace.UP);
					Block blockDown2 = blockDown.getRelative(BlockFace.DOWN);

					if (block.getType().equals(Material.BED_BLOCK)
							&& block.getRelative(BlockFace.DOWN).getType().equals(Material.STAINED_GLASS)) {
						if (PlayersConfy.getColor(player)
								== block.getRelative(BlockFace.DOWN).getData()) {
							player.sendMessage(Messages.cannotAntiTeam);
							event.setCancelled(true);
						} else {
							PartyManagement.removeBed(block.getWorld().getName(), block.getLocation(), player);
							destroyBed = true;
						}
					} else if (block.getType().equals(Material.BED_BLOCK)
							&& !block.getRelative(BlockFace.DOWN).getType().equals(Material.STAINED_GLASS)
							&& !destroyBed
							&& !((Bed) block.getState().getData()).isHeadOfBed()) {

						Block headBed = Misc.getBedHead(block);

						if (PlayersConfy.getColor(player) == headBed.getRelative(BlockFace.DOWN).getData()) {
							player.sendMessage(Messages.cannotAntiTeam);
							event.setCancelled(true);
						} else {
							PartyManagement.removeBed(block.getWorld().getName(), headBed.getLocation(), player);
						}
					}

					if (!Settings.BreakTNT.contains(block.getType())
							|| blockDown.getType().equals(Material.PUMPKIN) && block.getType().equals(Material.LOG)
							|| blockUp.getType().equals(Material.PUMPKIN) && block.getType().equals(Material.LOG)
							|| blockDown2.getType().equals(Material.PUMPKIN) && block.getType().equals(Material.LOG)
							|| blockUp.getType().equals(Material.COBBLE_WALL) && block.getType().equals(Material.LOG)
							|| blockDown2.getType().equals(Material.COBBLE_WALL) && block.getType().equals(Material.LOG)) {

						it.remove();
					}

				}
			}
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onVehicleDestroy(VehicleDestroyEvent event) {
		if (event.getVehicle().getType().equals(EntityType.BOAT)
				/*&& !(event.getAttacker() instanceof Player)*/) {
			//event.setCancelled(true);
			event.getVehicle().getLocation().getWorld().dropItem(event.getVehicle().getLocation(), Items.boat);
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onItemSpawn(ItemSpawnEvent event) {
		Material dropped = event.getEntity().getItemStack().getType();
		
		if(dropped.equals(Material.BED)
				|| dropped.equals(Material.LEATHER_HELMET)
				|| dropped.equals(Material.LEATHER_LEGGINGS)
				|| dropped.equals(Material.LEATHER_BOOTS)) {
			event.setCancelled(true);
		}
		else if (dropped.equals(Material.STAINED_CLAY)) {
			ItemStack item = Items.clay.clone();
			item.setAmount(event.getEntity().getItemStack().getAmount());
			event.getEntity().setItemStack(item);
		}
		else if (dropped.equals(Material.TNT)) {
			ItemStack item = Items.tnt.clone();
			item.setAmount(event.getEntity().getItemStack().getAmount());
			event.getEntity().setItemStack(item);
		}
		/*else if (dropped.equals(Material.STICK)) {
			ItemStack item = Items.boat.clone();
			item.setAmount(1);
			event.getEntity().setItemStack(item);
		}*/ else if (dropped.equals(Material.COBBLESTONE)) {
			event.getEntity().setItemStack(new ItemStack(Material.STONE, 1));
		} else if (dropped.equals(Material.SHIELD)) {
			ItemStack shield = Items.shield.clone();
			event.getEntity().setItemStack(shield);
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void PlayerPickupItemEvent(PlayerPickupItemEvent event) {
		Material dropped = event.getItem().getItemStack().getType();

		if (dropped.equals(Material.SHIELD)) {
			if (PlayersConfy.RushPlayers.containsKey(event.getPlayer())) {
				RushPlayer pl = PlayersConfy.RushPlayers.get(event.getPlayer());
				Banner banner = ((Banner) (PartyConfy.getBannerLocation(pl.getParty(), pl.getColor(), 0).getBlock().getState()));
				event.getItem().getItemStack().setItemMeta(UtilShield.patternOnShield(Items.shield.clone(), banner.getBaseColor(), null).getItemMeta());
			}
		}
	}
	
	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void onCraft(CraftItemEvent event) {
		//Player player = (Player) event.getWhoClicked();
		event.setCancelled(true);
		
		//player.sendMessage(Messages.cannotCraft);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void PrepareItemCraftEvent(PrepareItemCraftEvent event) {
		event.getInventory().setResult(null);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onDeath(PlayerDeathEvent event) {
		Player player = event.getEntity();
		EntityDamageEvent.DamageCause cause = player.getLastDamageCause().getCause();
		Location diedLocation = player.getLocation().clone();
		String killer = "";
		if (player.getKiller() != null) {
			killer = player.getKiller().getName();
		} else {
			killer = "";
		}
		Misc.kill(player, cause, diedLocation, killer);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onJoin(PlayerJoinEvent event){

		Player player = event.getPlayer();

		event.setJoinMessage(null);

		if (PlayersConfy.isRegister(player)) {

			byte type = (byte) PlayersConfy.getType(player);
			byte tip = (byte) PlayersConfy.getTip(player);
			byte part = (byte) PlayersConfy.getPart(player);
			int nb = PlayersConfy.getNb(player);
			String party = PlayersConfy.getParty(player);
			byte color = PlayersConfy.getColor(player);

			/*byte type = 1;
			byte tip = 1;
			byte part = 1;

			int nb = PartyConfy.getNow(type, tip, part);

			String party = Settings.nextParty(type, tip, part, nb);

			if (!PartyConfy.isCreated(party)) {
				PartyManagement.newParty(type, tip, part);
			}

			byte color = Teams.randomizeTeam(party, type, tip, part, nb);*/

			/*player.setCustomName(Teams.colorToChatString(color)+"§l" + player.getName());
			player.setCustomNameVisible(true);*/

			/*Tag
			TagPlayer tagplayer = new TagPlayer(player);
			TagManager tagmanager =  new TagManager(tagplayer);

			tagmanager.setTag(Teams.colorToChatString(color)+"§l" + player.getName());*/

			player.setPlayerListName(Teams.colorToChatString(color) + "§l" + player.getName());

			/*PlayersConfy.addPlayer(player, type, tip, part, nb, color);

			PartyConfy.addPlayer(player);*/

			if (PartyConfy.getPlayers(party).size() == Settings.getNbPlayer(tip, part)) {
				PartyManagement.startParty(type, tip, part, nb);
			}

			if (PlayersConfy.getType(player) == 1) {
				player.teleport(Settings.lobby1);
			} else if (PlayersConfy.getType(player) == 2) {
				player.teleport(Settings.lobby2);
			} else if (PlayersConfy.getType(player) == 3) {
				player.teleport(Settings.lobby3);
			}

			event.setJoinMessage(null);
			ChatManagement.sendMessage("@" + Messages.Join+" §r§6§l["+Integer.toString(PartyConfy.getPlayers(party).size())+"/"+Integer.toString(Settings.getNbPlayer(tip, part))+"]", player);

			Inventory chest = Bukkit.createInventory(null, 9, "§d§l§nENDER INVENTORY");

			PlayersConfy.chest.put(player.getName(), chest);

			player.setGameMode(GameMode.ADVENTURE);
			player.getInventory().clear();
			player.getInventory().setHelmet(new ItemStack(Material.AIR));
			player.getInventory().setChestplate(new ItemStack(Material.AIR));
			player.getInventory().setLeggings(new ItemStack(Material.AIR));
			player.getInventory().setBoots(new ItemStack(Material.AIR));
			for(PotionEffect effect : player.getActivePotionEffects())
			{
				player.removePotionEffect(effect.getType());
			}

			Misc.setArmor(player);

			Teams.showPlayersPlayer(player);

			Misc.setAllPartyXp(party, 0);

			player.setScoreboard(ScoreboardManagement.addTeams(player));
			ScoreboardManagement.refreshScoreboard(party);

			Misc.giveLobbyItems(player);


			if (Main.spectators != null || Main.spectators.size() != 0) {
				int bcl = 0;
				while (bcl < Main.spectators.size()) {
					player.hidePlayer(Main.spectators.get(bcl));
					Main.spectators.get(bcl).showPlayer(player);
					bcl++;
				}
			}

		}
		else {
			player.teleport(Settings.lobby1);

			player.setGameMode(GameMode.ADVENTURE);
			player.getInventory().clear();
			player.getInventory().setHelmet(new ItemStack(Material.AIR));
			player.getInventory().setChestplate(new ItemStack(Material.AIR));
			player.getInventory().setLeggings(new ItemStack(Material.AIR));
			player.getInventory().setBoots(new ItemStack(Material.AIR));
			for(PotionEffect effect : player.getActivePotionEffects())
			{
				player.removePotionEffect(effect.getType());
			}


			ArrayList<Integer> waiters = new ArrayList<Integer>();

			waiters.add(Bukkit.getScheduler().scheduleSyncRepeatingTask(TimeManagement.plugin, new Runnable() {


				@Override
				public void run() {

					if (PlayersConfy.isRegister(player)) {
						byte type = (byte) PlayersConfy.getType(player);
						byte tip = (byte) PlayersConfy.getTip(player);
						byte part = (byte) PlayersConfy.getPart(player);
						int nb = PlayersConfy.getNb(player);
						String party = PlayersConfy.getParty(player);
						byte color = PlayersConfy.getColor(player);

						player.setPlayerListName(Teams.colorToChatString(color) + "§l" + player.getName());

						if (PartyConfy.getPlayers(party).size() == Settings.getNbPlayer(tip, part)) {
							PartyManagement.startParty(type, tip, part, nb);
						}

						if (PlayersConfy.getType(player) == 1) {
							player.teleport(Settings.lobby1);
						} else if (PlayersConfy.getType(player) == 2) {
							player.teleport(Settings.lobby2);
						} else if (PlayersConfy.getType(player) == 3) {
							player.teleport(Settings.lobby3);
						}

						ChatManagement.sendMessage("@" + Messages.Join+" §r§6§l["+Integer.toString(PartyConfy.getPlayers(party).size())+"/"+Integer.toString(Settings.getNbPlayer(tip, part))+"]", player);

						Inventory chest = Bukkit.createInventory(null, 9, "§d§l§nENDER INVENTORY");

						PlayersConfy.chest.put(player.getName(), chest);

						Misc.setArmor(player);

						Teams.showPlayersPlayer(player);

						Misc.setAllPartyXp(party, 0);

						player.setScoreboard(ScoreboardManagement.addTeams(player));
						ScoreboardManagement.refreshScoreboard(party);

						Misc.giveLobbyItems(player);

						if (Main.spectators != null || Main.spectators.size() != 0) {
							int bcl = 0;
							while (bcl < Main.spectators.size()) {
								player.hidePlayer(Main.spectators.get(bcl));
								Main.spectators.get(bcl).showPlayer(player);
								bcl++;
							}
						}

						Bukkit.getScheduler().cancelTask(waiters.get(0));
					} else {
						player.teleport(Settings.lobby1);
					}

				}
			}, 0, 1));

		}


	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onQuit(PlayerQuitEvent event) {
		
		Player player = event.getPlayer();


		if (PlayersConfy.RushPlayers.containsKey(player)) {
			String party = PlayersConfy.getParty(player);

			ChatManagement.sendMessage("@" + Messages.Quit, player);

			RushStatistics.playerQuit(player);

			ArrayList<String> retur = new ArrayList<>();
			retur.add(0, "hub");
			retur.add(1, "rush");
			retur.add(2, Integer.toString(PartyConfy.RushPartys.get(party).getType()));
			retur.add(3, Integer.toString(PartyConfy.RushPartys.get(party).getTip()));
			retur.add(4, Integer.toString(PartyConfy.RushPartys.get(party).getPart()));

			BungeeWork.sendToGameMessage(player, retur);

			PartyConfy.deletePlayer(player);

			if (PartyConfy.getPlayers(party).size() == 0) {
			} else {
				Byte winColor = PartyManagement.testForWin(player);
				if (winColor != (byte) -1) {
					PartyManagement.winParty(party, player, winColor);
				}
			}

			PlayersConfy.deletePlayer(player);

			if (PartyConfy.getPlayers(party).size() == 0) {

				PartyManagement.deleteParty(party);
			}

			PlayersConfy.chest.remove(player.getName());

			event.setQuitMessage(null);


			if (PartyConfy.getStart(party)
					&& !player.getWorld().equals(Settings.lobby1.getWorld())) {
				if (player.getInventory().getContents() != null
						&& player.getInventory().getContents().length != 0) {
					for (ItemStack item : player.getInventory().getContents()) {
						if (item != null) {
							player.getLocation().getWorld().dropItem(player.getLocation(), item);
						}
					}
				}
			}

			if (!PartyConfy.getEnd(party)) {

				ScoreboardManagement.refreshScoreboard(party);
			}
		} else {
			event.setQuitMessage(null);
			if (Main.spectators.contains(player)) {
				Main.spectators.remove(player);
			}
		}


	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {



		if((event.getEntity() instanceof EnderCrystal)) {
			event.setCancelled(true);
		} else if (event.getEntity() instanceof Player
				&& event.getEntity().getWorld().equals(Bukkit.getWorld("lobby"))) {
			event.setCancelled(true);
		}

		else if (event.getEntity() instanceof Player
				&& !((Player) event.getEntity()).getPlayer().hasPotionEffect(PotionEffectType.DAMAGE_RESISTANCE)
				&& (event.getDamager() instanceof Arrow || event.getDamager() instanceof Player)) {

			if (event.getDamager() instanceof Arrow
					&& ((Arrow) event.getDamager()).getShooter() instanceof Player) {
				if (PlayersConfy.getColor(((Player) event.getEntity()).getPlayer()) == PlayersConfy.getColor((Player)((Arrow) event.getDamager()).getShooter())) {
					event.setCancelled(true);
				}
			} else if (event.getDamager() instanceof Player) {
				if (PlayersConfy.getColor(((Player) event.getEntity()).getPlayer()) == PlayersConfy.getColor((Player) event.getDamager())) {
					event.setCancelled(true);
				}
			}


		} else if (event.getEntity() instanceof Player
				&& ((Player) event.getEntity()).getPlayer().hasPotionEffect(PotionEffectType.DAMAGE_RESISTANCE)) {
			event.setCancelled(true);
		}

	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onInteractAtEntity(PlayerInteractEntityEvent event) {
		Player player = event.getPlayer();
		Entity entity = event.getRightClicked();
		
		if (entity instanceof EnderCrystal
				&& !player.getGameMode().equals(GameMode.SPECTATOR)) {
			player.openInventory(Traders.getInventory( PlayersConfy.getType(player)-1, (int) Traders.traderNameToByte(entity.getCustomName()), 0, 0));
		}
		else if (PlayersConfy.getFreezeStatu(player)) {
			event.setCancelled(true);
		}

	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onClickInventory(InventoryClickEvent event) {
		
		Player player = (Player) event.getWhoClicked();
		
		Inventory inv = event.getInventory();

		ItemStack item = new ItemStack(Material.AIR);

		if (event.getCurrentItem() != null) {

			item = event.getCurrentItem().clone();
		}

		if (Traders.tradersAll.contains(inv)) {
			event.setCancelled(true);

			if (!event.getClick().equals(ClickType.DOUBLE_CLICK)) {

				if (item.equals(Items.windowBlue)) {
					BukkitTask time = Bukkit.getScheduler().runTaskLater(TimeManagement.plugin, new Runnable() {
						@Override
						public void run() {
							player.openInventory(Traders.getInventory(Traders.getParty(inv), Traders.getName(inv), Traders.getMain(inv), Traders.getNb(inv) + 1));
						}
					}, 1);
				} else if (item.equals(Items.windowRed)) {
					BukkitTask time = Bukkit.getScheduler().runTaskLater(TimeManagement.plugin, new Runnable() {
						@Override
						public void run() {
							player.openInventory(Traders.getInventory(Traders.getParty(inv), Traders.getName(inv), Traders.getMain(inv), Traders.getNb(inv) - 1));
						}
					}, 1);
				} else if (item.equals(Items.windowGreen)) {
					ItemStack priceItem = (Traders.getPriceItemTrade(Traders.getParty(inv), Traders.getName(inv), Traders.getItem(inv))).clone();


					boolean chestplate = true;
					boolean leftHand = true;


					if (player.getInventory().getChestplate() == null
							|| player.getInventory().getChestplate().getType().equals(Material.AIR)) {
						chestplate = false;
						player.getInventory().setChestplate(new ItemStack(Material.BARRIER));
					}

					if (player.getInventory().getItemInOffHand() == null
							|| player.getInventory().getItemInOffHand().getType().equals(Material.AIR)) {
						leftHand = false;
						player.getInventory().setItemInOffHand(new ItemStack(Material.BARRIER));
					}



					if (player.getInventory().containsAtLeast(priceItem, priceItem.getAmount())
							&& player.getInventory().firstEmpty() != -1) {



						int bcl = 0;
						int nbbcl = 1;

					/*if (event.getClick().equals(ClickType.SHIFT_RIGHT)) {
						nbbcl = 10;
					}*/

						while (bcl < nbbcl) {

							int bucle = 0;

							while (bucle <= 35) {

								if (player.getInventory().getItem(bucle) != null) {

									if (player.getInventory().getItem(bucle).isSimilar(priceItem)
											&& player.getInventory().getItem(bucle).getAmount() >= priceItem.getAmount()) {

										ItemStack newItem = priceItem.clone();

										int aminv = player.getInventory().getItem(bucle).getAmount();
										int amit = priceItem.getAmount();
										int newam = aminv - amit;

										newItem.setAmount(newam);

										player.getInventory().setItem(bucle, newItem);
										//player.getInventory().setItem(player.getInventory().firstEmpty(), (Traders.getItem(inv)).clone());
										ItemStack trading = Traders.getItem(inv).clone();
										if (trading.getType().equals(Material.SHIELD)
												&& PlayersConfy.RushPlayers.containsKey(player)) {
											RushPlayer pl = PlayersConfy.RushPlayers.get(player);
											Banner banner = ((Banner) (PartyConfy.getBannerLocation(pl.getParty(), pl.getColor(), 0).getBlock().getState()));
											trading.setItemMeta(UtilShield.patternOnShield(new ItemStack(Material.SHIELD), banner.getBaseColor(), null).getItemMeta());
										}
										player.getInventory().addItem(trading);

										bucle = 36;

									}
								}

								bucle++;
							}
							bcl++;
						}

					}

					if (!chestplate) {
						player.getInventory().setChestplate(new ItemStack(Material.AIR));
					}

					if (!leftHand) {
						player.getInventory().setItemInOffHand(new ItemStack(Material.AIR));
					}

				} else if (Items.tradeItems.contains(item)
						&& !event.getClickedInventory().getType().equals(InventoryType.PLAYER)) {
					final ItemStack finalItem = item;
					BukkitTask time = Bukkit.getScheduler().runTaskLater(TimeManagement.plugin, new Runnable() {
						@Override
						public void run() {
							int[] tabl = (Traders.testForItemIn(Traders.getParty(inv), Traders.getName(inv), finalItem)).clone();
							player.openInventory(Traders.getInventory(Traders.getParty(inv), Traders.getName(inv), 1, tabl[2]));
						}
					}, 1);
				}
			}
		} else if (item.isSimilar(Items.chest)
				&& player.getOpenInventory().getTopInventory().equals(PlayersConfy.chest.get(player.getName()))) {
			event.setCancelled(true);
		}
		else if (item.getType().equals(Material.LEATHER_HELMET)
				|| item.getType().equals(Material.LEATHER_LEGGINGS)
				|| item.getType().equals(Material.LEATHER_BOOTS)) {
			event.setCancelled(true);
		}
		else if (item.isSimilar(Misc.lobbyItemHub)) {
			event.setCancelled(true);
			BungeeWork.sendPlayer(player, "hub");
		} else if (item.getType().equals(Material.BANNER)) {
			event.setCancelled(true);
			PartyManagement.changeTeamPlayer(player, item);
		} else if (item.isSimilar(Misc.teamitem)) {
			event.setCancelled(true);
			BukkitTask time = Bukkit.getScheduler().runTaskLater(TimeManagement.plugin, new Runnable() {
				@Override
				public void run() {
					player.openInventory(Misc.lobbyInventoryMapTeams.get((byte) PlayersConfy.getTip(player)));
				}
			}, 1);
		} /*else if (item.isSimilar(Items.frostWalker)) {
			event.setCancelled(true);
			player.sendMessage(Messages.frostWalking);

			//player.getInventory().remove(item);
			//event.getCurrentItem().setType(Material.AIR);

			player.getInventory().getBoots().addEnchantment(Enchantment.FROST_WALKER, 1);

			BukkitTask time = Bukkit.getScheduler().runTaskLater(TimeManagement.plugin, new Runnable() {
				@Override
				public void run() {
					player.getInventory().getBoots().removeEnchantment(Enchantment.FROST_WALKER);
				}
			}, 10*20);

		}*/



	}

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void onChat(AsyncPlayerChatEvent event) {
		Player player = event.getPlayer();
		String message = event.getMessage();
		event.setCancelled(true);

		if (PlayersConfy.RushPlayers.containsKey(player)) {

			if (PlayersConfy.getDiedStatu(player)
					|| player.getWorld().equals(Bukkit.getWorld("lobby"))) {
				if (message.charAt(0) != '@') {
					message = "@" + message;
				}
			}

			ChatManagement.sendMessage(message, player);
		}

	}

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void onHungerChange(FoodLevelChangeEvent event) {
		event.setCancelled(true);
	}

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void onDamage(EntityDamageEvent event) {
		if ((event.getEntity() instanceof Player)
				&& event.getCause().equals(EntityDamageEvent.DamageCause.FALL)
				&& event.getEntity().getWorld().equals(Bukkit.getWorld("lobby"))) {
			event.setCancelled(true);
		}

		if (event.getEntity() instanceof Player && event.getEntity().getWorld().equals(Bukkit.getWorld("lobby"))) {
			event.setCancelled(true);
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void WeatherChangeEvent(WeatherChangeEvent event) {
		if (event.toWeatherState()) {
			event.setCancelled(true);
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void PlayerTeleportEvent(PlayerTeleportEvent event) {
		if (!event.getCause().equals(PlayerTeleportEvent.TeleportCause.PLUGIN)) {

			Player player = event.getPlayer();

			if (PlayersConfy.RushPlayers.containsKey(event.getPlayer())) {

				RushPlayer Rplayer = PlayersConfy.RushPlayers.get(event.getPlayer());

				if (Rplayer.isFreeze()) {
					if (event.getCause().equals(PlayerTeleportEvent.TeleportCause.SPECTATE)) {
						event.setCancelled(true);
					} else {
						Bukkit.getScheduler().runTaskLater(TimeManagement.plugin, new Runnable() {
							@Override
							public void run() {
								if (player.getSpectatorTarget() != null) {
									player.setSpectatorTarget(null);
									Location bed = new Location(PartyConfy.getBedPlayer(player).getWorld(),
											PartyConfy.getBedPlayer(player).getX() + 0.5,
											PartyConfy.getBedPlayer(player).getY() + 0,
											PartyConfy.getBedPlayer(player).getZ() + 0.5,
											player.getLocation().getYaw(),
											player.getLocation().getPitch());
									player.teleport(bed);
								}
							}
						}, 1);

					}
				}
			}
		}
	}

}
