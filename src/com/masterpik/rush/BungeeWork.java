package com.masterpik.rush;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;
import com.masterpik.api.util.UtilArrayList;
import com.masterpik.rush.confy.PartyConfy;
import com.masterpik.rush.confy.PlayersConfy;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.messaging.PluginMessageListener;

import java.io.IOException;
import java.util.ArrayList;

public class BungeeWork implements PluginMessageListener {

    Plugin plugin;

    public BungeeWork(Plugin instance) {
        plugin = instance;
    }

    public static void sendPlayer(Player player, String serverName) {
        /*ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("Connect");
        out.writeUTF(serverName);
        player.sendPluginMessage(Main.plugin, "BungeeCord", out.toByteArray());*/

        final BungeeUtils bungee = new BungeeUtils(TimeManagement.plugin, "gamesCo");
        try {
            bungee.sendRequest(BungeeUtils.MessageType.CONNECT, serverName, null, null, "gamesCo", player);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onPluginMessageReceived(String channel, Player playerUsed, byte[] message) {
        Bukkit.getLogger().info("MDR TA RE9U UN PTN DE MESSAGE");

        if (channel.equalsIgnoreCase("BungeeCord")) {
            ByteArrayDataInput in = ByteStreams.newDataInput(message);
            String subchannel = in.readUTF();

            Bukkit.getLogger().info("Message bungee hehe : " + subchannel);

            if (subchannel.equals("gamesCo")) {

                String getMessage = in.readUTF();
                Bukkit.getLogger().info("Message de la gamesCo : " + getMessage);

                ArrayList<String> data = Misc.StringToArray(getMessage);

                /*int bucle = 0;
                while (bucle < data.size()) {
                    Bukkit.getLogger().info(data.get(bucle));
                    bucle++;
                }*/

                if (data.get(0).equals("rush")) {

                    final Player[] player = {Bukkit.getPlayer(data.get(1))};

                    ArrayList<Integer> waiters = new ArrayList<Integer>();

                    waiters.add(Bukkit.getScheduler().scheduleSyncRepeatingTask(TimeManagement.plugin, new Runnable() {


                        @Override
                        public void run() {

                            player[0] = Bukkit.getPlayer(data.get(1));

                            if (player[0] != null && player[0].isOnline()) {
                                if (!PlayersConfy.isRegister(player[0])) {
                                    byte type = (byte) Integer.parseInt(data.get(2));
                                    byte tip = (byte) Integer.parseInt(data.get(3));
                                    byte part = (byte) Integer.parseInt(data.get(4));

                                    int nb = PartyConfy.getNow(type, tip, part);

                                    String party = Settings.nextParty(type, tip, part, nb);

                                    if (!PartyConfy.isCreated(party)) {
                                        PartyManagement.newParty(type, tip, part);
                                    }

                                    byte color = Teams.randomizeTeam(party, type, tip, part, nb);

                                    PlayersConfy.addPlayer(player[0], type, tip, part, nb, color);

                                    PartyConfy.addPlayer(player[0]);

                                    Bukkit.getLogger().info("Joueur certainement enregistrée :p");
                                    Bukkit.getScheduler().cancelTask(waiters.get(0));
                                } else {
                                    Bukkit.getLogger().info("Joueur DEJA enregistrée :p");
                                    Bukkit.getScheduler().cancelTask(waiters.get(0));
                                }
                            } else {
                                Bukkit.getLogger().info("Joueur pas encore arrivé");
                            }


                        }

                    }, 0, 1));

                }
            }
        }

    }

    public static void sendToGameMessage(Player player, ArrayList<String> arguments) {

        /*ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("PlayerCount");
        out.writeUTF("ALL");

        int bucle = 0;
        while (bucle < arguments.size()) {
            out.writeUTF(arguments.get(bucle));
            bucle++;
        }


        player.sendPluginMessage(Main.plugin, "BungeeCord", out.toByteArray());*/

        String argumentsString = UtilArrayList.ArrayListToCryptedString(arguments);

        try {
            Main.bungee.sendRequest(BungeeUtils.MessageType.FORWARD, arguments.get(0), null, argumentsString, "hubCo", Bukkit.getServer()/*player*//*(Player) CraftPlayer.getEntity((CraftServer) Bukkit.getServer(), Main.fakePlayer.getEp())*/);
            Bukkit.getLogger().info("MESSAGE ENVOY2");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

