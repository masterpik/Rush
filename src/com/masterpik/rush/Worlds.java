package com.masterpik.rush;

import org.bukkit.Bukkit;

import java.util.ArrayList;

public class Worlds {

    public static ArrayList<String> worldList;

    public static void worldInit() {

        worldList = new ArrayList<String>();

        worldList.add("1-1-1");
        worldList.add("1-1-2");
        worldList.add("1-1-3");
        worldList.add("1-1-4");
        worldList.add("1-1-5");
        worldList.add("1-2-1");
        worldList.add("1-2-2");
        worldList.add("1-2-3");
        worldList.add("1-2-4");
        worldList.add("1-2-5");
        worldList.add("1-3-1");
        worldList.add("1-3-2");
        worldList.add("1-3-3");
        worldList.add("1-3-4");
        worldList.add("1-3-5");
        worldList.add("2-1-3");
        worldList.add("2-1-4");
        worldList.add("2-1-5");
        worldList.add("2-2-3");
        worldList.add("2-2-4");
        worldList.add("2-2-5");
        worldList.add("3-1-3");
        worldList.add("3-1-4"); //TODO make map
        worldList.add("3-1-5"); //TODO make map
        worldList.add("3-2-3"); //TODO make map
        worldList.add("3-2-4"); //TODO make map
        worldList.add("3-2-5"); //TODO make map


        int bucle = 0;

        while (bucle  < worldList.size()) {

            if (checkIfWorldExist(worldList.get(bucle))) {
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "mvm set autoload false "+worldList.get(bucle));
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "mv unload "+worldList.get(bucle));
            }

            bucle ++;
        }


    }

    public static boolean checkIfWorldExist(String world) {
        boolean check = false;

        int bucle = 0;

        while (bucle < Bukkit.getWorlds().size()) {

            if (Bukkit.getWorlds().get(bucle).getName().equals(world)) {
                check = true;
            }

            bucle++;
        }

        return check;
    }

}
