package com.masterpik.rush;


import com.masterpik.rush.confy.PartyConfy;
import com.masterpik.rush.confy.PlayersConfy;
import org.bukkit.entity.Player;

public class ChatManagement {

    public static String transformTeam(String message, Player player) {
        String nuew;

        nuew = ""+Messages.main+""+Messages.team+""+Teams.colorToChatString(PlayersConfy.getColor(player))+"§l"+player.getName()+"§l ➜§r§o§l "+message+"";

        return nuew;
    }

    public static String transformGlobal(String message, Player player) {
        String nuew;

        nuew = ""+Messages.main+""+Messages.global+""+Teams.colorToChatString(PlayersConfy.getColor(player))+"§l"+player.getName()+"§l ➜§r "+message+"";

        return nuew;
    }

    public static void send(String message, Player player) {
        player.sendMessage(message);
    }

    public static void sendToGlobal(String message, Player player) {
        String party = PlayersConfy.getParty(player);
        int bucle = 0;
        while (bucle  < PartyConfy.getPlayers(party).size()) {
            PartyConfy.getPlayers(party).get(bucle).sendMessage(message);
            bucle ++;
        }

    }

    public static void sendToTeam(String message, Player player) {
        String party = PlayersConfy.getParty(player);
        int bucle = 0;
        while (bucle  < PartyConfy.getPlayersTeam(party, PlayersConfy.getColor(player)).size()) {
            PartyConfy.getPlayersTeam(party, PlayersConfy.getColor(player)).get(bucle).sendMessage(message);
            bucle ++;
        }
    }

    public static void sendMessage(String message, Player player) {
        if (PlayersConfy.getPart(player) != 1) {
            if (message.charAt(0) == '@'
                    || message.charAt(0) == '!') {
                message = message.substring(1);
                message = transformGlobal(message, player);
                sendToGlobal(message, player);
            }
            else {
                message = transformTeam(message, player);
                sendToTeam(message, player);
            }
        }
        else {
            if (message.charAt(0) == '@'
                    || message.charAt(0) == '!') {
                message = message.substring(1);
            }
            message = transformGlobal(message, player);
            sendToGlobal(message, player);
        }
    }

    public static void brodcasteMessage(String message, Player player) {
        message = ""+Messages.main+""+Messages.global+""+message;
        sendToGlobal(message, player);
    }

}
