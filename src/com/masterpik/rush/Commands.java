package com.masterpik.rush;

import com.masterpik.rush.confy.PartyConfy;
import com.masterpik.rush.confy.PlayersConfy;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.ArrayList;

public class Commands implements CommandExecutor{
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (sender instanceof Player) {

            Player player = (Player) sender;

            if (true/*player.getName().equals("axroy") || player.getName().equals("killer_clement") || player.getName().equals("Sultan138")*/) {

                if (command.getName().equalsIgnoreCase("start")) {

                    String party = PlayersConfy.getParty(player);

                    if (!PartyConfy.getStart(party)) {

                        PartyConfy.RushPartys.get(party).setIsForced(true);

                    /*int mank = Settings.getNbPlayer((byte) PartyConfy.getPartyTip(party), (byte) PartyConfy.getPartyPart(party)) - PartyConfy.getPlayers(party).size();
                    int bucle = 0;

                    while (bucle < mank) {

                        PartyConfy.addPlayer((Player) Bukkit.getOfflinePlayer(Integer.toString(bucle)));

                        bucle ++;
                    }*/

                        PartyManagement.startParty((byte) PlayersConfy.getType(player), (byte) PlayersConfy.getTip(player), (byte) PlayersConfy.getPart(player), PlayersConfy.getNb(player));
                    }
                    return true;

                } else if (command.getName().equalsIgnoreCase("spectate")) {
                    Main.spectators.add(player);
                    Bukkit.getPluginManager().callEvent(new PlayerQuitEvent(player, "quit"));
                    player.sendMessage("enter in spectator mode");
                    player.setGameMode(GameMode.SPECTATOR);
                    player.setScoreboard(Main.scoreboardManager.getMainScoreboard());
                    //player.setScoreboard();
                    player.getInventory().clear();
                    ArrayList<Player> players = new ArrayList<>();
                    players.addAll(Bukkit.getOnlinePlayers());

                    int bucle = 0;

                    while (bucle < players.size()) {

                        player.showPlayer(players.get(bucle));
                        players.get(bucle).hidePlayer(player);

                        bucle ++;
                    }

                    return true;
                }

            }

        }

        return false;
    }
}
