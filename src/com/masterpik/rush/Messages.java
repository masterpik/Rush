package com.masterpik.rush;

import com.masterpik.messages.spigot.Api;

public class Messages {

    //all
    /*public static String main = "§0§l▌§r§c§lRUSH§r§0§l▌§r ";
    public static String team = "§2§l▌TEAM▌§r ";
    public static String global = "§4§l▌ALL▌§r ";*/
    public static String main = Api.getString("rush.chat.mainPrefix");
    public static String team = Api.getString("general.all.miniGames.chat.teamPrefix");
    public static String global = Api.getString("general.all.miniGames.chat.allPrefix");

    //confy
    /*public static String newFile = "Fichier de configuration créer avec succès";*/
    public static String newFile = Api.getString("general.logger.confy.created");

    //event
    /*public static String cannotSleep = Messages.main+"§2Pas besoin de dormir !";
    public static String cannotAntiTeam = Messages.main+"§c§lAttention l'anti-jeu peut mener à un bannisement !";
    public static String cannotCraft = Messages.main+"§2Pas besoin de crafter !";
    public static String Join = "§2§lA rejoint la partie";
    public static String Quit = "§4§lA quitté la partie";
    public static String bedExplode1 = "§r§3§lLe lit des §r";
    public static String bedEXplode2 = "§r§3§l a explosé §r";
    public static String win1 = "§r§2L\'équipe ";
    public static String win2 = "§r§2 a gagné";*/
    public static String cannotSleep = Messages.main+Api.getString("prevent.noSleep");
    public static String cannotAntiTeam = Api.getString("noCheat.chat.mainPrefix")+Api.getString("noCheat.chat.antiTeam");
    public static String cannotCraft = Messages.main+Api.getString("prevent.noCraft");
    public static String Join = Api.getString("general.all.miniGames.chat.playerJoin");
    public static String Quit = Api.getString("general.all.miniGames.chat.playerQuit");
    public static String bedExplode1 = Api.getString("rush.detection.bedExplode.1");
    public static String bedEXplode2 = Api.getString("rush.detection.bedExplode.2");
    public static String win1 = Api.getString("general.all.miniGames.detection.teamWin.1");
    public static String win2 = Api.getString("general.all.miniGames.detection.teamWin.2");


    //misc
    /*public static String youDied = "§5§lEst mort";
    public static String diedWithoutBed = "§5§lEst éliminé";
    public static String chooseInventoryTitle = "§2§lChoix des équipes";*/
    public static String youDied = Api.getString("general.all.miniGames.detection.died");
    public static String diedWithoutBed = Api.getString("general.all.miniGames.detection.eleminated");
    public static String chooseInventoryTitle = Api.getString("general.all.miniGames.inventoryTitle.teamChoose")+" ";

    //party management
    /*public static String countRebour = "§a§lDébut de la partie dans §r§2§l" /*... seconde(s);
    public static String partyStart = "§6§lDebut de la partie !";
    public static String partyStartTitle = "§6§lDebut de la partie !";
    public static String endParty = Messages.main+"§6§lPartie terminée merci d'avoir joué sur §r§8§lMAS§r§6§lTER§r§9§lPIK§r";
    public static String reduction = "§r§4§lAttention la bordure commence à se réduire, rapprochez vous du centre";*/
    public static String countRebour = Api.getString("general.all.miniGames.coolDown.partyStarting");
    public static String partyStart = Api.getString("general.all.miniGames.coolDown.partyStart");
    public static String partyStartTitle = Api.getString("general.all.miniGames.title.partyStart");
    public static String endParty = Messages.main+Api.getString("general.all.miniGames.detection.end");
    public static String reduction = Api.getString("general.all.miniGames.detection.bordersReduction");

    /*public static String teamIsFull1 = Messages.main+"§4§l L'équipe§r ";
    public static String teamIsFull2 = " §r§4§lest pleine !";*/
    public static String teamIsFull1 = Messages.main+Api.getString("general.all.miniGames.chat.teamFull.1");
    public static String teamIsFull2 = Api.getString("general.all.miniGames.chat.teamFull.2");

    /*public static String joinTeam = Messages.main+"§2§lTu as bien rejoint l'équipe ";*/
    public static String joinTeam = Messages.main+Api.getString("general.all.miniGames.chat.teamJoin");


    //scoreboard
    /*public static String favicon = "§r§8§l▌§r§6§lT§r§9§l▌§r";
    public static String masterpik = "§r§8§lMAS§r§6§lTER§r§9§lPIK§r";*/

    //public static String favicon = ""+ChatColor.DARK_GRAY+"▌"+ChatColor.GOLD+ChatColor.BOLD+"T"+ChatColor.BLUE+"▌";
    /*public static String masterpik = ""+ChatColor.DARK_GRAY+ChatColor.BOLD+ChatColor.UNDERLINE+"MAS"+ChatColor.GOLD+ChatColor.BOLD+ChatColor.UNDERLINE+"TER"+ChatColor.BLUE+ChatColor.BOLD+ChatColor.UNDERLINE+"PIK";*/
    public static String masterpik = Api.getString("general.masterpik.scoreboard.header");

    public static String bloc1 = "▇";
    public static String bloc2 = "▇▇";

    /*public static String joueurs = "Joueurs ► §o";
    public static String lit = "Lit ► ";*/


    public static String startingMessageDescription = Api.getString("rush.tutorial.startGame");
    public static String behindTntText = Api.getString("rush.tutorial.behindTnt");
    public static String behindECText = Api.getString("rush.tutorial.behindEC");
    public static String behindFrostWalkerText = Api.getString("rush.tutorial.behindFW");
    public static String frostWalking = Messages.main+Api.getString("rush.detection.frostWalking");

    public static String statsRegister = Api.getString("stats.register");


}
