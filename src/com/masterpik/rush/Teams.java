package com.masterpik.rush;

import com.masterpik.rush.confy.PartyConfy;
import com.masterpik.rush.confy.PlayersConfy;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collections;

public class Teams {

    public static ArrayList<Byte> tipToColor(byte tip) {

        ArrayList<Byte> colors = new ArrayList<Byte>();

        if (tip == (byte) 1) {

            colors.add((byte)14);
            colors.add((byte)11);


        }
        else if (tip == (byte) 2) {

            colors.add((byte)14);
            colors.add((byte)5);
            colors.add((byte)4);
            colors.add((byte)11);

        }
        else if (tip == (byte) 3) {

            colors.add((byte)14);
            colors.add((byte)5);
            colors.add((byte)4);
            colors.add((byte)11);
            colors.add((byte)15);
            colors.add((byte)2);
            colors.add((byte)8);
            colors.add((byte)1);
        }


        return colors;
    }

    public static String colorToString(Byte color) {

        String colorS = "";

        switch (color) {

            case 0 :
                colorS = "Blanc";
                break;
            case 15 :
                colorS = "Noir";
                break;
            case 14 :
                colorS = "Rouge";
                break;
            case 5 :
                colorS = "Vert";
                break;
            case 4 :
                colorS = "Jaune";
                break;
            case 11 :
                colorS = "Bleu";
                break;
            case 2 :
                colorS = "Violet";
                break;
            case 8 :
                colorS = "Gris";
                break;
            case 1 :
                colorS = "Orange";
                break;
            default:
                colorS = "ERROR";

        }

        //colorS.toUpperCase();

        return colorS;
    }

    public static ChatColor colorToChatColor(Byte color) {

        ChatColor colorS;

        switch (color) {

            case 0 :
                colorS = ChatColor.WHITE;
                break;
            case 15 :
                colorS = ChatColor.DARK_GRAY;
                break;
            case 14 :
                colorS = ChatColor.RED;
                break;
            case 5 :
                colorS = ChatColor.GREEN;
                break;
            case 4 :
                colorS = ChatColor.YELLOW;
                break;
            case 11 :
                colorS = ChatColor.AQUA;
                break;
            case 2 :
                colorS = ChatColor.LIGHT_PURPLE;
                break;
            case 8 :
                colorS = ChatColor.GRAY;
                break;
            case 1 :
                colorS = ChatColor.GOLD;
                break;
            default:
                colorS = ChatColor.DARK_RED;

        }

        return colorS;
    }

    public static Color colorToColor(Byte color) {

        Color colorS;

        switch (color) {

            case 0 :
                colorS = Color.WHITE;
                break;
            case 15 :
                colorS = Color.BLACK;
                break;
            case 14 :
                colorS = Color.RED;
                break;
            case 5 :
                colorS = Color.GREEN;
                break;
            case 4 :
                colorS = Color.YELLOW;
                break;
            case 11 :
                colorS = Color.AQUA;
                break;
            case 2 :
                colorS = Color.PURPLE;
                break;
            case 8 :
                colorS = Color.GRAY;
                break;
            case 1 :
                colorS = Color.ORANGE;
                break;
            default:
                colorS = Color.AQUA;

        }

        return colorS;
    }

    public static String colorToChatString(Byte color) {

        String colorS = "";

        switch (color) {

            case 0 :
                colorS = "§f";
                break;
            case 15 :
                colorS = "§8";
                break;
            case 14 :
                colorS = "§c";
                break;
            case 5 :
                colorS = "§a";
                break;
            case 4 :
                colorS = "§e";
                break;
            case 11 :
                colorS = "§b";
                break;
            case 2 :
                colorS = "§d";
                break;
            case 8 :
                colorS = "§7";
                break;
            case 1 :
                colorS = "§6";
                break;
            default:
                colorS = "§4";

        }

        return colorS;
    }

    public static Byte byteToBannerByte(Byte color) {
        Byte colorS = (byte) 0;

        switch (color) {

            case 0 :
                colorS = (byte) 15;
                break;
            case 15 :
                colorS = (byte) 0;
                break;
            case 14 :
                colorS = (byte) 1;
                break;
            case 5 :
                colorS = (byte) 10;
                break;
            case 4 :
                colorS = (byte) 11;
                break;
            case 11 :
                colorS = (byte) 12;
                break;
            case 2 :
                colorS = (byte) 13;
                break;
            case 8 :
                colorS = (byte) 7;
                break;
            case 1 :
                colorS = (byte) 14;
                break;
            default:
                colorS = (byte) 0;

        }

        return colorS;
    }

    public static Byte bannerByteToByte(Byte color) {
        Byte colorS = (byte) 0;

        switch (color) {

            case 15 :
                colorS = (byte) 0;
                break;
            case 0 :
                colorS = (byte) 15;
                break;
            case 1 :
                colorS = (byte) 14;
                break;
            case 10 :
                colorS = (byte) 5;
                break;
            case 11 :
                colorS = (byte) 4;
                break;
            case 12 :
                colorS = (byte) 11;
                break;
            case 13 :
                colorS = (byte) 2;
                break;
            case 7 :
                colorS = (byte) 8;
                break;
            case 14 :
                colorS = (byte) 1;
                break;
            default:
                colorS = (byte) 0;

        }

        return colorS;
    }

    public static Byte randomizeTeam(String party, byte type, byte tip, byte part, int nb) {
        byte color;

        ArrayList<Byte> colors = new ArrayList<Byte>();
        ArrayList<Player> playersList = PartyConfy.getPlayers(party);
        ArrayList<Player> playersColorList = new ArrayList<Player>();
        int nbMax = Settings.getNbPlayer((byte) tip, (byte) part);
        int bucle = 0;
        int bucle2 = 0;
        int bcl = 0;

        if (tip == 1) {
            playersColorList = PartyConfy.getPlayersTeam(party, (byte)11);
            if (playersColorList.size() != (nbMax/2)
                    || playersColorList.get(0).equals("null")) {
                colors.add((byte)11);
            }
            playersColorList = PartyConfy.getPlayersTeam(party, (byte)14);
            if (playersColorList.size() != (nbMax/2)
                    || playersColorList.get(0).equals("null")) {
                colors.add((byte)14);
            }
        }
        else if (tip == 2) {
            playersColorList = PartyConfy.getPlayersTeam(party, (byte)14);
            if (playersColorList.size() != (nbMax/4)
                    || playersColorList.get(0).equals("null")) {
                colors.add((byte)14);
            }
            playersColorList = PartyConfy.getPlayersTeam(party, (byte)5);
            if (playersColorList.size() != (nbMax/4)
                    || playersColorList.get(0).equals("null")) {
                colors.add((byte)5);
            }
            playersColorList = PartyConfy.getPlayersTeam(party, (byte)4);
            if (playersColorList.size() != (nbMax/4)
                    || playersColorList.get(0).equals("null")) {
                colors.add((byte)4);
            }
            playersColorList = PartyConfy.getPlayersTeam(party, (byte)11);
            if (playersColorList.size() != (nbMax/4)
                    || playersColorList.get(0).equals("null")) {
                colors.add((byte)11);
            }
        }
        else if (tip == 3) {
            playersColorList = PartyConfy.getPlayersTeam(party, (byte)14);
            if (playersColorList.size() != (nbMax/8)
                    || playersColorList.get(0).equals("null")) {
                colors.add((byte)14);
            }
            playersColorList = PartyConfy.getPlayersTeam(party, (byte)5);
            if (playersColorList.size() != (nbMax/8)
                    || !playersColorList.get(0).equals("null")) {
                colors.add((byte)5);
            }
            playersColorList = PartyConfy.getPlayersTeam(party, (byte)4);
            if (playersColorList.size() != (nbMax/8)
                    || playersColorList.get(0).equals("null")) {
                colors.add((byte)4);
            }
            playersColorList = PartyConfy.getPlayersTeam(party, (byte)11);
            if (playersColorList.size() != (nbMax/8)
                    || playersColorList.get(0).equals("null")) {
                colors.add((byte)11);
            }
            playersColorList = PartyConfy.getPlayersTeam(party, (byte)15);
            if (playersColorList.size() != (nbMax/8)
                    || playersColorList.get(0).equals("null")) {
                colors.add((byte)15);
            }
            playersColorList = PartyConfy.getPlayersTeam(party, (byte)2);
            if (playersColorList.size() != (nbMax/8)
                    || playersColorList.get(0).equals("null")) {
                colors.add((byte)2);
            }
            playersColorList = PartyConfy.getPlayersTeam(party, (byte)8);
            if (playersColorList.size() != (nbMax/8)
                    || playersColorList.get(0).equals("null")) {
                colors.add((byte)8);
            }
            playersColorList = PartyConfy.getPlayersTeam(party, (byte)1);
            if (playersColorList.size() != (nbMax/8)
                    || playersColorList.get(0).equals("null")) {
                colors.add((byte)1);
            }
        }

        /*bucle2 = 0;

        if (!PartyConfy.getPlayers(party).get(0).equals("null")) {
            ArrayList<String> playersList = PartyConfy.getPlayers(party);
            while (bucle2 < playersList.size()) {
                colors.remove((byte)PlayersConfy.getColor(Bukkit.getPlayer(playersList.get(bucle2))));
                bucle2++;
            }
        }*/


        Collections.shuffle(colors);
        color = colors.get(0);

        //color = colors.get(new Random().nextInt(colors.size()));

        return color;
    }

    public static void showPlayersPlayer(Player player) {

        ArrayList<Player> allPlayers = new ArrayList<Player>();
        allPlayers.addAll(Bukkit.getOnlinePlayers());

        int bucle1 = 0;

        while (bucle1 < allPlayers.size()) {
            player.hidePlayer(allPlayers.get(bucle1));
            allPlayers.get(bucle1).hidePlayer(player);
            bucle1 ++;
        }

        ArrayList<Player> partyPlayers = PartyConfy.getPlayers(PlayersConfy.getParty(player));

        int bucle2 = 0;

        while (bucle2 < partyPlayers.size()) {

            player.showPlayer(partyPlayers.get(bucle2));
            partyPlayers.get(bucle2).showPlayer(player);

            bucle2 ++;
        }

        //player.getWorld().refreshChunk(player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ());
        //player.getWorld().loadChunk(player.getLocation().getChunk());

    }

    public static String colorToFix(Byte color) {
        String colorS = "";

        switch (color) {

            case 0 :
                colorS = "  ";
                break;
            case 15 :
                colorS = "  ";
                break;
            case 14 :
                colorS = " ";
                break;
            case 5 :
                colorS = "  ";
                break;
            case 4 :
                colorS = " ";
                break;
            case 11 :
                colorS = "  ";
                break;
            case 2 :
                colorS = "   ";
                break;
            case 8 :
                colorS = "   ";
                break;
            case 1 :
                colorS = " ";
                break;
            default:
                colorS = "ERROR";

        }

        //colorS.toUpperCase();

        return colorS;
    }
}
