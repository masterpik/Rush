package com.masterpik.rush;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

public class TimeManagement {

    public static BukkitRunnable timer = new Timer();
    public static Plugin plugin = Bukkit.getPluginManager().getPlugin("Rush");

    public static void startTimer() {
        timer.runTaskTimer(plugin, 0, 40);
    }

    public static void stopTimer() {
        timer.cancel();
    }

}
