package com.masterpik.rush;

import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.messaging.Messenger;
import org.bukkit.plugin.messaging.PluginMessageListener;
import org.bukkit.plugin.messaging.PluginMessageRecipient;

import java.io.*;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.HashMap;
import java.util.List;

public class BungeeUtils implements PluginMessageListener {

    private final String subChannel;
    private final Plugin plugin;
    private final BungeeMessagesListener listener;

    public enum MessageType {
        CONNECT("Connect"),
        CONNECT_OTHER("ConnectOther"),
        IP("IP"),
        PLAYER_COUNT("PlayerCount"),
        PLAYER_LIST("PlayerList"),
        GET_SERVERS("GetServers"),
        MESSAGE("Message"),
        GET_SERVER("GetServer"),
        FORWARD("Forward"),
        UUID("UUID"),
        UUID_OTHER("UUIDOther");

        public final String name;
        private final static HashMap<String, MessageType> BY_NAME = new HashMap<String, MessageType>();

        MessageType(final String name) {
            this.name = name;
        }

        public static final MessageType fromName(final String name) {
            return BY_NAME.get(name);
        }

        static {
            for(final MessageType type : values()) {
                BY_NAME.put(type.name, type);
            }
        }
    }

    public BungeeUtils(final Plugin plugin, final String subChannel) {
        final Messenger messenger = Bukkit.getMessenger();
        messenger.registerOutgoingPluginChannel(plugin, "BungeeCord");
        //messenger.registerIncomingPluginChannel(plugin, "BungeeCord", new BungeeWork(this));
        this.subChannel = subChannel;
        this.plugin = plugin;
        listener = null;
        //listener = (BungeeMessagesListener) plugin;
    }

    public interface BungeeMessagesListener extends EventListener, Plugin {
        void onMessageReceived(final MessageType type, final String[] response);

    }

    @Override
    public final void onPluginMessageReceived(final String channel, final Player player, final byte[] message) {
        try {
            if(!channel.equals("BungeeCord")) {
                return;
            }
            final DataInputStream in = new DataInputStream(new ByteArrayInputStream(message));
            final String typeStr = in.readUTF();
            MessageType type = MessageType.fromName(typeStr);
            final List<String> response = new ArrayList<String>();
            if(type == null) {
                type = MessageType.FORWARD;
                if(typeStr.equals(subChannel)) {
                    for(final String line : in.readUTF().split(" ")) {
                        response.add(line);
                    }
                }
            }
            else {
                switch(type) {
                    case IP:
                    case PLAYER_COUNT:
                        response.add(in.readUTF());
                        response.add(String.valueOf(in.readInt()));
                        break;
                    case PLAYER_LIST:
                    case UUID_OTHER:
                        response.add(in.readUTF());
                        response.add(in.readUTF());
                        break;
                    case GET_SERVERS:
                    case GET_SERVER:
                    case UUID:
                        response.add(in.readUTF());
                        break;
                    default:
                        break;
                }
            }
            listener.onMessageReceived(type, response.size() == 0 ? new String[]{"Nothing"} : response.toArray(new String[response.size()]));
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    public final void sendRequest(final MessageType type, final String server, final String destPlayer, final String arg, final String subChannel) throws IOException {
        sendRequest(type, server, destPlayer, arg, subChannel, ((ArrayList<Player>)Bukkit.getOnlinePlayers()).get(0));
    }

    public final void sendRequest(final MessageType type, final String server, final String destPlayer, final String arg, final String subChannel, final PluginMessageRecipient sender) throws IOException {
        final ByteArrayOutputStream b = new ByteArrayOutputStream();
        final DataOutputStream out = new DataOutputStream(b);
        out.writeUTF(type.name);
        switch(type) {
            case CONNECT:
            case PLAYER_COUNT:
            case PLAYER_LIST:
                out.writeUTF(server);
                break;
            case CONNECT_OTHER:
                out.writeUTF(destPlayer);
                out.writeUTF(server);
                break;
            case MESSAGE:
                out.writeUTF(destPlayer);
                out.writeUTF(arg);
                break;
            case FORWARD:
                final byte[] data = arg.getBytes();
                out.writeUTF(server);
                out.writeUTF(subChannel);
                out.writeShort(data.length);
                out.write(data);
                break;
            case UUID_OTHER:
                out.writeUTF(destPlayer);
                break;
            default:
                break;
        }
        sender.sendPluginMessage(plugin, "BungeeCord", b.toByteArray());
    }

    public final void sendRequest(final MessageType type, final String server, final String destPlayer, final String arg, final String subChannel, final Server sender) throws IOException {
        final ByteArrayOutputStream b = new ByteArrayOutputStream();
        final DataOutputStream out = new DataOutputStream(b);
        out.writeUTF(type.name);
        switch(type) {
            case CONNECT:
            case PLAYER_COUNT:
            case PLAYER_LIST:
                out.writeUTF(server);
                break;
            case CONNECT_OTHER:
                out.writeUTF(destPlayer);
                out.writeUTF(server);
                break;
            case MESSAGE:
                out.writeUTF(destPlayer);
                out.writeUTF(arg);
                break;
            case FORWARD:
                final byte[] data = arg.getBytes();
                out.writeUTF(server);
                out.writeUTF(subChannel);
                out.writeShort(data.length);
                out.write(data);
                break;
            case UUID_OTHER:
                out.writeUTF(destPlayer);
                break;
            default:
                break;
        }
        sender.sendPluginMessage(plugin, "BungeeCord", b.toByteArray());
    }

}
